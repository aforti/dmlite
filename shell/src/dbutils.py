from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import re
import threading
import logging

# compatibility with MySQL modules available on SLC6, CentOS7, CentOS8
try:
    import pymysql
    import pymysql.cursors as pymysql_cursors
except ImportError:
    try:
        import MySQLdb as pymysql
        import MySQLdb.cursors as pymysql_cursors
        # implement functions missing in MySQLdb
        if not hasattr(pymysql_cursors.BaseCursor, '__enter__'):
            def pymysql_cursors__enter__(self):
                return self
            pymysql_cursors.BaseCursor.__enter__ = pymysql_cursors__enter__
        if not hasattr(pymysql_cursors.BaseCursor, '__exit__'):
            def pymysql_cursors__exit__(self, *exc_info):
                del exc_info
                self.close()
            pymysql_cursors.BaseCursor.__exit__ = pymysql_cursors__exit__
    except ImportError:
        sys.exit("Could not import MySQL module, please install the MySQL Python module (python2-mysql/python3-mysql or MySQL-python).")


_log = logging.getLogger('dmlite-shell')



class DBConn(object):
    """Simple class that provides access to the shared
    or new database connections to cns_db and dpm_db"""
#    _instance = None
    _conn = {}
    _mode = None
    _data = {
        'host': 'localhost',
        'port': 3306,
        'user': 'dpm',
        'pass': None,
        'cns_db': 'cns_db',
        'dpm_db': 'dpm_db',
    }
    _connections = []
    _lock = threading.Lock()

    _config_obsolete_cns_filename = '/usr/etc/NSCONFIG'
    _config_obsolete_dpm_filename = '/usr/etc/DPMCONFIG'
    _config_legacy_filename = '/etc/dmlite.conf.d/mysql.conf'
    _config_dome_filename = '/etc/domehead.conf'

    _config_legacy_map = {
        'MySqlHost': 'host',
        'MySqlPort': 'port',
        'MySqlUsername': 'user',
        'MySqlPassword': 'pass',
        'NsDatabase': 'cns_db',
        'DpmDatabase': 'dpm_db',
    }
    _config_dome_map = {
        'head.db.host:': 'host',
        'head.db.port:': 'port',
        'head.db.user:': 'user',
        'head.db.password:': 'pass',
        'head.db.cnsdbname:': 'cns_db',
        'head.db.dpmdbname:': 'dpm_db',
    }


    @classmethod
    def get(cls, db):
        """return shared connection for given database"""
        _log.debug("get database connection for %s", db)

        if not cls._mode:
            cls.configure()

        if db not in cls._conn:
            # no global connection exist, create first connection
            with cls._lock:
                if db not in cls._conn:
                    conn = pymysql.connect(
                        host=cls._data['host'], user=cls._data['user'],
                        passwd=cls._data['pass'], db=cls._data[db])
                    cls._connections.append(conn)
                    cls._conn[db] = conn

        return cls._conn[db]


    @classmethod
    def new(cls, db):
        """return new connection for given database"""
        _log.debug("new database connection for %s", db)

        if not cls._mode:
            cls.configure()

        conn = pymysql.connect(
            host=cls._data['host'], user=cls._data['user'],
            passwd=cls._data['pass'], db=cls._data[db])

        with cls._lock:
            cls._connections.append(conn)

        return conn


    @classmethod
    def close(cls, conn=None):
        with cls._lock:
            if conn:
                if conn in cls._connections:
                    try:
                        if conn.open:
                            conn.close()
                        cls._connections.remove(conn)
                    except pymysql.Error as e:
                        _log.warn("failed to close database connection %s: %s", str(conn), str(e))
                else:
                    _log.debug("unable to find db connection %s to close", str(conn))
            else:
                # close/remove all connections
                for conn in cls._connections:
                    try:
                        if conn.open:
                            conn.close()
                        cls._connections.remove(conn)
                    except pymysql.Error as e:
                        _log.warn("failed to close database connection %s: %s", str(conn), str(e))


    @classmethod
    def configure(cls, mode='auto', data=None):
        """Configure parameters for database connection"""
        if mode not in ['auto', 'dome', 'legacy', 'obsolete', 'user']:
            raise Exception("unknown database configuration mode '%s'" % mode)

        with cls._lock:
            if not cls._mode:
                _log.debug("configure database in %s mode", mode)
            else:
                _log.debug("database connection already configured, skipping %s", mode)

            if not cls._mode and mode in ['auto', 'user']:
                if data:
                    cls._data.update(data)
                    cls._mode = 'user'
                elif mode == 'user':
                    raise Exception("missing connection data parametr")

            if not cls._mode and mode in ['auto', 'legacy']:
                if os.path.exists(DBConn._config_legacy_filename) and os.stat(DBConn._config_legacy_filename).st_size > 0:
                    cls._data.update(DBConn._read_config(DBConn._config_legacy_filename, DBConn._config_legacy_map))
                elif mode == 'legacy':
                    raise Exception("missing or emply legacy configuration file %s" % DBConn._config_legacy_filename)

            if not cls._mode and mode in ['auto', 'dome']:
                if os.path.exists(DBConn._config_dome_filename) and os.stat(DBConn._config_dome_filename).st_size > 0:
                    cls._data.update(DBConn._read_config(DBConn._config_dome_filename, DBConn._config_dome_map))
                    cls._mode = 'dome'
                elif mode == 'dome':
                    raise Exception("missing or emply dome configuration file %s" % DBConn._config_dome_filename)

            if not cls._mode and mode in ['auto', 'obsolete']:
                if os.path.exists(DBConn._config_obsolete_cns_filename) and os.path.exists(DBConn._config_obsolete_dpm_filename):
                    cls._data.update(DBConn._read_dpm_config(DBConn._config_obsolete_cns_filename, 'cns_db'))
                    cls._data.update(DBConn._read_dpm_config(DBConn._config_obsolete_dpm_filename, 'dpm_db'))
                    cls._mode = 'obsolete'
                elif mode == 'obsolete':
                    raise Exception("missing or emply obsolete configuration files %s and %s" % (DBConn._config_obsolete_cns_filename, DBConn._config_obsolete_dpm_filename))

            if not cls._mode and mode == 'auto':
                raise Exception("auto database configuration mode did not succeeded")

            _log.debug("database configured using %s config: %s", cls._mode, ", ".join(["%s=%s" % (k, v) for k, v in cls._data.items() if k != 'pass']))


    @staticmethod
    def _read_config(filename, cfgmap):
        _log.debug("reading database configuration from %s", filename)

        ret = {}

        with open(filename) as f:
            for line in f.readlines():
                for k, v in cfgmap.items():
                    if line.startswith(k):
                        ret[v] = line[len(k):].strip()

        if 'port' in ret:
            ret['port'] = int(ret['port'])
            if ret['port'] == 0:
                ret['port'] = 3306

        return ret


    @staticmethod
    def _read_dpm_config(filename, db):
        _log.debug("reading database configuration from %s", filename)

        ret = {}
        res = None

        with open(filename) as f:
            config_line = f.readline().strip()
            nsre = re.compile(r"(.*)/(.*)@([^/]*)(?:/(.*))?")
            res = nsre.match(config_line)
            if res == None:
                raise Exception("bad line in DPM config '%s', doesn't match re '%s'", filename, nsre)

        ret['user'] = res.group(1)
        ret['pass'] = res.group(2)
        ret['host'] = res.group(3)
        if res.group(4):
            ret[db] = res.group(4)

        return ret



class CachedFullPath(object):
    """DPM file metadata stored in database have pointer just
    to parent directory and to build whole path it is necessary
    to recursively query all parent directories to the root "/".

    Althought these queries are pretty cheap they are done for
    each file and even with small latencies (especially with
    database on dedicated server) they can take quite a time.

    This class not only caches past queries, but also limit
    size of cached data not to exhaust whole memory while
    dumping big DPM database with a lot of files.
    """

    def __init__(self, conn=None, maxsize=1000000, table='Cns_file_metadata', fileid_only=False):
        self._cache = {}
        self._cache_path = {}
        if conn:
            self._conn = conn
        else:
            self._conn = DBConn.get('cns_db')
        self._maxsize = maxsize
        self._table = table
        self._fileid_only = fileid_only
        self._ntotal = 0
        self._ncached = 0
        self._nqueries = 0
        self._ncleanup = 0

    def __del__(self):
        if _log:
            _log.info("path lookup cache usage: total %i, cached %i, queries %i, cleanup %i", self._ntotal, self._ncached, self._nqueries, self._ncleanup)

    def _fill_cache(self, fileid):
        """Recursively get full path for given fileid (simple - could be used to validate _fill_cache_multi)"""
        self._ntotal += 1

        if fileid in self._cache:
            self._ncached += 1
            return

        if len(self._cache) >= self._maxsize:
            _log.debug("fullpath cache too big (%i), dropping cached records...", len(self._cache))
            self._ncleanup += 1
            del(self._cache)
            del(self._cache_path)
            self._cache = {}
            self._cache_path = {}

        sql = "SELECT parent_fileid, name FROM %s WHERE fileid=%%s" % self._table
        if self._fileid_only:
            sql = "SELECT parent_fileid FROM %s WHERE fileid=%%s" % self._table

        cursor = self._conn.cursor()
        cursor.execute(sql, (fileid, ))
        res = cursor.fetchone()
        cursor.close()

        self._nqueries += 1

        if _log.getEffectiveLevel() < logging.DEBUG:
            _log.debug("query parent directory '%s': %s", sql, res)

        if res == None:
            if fileid != 0:
                _log.info("db inconsistency: could not find path for fileid %i (most likely the entry is orphan)", fileid)
            else:
                _log.debug("no parent for top level directory 0")
            self._cache[fileid] = None
            return

        parentid = res[0]
        name = str(fileid) if self._fileid_only else res[1]

        if parentid == 0: # top level directory
            self._cache[fileid] = [fileid]
            self._cache_path[fileid] = ''
            return

        if name.find('/') != -1: # this script doesn't support '/' characters in metadata name
            raise Exception("fileid {0} contains slash character in its name '{1}'".format(fileid, name))

        self._fill_cache(parentid)
        if parentid not in self._cache or self._cache[parentid] == None:
            # db inconsistency already detected and logged
            self._cache[fileid] = None
            return

        self._cache[fileid] = self._cache[parentid] + [fileid]
        if not self._fileid_only:
            fullpath = "{0}/{1}".format(self._cache_path[parentid], name)
            self._cache_path[fileid] = fullpath

    def _fill_cache_multi(self, fileids):
        """Reduce impact of query latency by resolving paths for multiple
        fileids. Max number of queries still corresponds to the max path
        depth, but all fileids are resolved at the same time."""
        self._ntotal += len(fileids)

        if len(self._cache) + len(fileids) >= self._maxsize:
            _log.debug("fullpath cache too big (%i+%i), dropping cached records...", len(self._cache), len(fileids))
            self._ncleanup += 1
            del(self._cache)
            del(self._cache_path)
            self._cache = {}
            self._cache_path = {}

        tores = set()
        id2name = {}
        id2parent = {}
        for fileid in fileids:
            if fileid in self._cache:
                if self._cache[fileid] != None:
                    self._ncached += len(self._cache[fileid])
                else:
                    self._ncached += 1
            else:
                tores.add(fileid)

        if len(tores) > 0:
            cursor = self._conn.cursor()

            while len(tores) > 0:

                sql = "SELECT fileid, parent_fileid, name FROM Cns_file_metadata WHERE fileid IN ({0})".format(','.join([ str(x) for x in tores ]))
                if self._fileid_only:
                    sql = "SELECT fileid, parent_fileid FROM Cns_file_metadata WHERE fileid IN ({0})".format(','.join([ str(x) for x in tores ]))

                cursor.execute(sql)

                self._nqueries += 1
                tores = set()

                for row in cursor:
                    fileid = row[0]
                    parentid = row[1]
                    name = str(row[0]) if self._fileid_only else row[2]

                    if _log.getEffectiveLevel() < logging.DEBUG:
                        _log.debug("query parent directory '%s': %s", sql, row)

                    if parentid == 0:
                        name = ''

                    if name.find('/') != -1: # this script doesn't support '/' characters in metadata name
                        raise Exception("fileid {0} contains slash character in its name '{1}'".format(fileid, name))

                    id2name[fileid] = name
                    id2parent[fileid] = parentid

                    if parentid == 0:
                        pass
                    elif parentid in self._cache:
                        if self._cache[parentid] != None:
                            self._ncached += len(self._cache[parentid])
                        else:
                            self._ncached += 1
                    elif parentid not in id2parent:
                        tores.add(parentid)

            cursor.close()

        for fileid in fileids:
            if fileid in self._cache: continue

            currid = fileid
            revids = []

            while True:
                if currid in self._cache:
                    if self._cache[currid] != None:
                        self._ncached += len(self._cache[currid])
                    else:
                        self._ncached += 1
                    break
                elif currid in id2parent:
                    if currid in revids:
                        revids.reverse()
                        fullpath = '/'.join([id2name[x] for x in revids])
                        _log.info("db inconsistency: detected directory loop for fileid %i parent %i %s", fileid, currid, fullpath)
                        for revid in revids: self._cache[revid] = None
                        revids = []
                        break
                    revids.append(currid)
                    currid = id2parent[currid]
                    if currid == 0: # root directory
                        break
                else:
                    if fileid != 0:
                        if fileid != currid:
                            revids.reverse()
                            fullpath = '/'.join([id2name[x] for x in revids])
                            _log.info("db inconsistency: could not find path for fileid %i parent %i (most likely the entry is orphan, path %s)", fileid, currid, fullpath)
                        else:
                            _log.info("db inconsistency: could not find path for fileid %i (most likely the entry is orphan)", fileid)
                    else:
                        _log.debug("no parent for top level directory 0")
                    for revid in revids: self._cache[revid] = None
                    revids = []
                    break

            if len(revids) > 0:
                revids.reverse()
                for i, revid in enumerate(revids):
                    if currid == 0:
                        self._cache[revid] = revids[:i+1]
                    else:
                        self._cache[revid] = self._cache[currid] + revids[:i+1]
                    if not self._fileid_only:
                        if revid in self._cache_path: continue
                        pathsuffix = '/'.join([id2name[x] for x in revids[:i+1]])
                        if currid == 0:
                            self._cache_path[revid] = pathsuffix
                        elif currid in self._cache_path:
                            self._cache_path[revid] = "{0}/{1}".format(self._cache_path[currid], pathsuffix)


    def get_ids(self, fileid):
        self._fill_cache_multi(fileid)
        #self._fill_cache_multi([fileid])

        return self._cache.get(fileid)


    def get_path(self, fileid):
        if self._fileid_only:
            raise Exception("can't get directory path with {0} instance in fileid_only mode".format(self.__class__.__name__))

        self._fill_cache(fileid)
        #self._fill_cache_multi([fileid])

        return self._cache_path.get(fileid)


    def get_ids_multi(self, fileids):
        ret = {}

        self._fill_cache_multi(fileids)

        for fileid in fileids:
            if fileid not in self._cache: continue # db inconsistency already reported
            ret[fileid] = self._cache[fileid]

        return ret


    def get_path_multi(self, fileids):
        if self._fileid_only:
            raise Exception("can't get directory path with {0} instance in fileid_only mode".format(self.__class__.__name__))

        ret = {}

        self._fill_cache_multi(fileids)

        for fileid in fileids:
            if fileid not in self._cache_path: continue # db inconsistency already reported
            ret[fileid] = self._cache_path[fileid]

        return ret



class DPMDB(object):
    """Legacy DPMDB object used by interpreter module"""

    def __init__(self, legacy=False):
        self.dirhash = {}

        try:
            if legacy:
                DBConn.configure('legacy')
            else:
                DBConn.configure('dome')

            # separate connections for dpm_db and cns_db
            self.dpmdb_c = DBConn.new('dpm_db').cursor()
            self.nsdb_c = DBConn.new('cns_db').cursor()

        except pymysql.Error as e:
            _log.error("database connection failed, error %d: %s", e.args[0], e.args[1])
            raise e

    def getReplicasInServer(self, server):
        """Method to get all the file replica for a single server."""
        try:
            self.nsdb_c.execute('''
                SELECT m.name, r.poolname,r.fs, r.host, r.sfn, m.filesize, m.gid, m.status, r.status, r.setname, r.ptime
                FROM Cns_file_replica r
                JOIN Cns_file_metadata m USING (fileid)
                WHERE r.host = %s
                ''', (server, ))
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(FileReplica(row[0], row[1], row[2], row[3], row[4].decode('utf-8'), row[5], row[6], row[7], row[8], row[9], row[10]))
            return ret
        except pymysql.Error as e:
            _log.error("Database server '%s' replica query failed, error %d: %s", server, e.args[0], e.args[1])
            raise e

    def getReplicasInFS(self, fsname, server):
        """Method to get all the file replica for a FS."""
        try:
            self.nsdb_c.execute('''
                SELECT m.name, r.poolname,r.fs, r.host, r.sfn, m.filesize, m.gid, m.status, r.status, r.setname, r.ptime
                FROM Cns_file_replica r
                JOIN Cns_file_metadata m USING (fileid)
                WHERE r.fs = %s AND r.host= %s
                ''', (fsname, server))
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(FileReplica(row[0], row[1], row[2], row[3], row[4].decode('utf-8'), row[5], row[6], row[7], row[8], row[9], row[10]))
            return ret
        except pymysql.Error as e:
            _log.error("Database server '%s' filesystem '%s' replica query failed, error %d: %s", server, fsname, e.args[0], e.args[1])
            raise e

    def getReplicaInFSFolder(self, fsname, server, folder):
        """Method to get all the file replica for a FS folder"""
        try:
            fsname_esc = pymysql.escape_string(fsname)
            server_esc = pymysql.escape_string(server)
            folder_esc = pymysql.escape_string(folder)
            self.nsdb_c.execute('''
                SELECT m.name, r.poolname,r.fs, r.host, r.sfn, m.filesize, m.gid, m.status, r.status, r.setname, r.ptime
                FROM Cns_file_replica r
                JOIN Cns_file_metadata m USING (fileid)
                WHERE r.fs = '%s' AND r.host= '%s' AND r.sfn LIKE '%s:%s/%s%%'
                ''' % (fsname_esc, server_esc, server_esc, fsname_esc, folder_esc))
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(FileReplica(row[0], row[1], row[2], row[3], row[4].decode('utf-8'), row[5], row[6], row[7], row[8], row[9], row[10]))
            return ret
        except pymysql.Error as e:
            _log.error("Database server '%s' filesystem '%s' folder '%s' replica query failed, error %d: %s", server, fsname, folder, e.args[0], e.args[1])
            raise e

    def getReplicasInPool(self, poolname):
        """Method to get all the file replica for a pool"""
        try:
            self.nsdb_c.execute('''
                SELECT m.name, r.poolname,r.fs, r.host, r.sfn, m.filesize, m.gid, m.status, r.status, r.setname, r.ptime
                FROM Cns_file_replica r
                JOIN Cns_file_metadata m USING (fileid)
                WHERE r.poolname = %s
                ''', (poolname, ))
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(FileReplica(row[0], row[1], row[2], row[3], row[4].decode('utf-8'), row[5], row[6], row[7], row[8], row[9], row[10]))
            return ret
        except pymysql.Error as e:
            _log.error("Database pool '%s' replica query failed, error %d: %s", poolname, e.args[0], e.args[1])
            raise e

    def getPoolFromReplica(self, sfn):
        """Method to get the pool name from a sfn"""
        try:
            self.nsdb_c.execute('SELECT poolname FROM Cns_file_replica WHERE sfn = %s', (sfn, ))
            ret = list()
            for row in self.nsdb_c.fetchall():
                ret.append(row[0])
            return ret
        except pymysql.Error as e:
            _log.error("Database pool from replica '%s' query failed, error %d: %s", sfn, e.args[0], e.args[1])
            raise e

    def getFilesystems(self, poolname):
        """get all filesystems in a pool"""
        try:
            self.dpmdb_c.execute('SELECT poolname, server, fs, status, weight FROM dpm_fs WHERE poolname = %s', (poolname, ))
            ret = list()
            for row in self.dpmdb_c.fetchall():
                ret.append(FileSystem(row[0], row[1], row[2], row[3], row[4]))
            return ret
        except pymysql.Error as e:
            _log.error("Database query for pool '%s' filesystems failed, error %d: %s", poolname, e.args[0], e.args[1])
            raise e

    def getFilesystemsInServer(self, server):
        """get all filesystems in a server"""
        try:
            self.dpmdb_c.execute('SELECT poolname, server, fs, status, weight FROM dpm_fs WHERE server = %s', (server, ))
            ret = list()
            for row in self.dpmdb_c.fetchall():
                ret.append(FileSystem(row[0], row[1], row[2], row[3], row[4]))
            return ret
        except pymysql.Error as e:
            _log.error("Database query for server '%s' filesystems failed, error %d: %s", server, e.args[0], e.args[1])
            raise e

    def getServers(self, pool):
        """get all server in a server"""
        try:
            self.dpmdb_c.execute('SELECT server FROM dpm_fs WHERE poolname = %s', (pool, ))
            ret = list()
            for row in self.dpmdb_c.fetchall():
                ret.append(row[0])
            return ret
        except pymysql.Error as e:
            _log.error("Database query for pool '%s' servers failed, error %d: %s", pool, e.args[0], e.args[1])
            raise e

    def getPoolFromFS(self, server, fsname):
        """get the pool related to FS"""
        try:
            self.dpmdb_c.execute('SELECT poolname FROM dpm_fs WHERE server = %s AND fs = %s', (server, fsname))
            ret = list()
            for row in self.dpmdb_c.fetchall():
                ret.append(row[0])
            return ret
        except pymysql.Error as e:
            _log.error("Database query for pool from server '%s' and filesystem '%s' failed, error %d: %s", server, fsname, e.args[0], e.args[1])
            raise e

    def getGroupByGID(self, gid):
        """get groupname by gid """
        try:
            self.nsdb_c.execute('SELECT groupname FROM Cns_groupinfo WHERE gid = %s', (gid, ))
            row = self.nsdb_c.fetchone()
            return row[0]
        except pymysql.Error as e:
            _log.error("Database query for group by gid '%i' failed, error %d: %s", gid, e.args[0], e.args[1])
            raise e

    def getGroupIdByName(self, groupname):
        """get groupid by name """
        try:
            self.nsdb_c.execute('SELECT gid FROM Cns_groupinfo WHERE groupname = %s', (groupname, ))
            row = self.nsdb_c.fetchone()
            return row[0]
        except pymysql.Error as e:
            _log.error("Database query for gid by group '%s' failed, error %d: %s", groupname, e.args[0], e.args[1])
            raise e

    def find(self, pattern, folder):
        """retrieve a list of sfn matching a pattern"""
        comparison = '='
        ret = list()
        if folder:
            comparison = '!='

        try:
            self.nsdb_c.execute('''
                select name, fileid, parent_fileid, filemode from Cns_file_metadata
                where name like '%%%s%%' and filemode&32768 %s 32768
                ''' % (pymysql.escape_string(pattern), comparison))
            for row in self.nsdb_c.fetchall():
                name, fileid, parent_fileid, filemode = row
                namelist = self.getLFN(parent_fileid) + [str(name)]
                ret.append('/'.join(namelist))
            return ret
        except pymysql.Error as e:
            _log.error("Database query for file pattern '%s' failed, error %d: %s", pattern, e.args[0], e.args[1])
            raise e
        except ValueError as v:
            _log.error("Failed to evaluate pattern '%s': %s", pattern, str(v))
            raise v

    def getLFNFromSFN(self, sfn):
        """get LFN from sfn"""
        try:
            self.nsdb_c.execute('''
                SELECT parent_fileid, name
                FROM Cns_file_replica JOIN Cns_file_metadata ON Cns_file_replica.fileid = Cns_file_metadata.fileid
                WHERE Cns_file_replica.sfn = %s''', (sfn, ))
            row = self.nsdb_c.fetchone()
            if row == None:
                raise ValueError("no replica metadata")
            parent_fileid, name = row
            namelist = self.getLFN(parent_fileid) + [str(name)]
            return '/'.join(namelist)
        except pymysql.Error as e:
            _log.error("Database query for sfn '%s' failed, error %d: %s", sfn, e.args[0], e.args[1])
            return None
        except ValueError as e:
            _log.error("Failed to get LFN from sfn '%s': %s", sfn, str(e))
            return None

    def getLFNFromIno(self, fileid):
        """get LFN from sfn"""
        try:
            namelist = self.getLFN(fileid)
            return '/'.join(namelist)
        except pymysql.Error as e:
            _log.error("Database query for fileid '%s' failed, error %d: %s", fileid, e.args[0], e.args[1])
            return None
        except ValueError as e:
            _log.error("Failed to get LFN from fileid '%s': %s", fileid, str(e))
            return None

    def getLFN(self, fileid):
        """get LFN from fileid"""
        namelist = []
        parent_fileid = fileid
        while parent_fileid > 0:
            if parent_fileid not in self.dirhash:
                self.nsdb_c.execute('SELECT parent_fileid, name FROM Cns_file_metadata WHERE fileid = %s', (parent_fileid, ))
                row = self.nsdb_c.fetchone()
                if row == None:
                    raise ValueError("no parent for %s" % parent_fileid)
                if len(self.dirhash) > 100000:
                    # prevent excessive size of cached entries
                    self.dirhash = {}
                self.dirhash[parent_fileid] = list(row)
            parent_fileid, name = self.dirhash[parent_fileid]
            namelist.append(name)
        namelist.reverse() #put entries in "right" order for joining together
        return [''] + namelist[1:] #and sfn and print dpns name (minus srm bits)



# Get a filesytem information from db
class FileReplica(object):

    def __init__(self, name, poolname, fsname, host, sfn, size, gid, status, replicastatus, setname, ptime):
        self.name = name
        self.poolname = poolname
        self.fsname = fsname
        self.host = host
        self.sfn = sfn
        self.size = size
        self.gid = gid
        self.status = status
        self.replicastatus = replicastatus
        self.setname = setname
        self.pinnedtime = ptime
        self.lfn = None

    def __repr__(self):
        return "FileReplica(name=" + self.name + ", poolname=" + self.poolname + ", server=" + self.host + ", fsname=" + self.fsname + ", sfn=" + self.sfn + ", size=" + str(self.size) + ", gid=" + str(self.gid) + ", status=" + self.status + ", replicastatus=" + self.replicastatus + ", setname=" + self.setname + ", pinnedtime=" + str(self.pinnedtime) + ")"



# Get a filesytem information from db
class FileSystem(object):

    def __init__(self, poolname, server, name, status, weight):
        self.poolname = poolname
        self.server = server
        self.name = name
        self.status = status
        self.files = list()
        # Filled in by annotateFreeSpace()
        self.size = None
        self.avail = None
        self.status = status
        self.weight = weight

    def desc(self):
        """Short description of this location - i.e. a minimal human readable description of what this is"""
        return str(self.server) + str(self.name)

    def fileCount(self):
        """Support for using these to track dirs at a time"""
        return len(self.files)

    def __repr__(self):
        return "FileSystem(poolname=" + self.poolname + ", server=" + self.server + ", name=" + self.name + ", status=" + str(self.status) + ", weight=" + str(self.weight) + ", with " + str(len(self.files)) + " files and " + str(self.avail) + " 1k blocks avail)"



if __name__ == '__main__':
    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.DEBUG)

    # just simple module tests to make debugging easier
    _log.debug("start")

    DBConn.configure('auto')
    #DBConn.configure('obsolete')
    #DBConn.configure('legacy')
    #DBConn.configure('dome')
    #DBConn.configure('user', {'pass': 'secret'})

    _log.debug("test ping with cns_db")
    DBConn.get('cns_db').ping()
    _log.debug("test ping with dpm_db")
    DBConn.get('dpm_db').ping()

    _log.debug("test select with cns_db")
    with DBConn.get('cns_db').cursor() as cur:
        cur.execute("SELECT 1")
        res = cur.fetchall()
        _log.debug("database SELECT from cns_db succeeded: %s", res[0][0])
    _log.debug("test select with dpm_db")
    with DBConn.get('dpm_db').cursor() as cur:
        cur.execute("SELECT 1")
        res = cur.fetchall()
        _log.debug("database SELECT from dpm_db succeeded: %s", res[0][0])

    _log.debug("end")
