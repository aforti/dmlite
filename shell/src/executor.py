import os
import re
import json
import ssl
import subprocess # used by execute_old
import logging

# compatibility with python 2 and 3
try:
    from urllib.parse import urlparse
    from http.client import HTTPConnection, HTTPSConnection
except ImportError:
    from urlparse import urlparse
    from httplib import HTTPConnection, HTTPSConnection


_log = logging.getLogger('dmlite-shell')


class DomeCredentials(object):
    """Stores all credentials needed for a dome request"""

    def __init__(self, cert, key, capath, clientDN, clientAddress):
        self.cert = cert
        self.key = key
        self.capath = capath
        self.clientDN = clientDN
        self.clientAddress = clientAddress


# filter out all keys for which the value is None
def filterDict(params):
    newdict = {}
    for key, value in params.items():
        if value != None:
            newdict[key] = value
    return newdict


class DomeTalker(object):
    """Issues requests to Dome"""

    @staticmethod
    def build_url(url, command):
        while url.endswith("/"):
            url = url[:-1]
        return "{0}/command/{1}".format(url, command)

    def __init__(self, creds, uri, verb, cmd):
        self.creds = creds
        self.uri = uri
        self.verb = verb
        self.cmd = cmd

    def execute_old(self, data):
        _log.debug("talk with %s %s" % (self.cmd, data))

        cmd = ["davix-http"]
        if self.creds.cert:
            cmd += ["--cert", self.creds.cert]
        if self.creds.key:
            cmd += ["--key", self.creds.key]
        if self.creds.clientDN:
            cmd += ["-H", "remoteclientdn: {0}".format(self.creds.clientDN)]
        if self.creds.clientAddress:
            cmd += ["-H", "remoteclienthost: {0}".format(self.creds.clientAddress)]
        cmd += ["-H", "remoteclient: {0}".format("root")]
        cmd += ["-H", "remoteclientgroups: {0}".format(0)]
        if self.creds.capath:
            cmd += ["--capath", self.creds.capath]

        cmd += ["--data", json.dumps(filterDict(data))]
        cmd += ["--request", self.verb]

        cmd.append(DomeTalker.build_url(self.uri, self.cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out = proc.communicate()[0].decode('utf-8')
        if "(Davix::HttpRequest) Error:" in out:
            return (out, 500)
        return (out, 200)

    def execute_new(self, params):
        _log.debug("talk with %s %s" % (self.cmd, params))

        url = urlparse(DomeTalker.build_url(self.uri, self.cmd))

        hdrs = {}
        #hdrs['Content-Type'] = 'application/json'
        if self.creds.clientDN:
            hdrs['remoteclientdn'] = self.creds.clientDN
        if self.creds.clientAddress:
            hdrs['remoteclienthost'] = self.creds.clientAddress
        hdrs['remoteclient'] = 'root'
        hdrs['remoteclientgroups'] = '0'

        body = json.dumps(filterDict(params))

        data = 'Unexpected error calling DPM DOME'
        status = 500

        redirects = 0
        max_redirects = 3
        method = self.verb
        conn = {}

        try:
            while True:
                if redirects >= max_redirects:
                    raise Exception("Too many redirections")

                scheme = url.scheme
                host = url.hostname
                port = url.port if url.port else 1094
                path = "%s?%s" % (url.path, url.query) if url.query else url.path

                if conn.get('scheme') != scheme or conn.get('host') != host or conn.get('port') != port:
                    if conn.get('conn'):
                        conn['conn'].close()

                    if url.scheme == 'https':
                        # configure SSL context
                        context = ssl.create_default_context()
                        if self.creds.cert:
                            context.load_cert_chain(certfile=self.creds.cert, keyfile=self.creds.key)
                        context.verify_mode = ssl.CERT_REQUIRED
                        if self.creds.capath:
                            context.load_verify_locations(capath=self.creds.capath)

                        conn['conn'] = HTTPSConnection(host, port=port, context=context)

                    else:
                        conn['conn'] = HTTPConnection(host, port=port)

                    conn['scheme'] = scheme
                    conn['host'] = host
                    conn['port'] = port

                # submit HTTPS request
                _log.debug("New %s %s connection %s:%s%s", url.scheme.upper(), method, host, port, path)
                conn['conn'].request(method=method, url=path, headers=hdrs, body=body)
                response = conn['conn'].getresponse()

                if response.status in [301, 302, 303, 307]:
                    location = response.getheader('Location')
                    if not location:
                        raise Exception("HTTP redirection without Location header")

                    url = urlparse(location)
                    method = 'GET'

                    redirects += 1
                    continue

                break

            data = response.read().decode('utf-8')
            status = response.status

        except Exception as e:
            _log.error("failed to talk with DOME: %s", str(e))
            data = str(e)

        finally:
            if conn.get('conn'):
                conn['conn'].close()

        return (data, status)

    execute = execute_new if not os.path.exists('/etc/dome.dmlite-shell.davix') else execute_old


class DomeExecutor(object):
    """Wrapper around DomeTalker"""

    def __init__(self, url, cert, key, capath, clientDN, clientAddress):
        self.creds = DomeCredentials(cert, key, capath, clientDN, clientAddress)
        self.url = url

    def _simple_response(self, verb, cmd, params=None):
        if params is None: params = {}
        talker = DomeTalker(self.creds, self.url, verb, cmd)
        data, status = talker.execute(params)
        if status >= 400:
            if not data:
                return (None, "%s request failed" % cmd)
            return (None, "[HTTP%i] %s" % (status, data))
        else:
            return (data, None)

    def _json_response(self, verb, cmd, params=None):
        data, error = self._simple_response(verb, cmd, params)
        if error:
            return (None, error)

        if not data:
            return ({}, None)

        try:
            jdata = json.loads(data)
            return (jdata, None)
        except ValueError:
        #except json.JSONDecodeError as e: # python 3.5
            _log.error("Unable to parse %s %s(%s) response: %s", verb, cmd, str(params), str(data))
            return (None, "Failed to parse %s response" % cmd)

    def info(self):
        data, err = self._simple_response('GET', 'dome_info')
        if err:
            return (None, err)

        ret = {'data': data}
        for line in data.split('\n'):
            res = re.match(r'^dome \[(.*?)\] running as (\S+)', line)
            if res:
                ret['version'] = res.group(1)
                ret['flavor'] = res.group(2)
            res = re.match(r'^Total: (\d+) .*Free: (\d+)', line)
            if res:
                ret['space_total'] = int(res.group(1))
                ret['space_free'] = int(res.group(2))
            res = re.match(r'^Server PID: (\d+)', line)
            if res:
                ret['pid'] = int(res.group(1))
            res = re.match(r'^Request rate: (.*?)Hz \(Peak: (.*?)Hz\)', line)
            if res != None:
                ret['request_rate'] = float(res.group(1))
                ret['request_rate_peak'] = float(res.group(2))
                res = re.search(r'DB queries: (.*?)Hz', line)
                if res != None: ret['db_query_rate'] = float(res.group(1))
                res = re.search(r'DB transactions: (.*?)Hz', line)
                if res != None: ret['db_transaction_rate'] = float(res.group(1))
                res = re.search(r'DB avg transaction time: (.*?)ms', line)
                if res != None: ret['db_transaction_time_ms'] = float(res.group(1))
                res = re.search(r'Intercluster messages: (.*?)Hz', line)
                if res != None: ret['intercluster_rate'] = float(res.group(1))
            res = re.match(r'^Queue checksum: (\d+) .*Running checksum: (\d+) .*Waiting checksum: (\d+) .*Queue file pull: (\d+) .*Running file pull: (\d+) .*Waiting file pull: (\d+)', line)
            if res != None:
                ret['checksum_running'] = int(res.group(2))
                ret['checksum_waiting'] = int(res.group(3))
                ret['checksum_other'] = int(res.group(1)) - ret['checksum_running'] - ret['checksum_waiting']
                ret['filepull_running'] = int(res.group(5))
                ret['filepull_waiting'] = int(res.group(6))
                ret['filepull_other'] = int(res.group(4)) - ret['filepull_running'] - ret['filepull_waiting']
            res = re.match(r'^Tasks total: (\d+) .*Tasks running: (\d+) .*Tasks finished: (\d+)', line)
            if res != None:
                ret['tasks_running'] = int(res.group(2))
                ret['tasks_finished'] = int(res.group(3))
                ret['tasks_other'] = int(res.group(1)) - ret['tasks_running'] - ret['tasks_finished']

        return (ret, None)

    def getconfig(self, name):
        return self._simple_response('GET', 'dome_config', {"name": name})

    def setconfig(self, name, value=None):
        return self._simple_response('POST', 'dome_config', {"name": name, "value": value})

    def addpool(self, pool, defsize=None, stype=None):
        return self._simple_response('POST', 'dome_addpool', {"poolname": pool, "pool_defsize": defsize, "pool_stype": stype})

    def modifypool(self, pool, defsize=None, stype=None):
        return self._simple_response('POST', 'dome_modifypool', {"poolname": pool, "pool_defsize": defsize, "pool_stype": stype})

    def rmpool(self, pool):
        return self._simple_response('POST', 'dome_rmpool', {"poolname": pool})

    def addfstopool(self, server, fs, pool, status):
        return self._simple_response('POST', 'dome_addfstopool', {"server": server, "fs": fs, "poolname": pool, "status": status})

    def modifyfs(self, server, fs, pool, status):
        return self._simple_response('POST', 'dome_modifyfs', {"server": server, "fs": fs, "poolname": pool, "status": status})

    def rmfs(self, server, fs):
        return self._simple_response('POST', 'dome_rmfs', {"server": server, "fs": fs})

    def getspaceinfo(self):
        return self._json_response('GET', 'dome_getspaceinfo')

    def statpool(self, pool):
        return self._json_response('GET', 'dome_statpool', {"poolname": pool})

    def getquotatoken(self, lfn, getparentdirs, getsubdirs):
        return self._json_response('GET', 'dome_getquotatoken', {"path": lfn, "getsubdirs": getsubdirs, "getparentdirs": getparentdirs})

    def setquotatoken(self, lfn, pool, space, desc, groups):
        return self._simple_response('POST', 'dome_setquotatoken', {"path": lfn, "poolname": pool, "quotaspace": space, "description": desc, "groups": groups})

    def modquotatoken(self, s_token, lfn, pool, space, desc, groups):
        return self._simple_response('POST', 'dome_modquotatoken', {"tokenid": s_token, "path": lfn, "poolname": pool, "quotaspace": space, "description": desc, "groups": groups})

    def getdirspaces(self, lfn):
        return self._json_response('GET', 'dome_getdirspaces', {"path": lfn})

    def delquotatoken(self, lfn, pool):
        return self._simple_response('POST', 'dome_delquotatoken', {"path": lfn, "poolname": pool})

    def chooseserver(self, lfn):
        return self._json_response('GET', 'dome_chooseserver', {"lfn": lfn})

    def getreplicainfo(self, replicaid=None, rfn=None):
        return self._json_response('GET', 'dome_getreplicainfo', {"replicaid": replicaid, "rfn": rfn})

    def getreplicavec(self, lfn):
        return self._json_response('GET', 'dome_getreplicavec', {"lfn": lfn})

    def updatereplica(self, rfn, replicaid, status, type, setname, xattr):
        return self._simple_response('POST', 'dome_updatereplica', {"rfn": rfn, "replicaid": replicaid, "status": status, "type": type, "setname": setname, "xattr": xattr})

    def accessreplica(self, rfn, mode):
        return self._simple_response('GET', 'dome_accessreplica', {"rfn": rfn, "mode": mode})

    def addreplica(self, rfn, status, type, setname, xattr):
        return self._simple_response('POST', 'dome_addreplica', {"rfn": rfn, "status": status, "type": type, "setname": setname, "xattr": xattr})

    def delreplica(self, server, pfn):
        return self._simple_response('POST', 'dome_delreplica', {"server": server, "pfn": pfn})

    def getstatinfo(self, server=None, pfn=None, rfn=None, lfn=None):
        return self._json_response('GET', 'dome_getstatinfo', {"server": server, "pfn": pfn, "rfn": rfn, "lfn": lfn})

    def statpfn(self, pfn):
        return self._json_response('GET', 'dome_statpfn', {"pfn": pfn})

    def pfnrm(self, pfn):
        return self._simple_response('POST', 'dome_pfnrm', {"pfn": pfn})

    def access(self, lfn, mode):
        return self._json_response('GET', 'dome_access', {"path": lfn, "mode": mode})

    def getdir(self, lfn):
        return self._json_response('GET', 'dome_getdir', {"path": lfn})

    def makedir(self, lfn, mode=0o0755):
        return self._simple_response('POST', 'dome_makedir', {"path": lfn, "mode": mode})

    def removedir(self, lfn):
        return self._simple_response('POST', 'dome_removedir', {"path": lfn})

    def create(self, lfn, mode=0o0644):
        return self._simple_response('POST', 'dome_create', {"path": lfn, "mode": mode})

    def unlink(self, lfn, ignorebrokenfs=False, ignorereadonlyfs=True, ignorefiledeletionerr=False):
        return self._simple_response('POST', 'dome_unlink', {"lfn": lfn, "ignorebrokenfs": ignorebrokenfs, "ignorereadonlyfs": ignorereadonlyfs, "ignorefiledeletionerr": ignorefiledeletionerr})

    def get(self, lfn):
        return self._json_response('GET', 'dome_get', {"lfn": lfn})

    def put(self, lfn, pool=None, server=None, fs=None, additionalreplica=False):
        return self._json_response('POST', 'dome_put', {"lfn": lfn, "pool": pool, "host": server, "fs": fs, "additionalreplica": "true" if additionalreplica else "false"})

    def putdone(self, server, pfn, size=0, checksumtype=None, checksumvalue=None):
        return self._simple_response('POST', 'dome_putdone', {"server": server, "pfn": pfn, "size": size, "checksumtype": checksumtype, "checksumvalue": checksumvalue})

    def chksum(self, checksumtype, lfn, pfn=None, force=False):
        return self._simple_response('GET', 'dome_chksum', {"checksum-type": checksumtype, "lfn": lfn, "pfn": pfn, "force-recalc": force})

    def chksumstatus(self, checksumtype, lfn, pfn, status, checksum, update_lfn=False, reason=None):
        return self._simple_response('POST', 'dome_chksumstatus', {"checksum-type": checksumtype, "lfn": lfn, "pfn": pfn, "status": status, "checksum": checksum, "update-lfn-checksum": update_lfn, "reason": reason})

    def dochksum(self, checksumtype, lfn, pfn):
        return self._simple_response('POST', 'dome_dochksum', {"checksum-type": checksumtype, "lfn": lfn, "pfn": pfn})

    def setchecksum(self, lfn, checksumtype, checksumvalue):
        return self._simple_response('POST', 'dome_setchecksum', {"lfn": lfn, "checksum-type": checksumtype, "checksum-value": checksumvalue})

    def rename(self, oldlfn, newlfn):
        return self._simple_response('POST', 'dome_rename', {"oldpath": oldlfn, "newpath": newlfn})

    def symlink(self, lfn, target):
        return self._simple_response('POST', 'dome_symlink', {"link": lfn, "target": target})

    def readlink(self, lfn):
        return self._json_response('GET', 'dome_readlink', {"lfn": lfn})

    def setowner(self, lfn, uid, gid):
        return self._simple_response('POST', 'dome_setowner', {"path": lfn, "uid": uid, "gid": gid})

    def setmode(self, lfn, mode):
        return self._simple_response('POST', 'dome_setmode', {"path": lfn, "mode": mode})

    def setutime(self, lfn, actime, modtime):
        return self._simple_response('POST', 'dome_setutime', {"path": lfn, "actime": actime, "modtime": modtime})

    def setsize(self, lfn, size):
        return self._simple_response('POST', 'dome_setsize', {"path": lfn, "size": size})

    def setacl(self, lfn, acl):
        return self._simple_response('POST', 'dome_setacl', {"path": lfn, "acl": acl})

    def updatexattr(self, lfn=None, fileid=None, xattr=None):
        return self._simple_response('POST', 'dome_updatexattr', {"lfn": lfn, "fileid": fileid, "xattr": xattr})

    def getcomment(self, lfn=None, fileid=None):
        return self._json_response('GET', 'dome_getcomment', {"lfn": lfn, "fileid": fileid})

    def setcomment(self, lfn=None, fileid=None, comment=None):
        return self._json_response('POST', 'dome_setcomment', {"lfn": lfn, "fileid": fileid, "comment": comment})

    def newuser(self, username):
        return self._simple_response('POST', 'dome_newuser', {"username": username})

    def getuser(self, userid=None, username=None):
        return self._json_response('GET', 'dome_getuser', {"userid": userid, "username": username})

    def getusersvec(self):
        return self._json_response('GET', 'dome_getusersvec')

    def updateuser(self, userid=None, username=None, banned=None, xattr=None):
        return self._simple_response('POST', 'dome_updateuser', {"userid": userid, "username": username, "banned": banned, "xattr": xattr})

    def deleteuser(self, username):
        return self._simple_response('POST', 'dome_deleteuser', {"username": username})

    def newgroup(self, groupname):
        return self._simple_response('POST', 'dome_newgroup', {"groupname": groupname})

    def getgroup(self, groupid=None, groupname=None):
        return self._json_response('GET', 'dome_groupuser', {"groupid": groupid, "groupname": groupname})

    def getgroupsvec(self):
        return self._json_response('GET', 'dome_getgroupsvec')

    def updategroup(self, groupid=None, groupname=None, banned=None, xattr=None):
        return self._simple_response('POST', 'dome_updategroup', {"groupid": groupid, "groupname": groupname, "banned": banned, "xattr": xattr})

    def deleteroup(self, groupname):
        return self._simple_response('POST', 'dome_deletegroup', {"groupname": groupname})

    def getidmap(self, username, groupnames=None):
        return self._json_response('GET', 'dome_getidmap', {"username": username, "groupnames": groupnames})



if __name__ == '__main__':
    import sys
    import socket
    import M2Crypto

    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.DEBUG)

    dome_host = socket.getfqdn()
    dome_port = 1094
    #dome_port = 1095
    dome_urlprefix = '/domehead'
    #dome_urlprefix = '/domedisk'
    dome_cert = '/etc/grid-security/dpmmgr/dpmcert.pem'
    dome_key = '/etc/grid-security/dpmmgr/dpmkey.pem'
    dome_capath = '/etc/grid-security/certificates/'

    dome_x509 = M2Crypto.X509.load_cert(dome_cert, M2Crypto.X509.FORMAT_PEM)
    dome_hostDN = "/{0}".format(dome_x509.get_subject().as_text(flags=M2Crypto.m2.XN_FLAG_SEP_MULTILINE).replace('\n', '/'))
    dome_command_base = "https://{0}:{1}{2}".format(dome_host, dome_port, dome_urlprefix)

    _log.debug("talk with DOME using %s", DomeTalker.execute.__name__)

    executor = DomeExecutor(dome_command_base, dome_cert, dome_key, dome_capath, dome_hostDN, dome_host)

    data, error = executor.info()
    if error:
        raise Exception("Unable to get DPM DOME info ({0})".format(error))
    print(data)

    data, error = executor.getspaceinfo()
    if error:
        raise Exception("Unable to get DPM DOME storage info ({0})".format(error))
    print(data)
