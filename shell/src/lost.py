# Find and deal with DPM lost and dark files (DB vs. filesystem inconsistencies)
#
# usage:
#   python lost.py --help
# examples:
#   python lost.py --verbose --processes 10 --stat-types=dark --fix-dark --all --include-fs-rdonly &> dpm-cleaner.out
#
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import re
import io
import sys
import time
import random
import signal
import select
import socket
import multiprocessing
import logging, logging.handlers

import M2Crypto

from .dbutils import DBConn
from .executor import DomeExecutor

# compatibility for existing SLC6, CentOS7, CentOS8 packages
try:
    import pymysql.cursors as pymysql_cursors
except ImportError:
    import MySQLdb.cursors as pymysql_cursors

try:
    import paramiko
except ImportError:
    sys.exit("Could not import ssh library module. Please install the paramiko rpm.")


__author__ = 'Petr Vokac'
__date__ = 'March 2018'
__version = '0.0.1'

_log = logging.getLogger('dmlite-shell')

DEFAULT_HOST = socket.getfqdn()
DEFAULT_HEAD_PORT = 1094
DEFAULT_DOME_CONFIG = '/etc/domehead.conf'
DEFAULT_URLPREFIX = '/domehead/'
DEFAULT_CERT = '/etc/grid-security/dpmmgr/dpmcert.pem'
DEFAULT_KEY = '/etc/grid-security/dpmmgr/dpmkey.pem'
DEFAULT_CAPATH = '/etc/grid-security/certificates/'


#########################################################################
# modules applied on normal/dark/lost files
#########################################################################

class BaseModule(object):

    def __init__(self, poolname, diskserver, diskserverfs, run = []):
        self._status = 'init'
        self._poolname = poolname
        self._diskserver = diskserver
        self._diskserverfs = diskserverfs
        self.run_normal = 'normal' in run
        self.run_dark = 'dark' in run
        self.run_lost = 'lost' in run
        self._ssh = None

    def start(self, ssh):
        if self._status not in [ 'init' ]:
            raise Exception("invalid state transition (%s -> started)" % self._status)
        self._status = 'started'
        self._ssh = ssh

    def normal(self, filename, data):
        pass

    def dark(self, filename, data):
        pass

    def lost(self, filename, data):
        pass

    def finish(self):
        if self._status not in [ 'started' ]:
            raise Exception("invalid state transition (%s -> finished)" % self._status)
        self._status = 'finished'
        self._ssh = None

        return {}



class TestModule(BaseModule):
    """Example of module used to deal with consistency check output."""

    def __init__(self, poolname, diskserver, diskserverfs, run=['normal','dark','lost']):
        super(TestModule, self).__init__(poolname, diskserver, diskserverfs, run)
        print("%s.__init__(%s, %s, %s)" % (self.__class__.__name__, diskserver, diskserverfs, run))

    def start(self, ssh):
        super(TestModule, self).start(ssh)
        print("%s.start(%s)" % (self.__class__.__name__, ssh))

    def normal(self, filename, data):
        #if not self.run_normal: return
        super(TestModule, self).normal(filename, data)
        print("%s.normal(%s, %s)" % (self.__class__.__name__, filename, data))

    def dark(self, filename, data):
        #if not self.run_dark: return
        super(TestModule, self).dark(filename, data)
        print("%s.dark(%s, %s)" % (self.__class__.__name__, filename, data))

    def lost(self, filename, data):
        #if not self.run_lost: return
        super(TestModule, self).lost(filename, data)
        print("%s.lost(%s, %s)" % (self.__class__.__name__, filename, data))

    def finish(self):
        ret = super(TestModule, self).finish()
        print("%s.finish()" % (self.__class__.__name__, ))
        return ret



class StatModule(BaseModule):
    """Get stat details about diskpool files."""

    def __init__(self, poolname, diskserver, diskserverfs, run=['dark']):
        super(StatModule, self).__init__(poolname, diskserver, diskserverfs, run)

    def start(self, ssh):
        super(StatModule, self).start(ssh)
        self._cnt = 0
        self._done = 0
        self._time = 0

    def finish(self):
        ret = super(StatModule, self).finish()

        ret['cnt'] = getattr(self, '_cnt', 0)
        ret['done'] = getattr(self, '_done', 0)
        ret['time'] = getattr(self, '_time', 0)

        _log.info("%s:%s stat time %0.1fs (cnt: %i, done: %i)", self._diskserver, self._diskserverfs, ret['time'], ret['cnt'], ret['done'])

        return ret

    def normal(self, filename, data):
        self._cnt += 1

        start = time.time()
        stat = self._stat(filename)
        self._time += time.time() - start

        if stat != None:
            self._done += 1
            data['stat'] = stat

    def dark(self, filename, data):
        self._cnt += 1

        start = time.time()
        stat = self._stat(filename)
        self._time += time.time() - start

        if stat != None:
            self._done += 1
            data['stat'] = stat

    def _stat(self, filename):
        ret = None
        stat_filename = filename.replace(r"'", r"'\''")
        stat_cmd = "stat --format='%%i;%%h;%%f;%%s;%%U;%%G;%%X;%%Y;%%Z' '%s'" % stat_filename

        try:
            exit_code, stdout_lines, stderr_lines = run_ssh_command(self._ssh, stat_cmd)

            if exit_code != 0:
                raise Exception("non-zero exit code %i" % exit_code)
            if len(stdout_lines) == 0:
                raise Exception("no output")

            statout = stdout_lines[0].strip().split(';')
            if len(statout) != 9:
                raise Exception("ivalid output %s" % str(statout))

            inode, links, mode, size, user, group, atime, mtime, ctime = statout
            ret = (int(inode), int(links), mode, int(size), user, group, int(atime), int(mtime), int(ctime))

        except Exception as e:
            _log.error("%s:%s stat '%s' failed: %s", self._diskserver, self._diskserverfs, stat_filename, str(e))

        return ret



class ChksumModule(BaseModule):
    """Calculate checksum of the diskpool files."""

    def __init__(self, poolname, diskserver, diskserverfs, run=['normal'], method='auto'):
        super(ChksumModule, self).__init__(poolname, diskserver, diskserverfs, run)

        if method not in ['auto', 'dome-checksum-adler32', 'dome-checksum-crc32', 'dome-checksum-md5', 'xrdadler32', 'cksum', 'md5sum', 'sha1sum', 'sha256sum', 'sha512sum']:
            raise Exception("unknown checksum method '%s'" % method)

        self._method = method

    def start(self, ssh):
        super(ChksumModule, self).start(ssh)
        self._cnt = 0
        self._done = 0
        self._time = 0

    def finish(self):
        ret = super(ChksumModule, self).finish()

        ret['cnt'] = getattr(self, '_cnt', 0)
        ret['done'] = getattr(self, '_done', 0)
        ret['time'] = getattr(self, '_time', 0)

        _log.info("%s:%s chksum time %0.1fs (cnt: %i, done: %i)", self._diskserver, self._diskserverfs, ret['time'], ret['cnt'], ret['done'])

        return ret

    def normal(self, filename, data):
        self._cnt += 1

        method = self._method
        if method == 'auto':
            method = {
                'AD': 'dome-checksum-adler32',
                'CD': 'dome-checksum-crc32',
                'MD': 'dome-checksum-md5',
            }.get(data.get('metadata_csumtype', ''), 'xrdadler32')

        start = time.time()
        chksum = self._chksum(filename, method)
        self._time += time.time() - start

        if chksum != None:
            self._done += 1
            data['chksum'] = chksum
            data["chksum.%s" % method] = chksum
            if method in ['dome-checksum-adler32', 'xrdadler32']:
                data['chksum.adler32'] = chksum
            if method in ['dome-checksum-crc32', 'cksum']:
                data['chksum.crc32'] = chksum
            if method in ['dome-checksum-md5', 'md5sum']:
                data['chksum.md5'] = chksum

    def dark(self, filename, data):
        self._cnt += 1

        method = "xrdadler32" if self._method == 'auto' else self._method

        start = time.time()
        chksum = self._chksum(filename, method)
        self._time += time.time() - start

        if chksum != None:
            self._done += 1
            data['chksum'] = chksum
            data["chksum.%s" % method] = chksum
            if method in ['dome-checksum-adler32', 'xrdadler32']:
                data['chksum.adler32'] = chksum
            if method in ['dome-checksum-crc32', 'cksum']:
                data['chksum.crc32'] = chksum
            if method in ['dome-checksum-md5', 'md5sum']:
                data['chksum.md5'] = chksum

    def _chksum(self, filename, method):
        ret = None
        chksum_filename = filename.replace(r"'", r"'\''")
        if method.startswith('dome-checksum-'):
            chksum_cmd = "dome-checksum %s '%s'" % (method[len('dome-checksum-'):], chksum_filename)
        elif method in ['xrdadler32', 'cksum', 'md5sum', 'sha1sum', 'sha256sum', 'sha512sum']:
            chksum_cmd = "%s '%s'" % (method, chksum_filename)

        try:
            exit_code, stdout_lines, stderr_lines = run_ssh_command(self._ssh, chksum_cmd)

            if exit_code != 0:
                raise Exception("non-zero exit code %i" % exit_code)
            if len(stdout_lines) == 0:
                raise Exception("no output")

            if method.startswith('dome-checksum-'):
                ret = stdout_lines[-1].split(' ')[2].strip()
            elif method in ['xrdadler32', 'cksum', 'md5sum', 'sha1sum', 'sha256sum', 'sha512sum']:
                ret = stdout_lines[0].split(' ')[0].strip()

        except Exception as e:
            _log.error("%s:%s %s chksum '%s' failed: %s", self._diskserver, self._diskserverfs, method, chksum_filename, str(e))

        return ret



class FileModule(BaseModule):
    """Write consistency check details in the file."""

    def __init__(self, poolname, diskserver, diskserverfs, run=['normal','dark','lost'], filename=None, header=False):
        super(FileModule, self).__init__(poolname, diskserver, diskserverfs, run)

        if filename:
            self._filename = filename % {
                'POOL': poolname,
                'DISKSERVER': diskserver,
                'DISKSERVERFS': diskserverfs.lstrip('/').replace('/', '_'),
            }
        else:
            self._filename = 'stdout://'
        self._header = header
        self._fh = None
        self._summary = {}

    def start(self, ssh):
        super(FileModule, self).start(ssh)

        if self._filename == 'stdout://':
            self._fh = sys.stdout
        elif self._filename == 'stderr://':
            self._fh = sys.stderr
        elif self._filename.startswith('file://'):
            self._fh = open(self._filename[len('file://'):], "w")
        else:
            self._fh = open(self._filename, "w")

        if self._header:
            self._fh.write("#type\tserver\tfile\tdbfileid\tdbmetadataid\tdbreplicaid\tdbstatus\tdbsize\tsize\tdbchksum\tchksum\n")

    def finish(self):
        data = super(FileModule, self).finish()

        for k, v in sorted(self._summary.items()):
            _log.info("FileModule %s:%s[%s] summary %s: %s", self._diskserver, self._diskserverfs, self._poolname, k, v)

        if self._fh == None:
            return
        if self._fh not in [ sys.stdout, sys.stderr ]:
            self._fh.close()
        self._fh = None

        return data

    def normal(self, filename, data):
        self._write('FILE', self._diskserver, filename, data)

    def dark(self, filename, data):
        self._write('DARK', self._diskserver, filename, data)

    def lost(self, filename, data):
        if data['parent_dir_exists']:
            self._write('LOST', self._diskserver, filename, data)
        else:
            self._write('LOSTNODIR', self._diskserver, filename, data)

    def _write(self, name, diskserver, filename, data):
        db = data.get('db', {})
        fileid = db.get('metadata_fileid', db.get('replica_fileid', ''))
        mrowid = db.get('metadata_rowid', '')
        rrowid = db.get('replica_rowid', '')
        status = db.get('replica_status', '')
        metasize = db.get('metadata_filesize', '')
        statsize = data.get('stat', (0, 0, '', 0, '', '', 0, 0, 0))[3]
        metasum = "%s:%s" % (data['metadata_csumtype'], data.get('metadata_csumvalue', '')) if 'metadata_csumtype' in data else data.get('metadata_csumvalue', '')
        chksum = data.get('chksum', '')

        self._fh.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (name, diskserver, filename, fileid, mrowid, rrowid, status, metasize, statsize, metasum, chksum))

        self._summary["nfiles %s" % name] = self._summary.get("nfiles %s" % name, 0) + 1
        if 'metadata_filesize' in data:
            try:
                self._summary["metasize %s" % name] = self._summary.get("metasize %s" % name, 0) + int(metasize)
            except ValueError as e:
                _log.debug("invalid meta size: %s ... %s", filename, metasize)
        if 'stat' in data:
            try:
                self._summary["statsize %s" % name] = self._summary.get("statsize %s" % name, 0) + int(statsize)
            except ValueError as e:
                _log.debug("invalid stat size: %s ... %s", filename, statsize)



class FixModule(BaseModule):
    """Fix problems found by consistency checks."""

    def __init__(self, poolname, diskserver, diskserverfs, run=['dark','lost'], executor=None):
        super(FixModule, self).__init__(poolname, diskserver, diskserverfs, run)
        self._executor = executor

    def dark(self, filename, data):
        rm_cmd = "rm '%s'" % filename.replace(r"'", r"'\''")
        try:
            exit_code, stdout_lines, stderr_lines = run_ssh_command(self._ssh, rm_cmd)

            if exit_code != 0:
                raise Exception("non-zero exit code %i" % exit_code)

        except Exception as e:
            _log.error("%s:%s remove dark '%s' failed: %s", self._diskserver, self._diskserverfs, filename, str(e))

    def lost(self, filename, data):
        status = data.get('status', '')
        if status != '-':
            _log.info("skipping replica %s:%s with status %s", self._diskserver, filename, status)
            return

        if data['parent_dir_exists']:
            #dpm.dpm_delreplica("%s:%s" % (self._diskserver, filename))
            if self._executor:
                _, err = self._executor.delreplica(self._diskserver, filename)
                if err:
                    _log.warn("unable to delete replica %s:%s: %s", self._diskserver, filename, err)
            else:
                _log.debug("executor not defined, not deleting replica %s:%s", self._diskserver, filename)
        else:
            _log.warn("replica directory missing, skipping delreplica %s:%s", self._diskserver, filename)



#########################################################################
#########################################################################

# execute command on remote machine and deal with buffering
# for commands that produce huge stdout/stderr
def run_ssh_command(client, cmd, timeout=0):
    _log.debug("%s running command `%s` (timeout=%s)", str(client.get_transport().getpeername()[0]), cmd, timeout)

    channel = client.get_transport().open_session()
    channel.exec_command(cmd)
    channel.shutdown_write()

    stdout_bytes = io.BytesIO()
    stderr_bytes = io.BytesIO()
    while not channel.eof_received or channel.recv_ready() or channel.recv_stderr_ready():
        readq, _, _ = select.select([channel], [], [], timeout)
        for c in readq:
            if c.recv_ready():
                stdout_bytes.write(channel.recv(len(c.in_buffer)))
            if c.recv_stderr_ready():
                stderr_bytes.write(channel.recv(len(c.in_buffer)))

    exit_status = channel.recv_exit_status()
    channel.shutdown_read()
    channel.close()

    stdout_bytes.seek(0)
    stderr_bytes.seek(0)
    stdout_wrapper = io.TextIOWrapper(stdout_bytes, encoding='utf-8')
    stderr_wrapper = io.TextIOWrapper(stderr_bytes, encoding='utf-8')
    stdout_lines = stdout_wrapper.readlines()
    stderr_lines = stderr_wrapper.readlines()

    _log.debug("%s finished command `%s`: exit_status %i, stdout lines %i, stderr lines %i", str(client.get_transport().getpeername()[0]), cmd, exit_status, len(stdout_lines), len(stderr_lines))

    return (exit_status, stdout_lines, stderr_lines)



def diskserverfs_check(diskserver, diskserverfs, modules = []):
    _log.debug("diskserverfs_check(%s, %s, %s)", diskserver, diskserverfs, [x.__class__.__name__ for x in modules])

    # login to diskserver
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(diskserver, username='root', allow_agent=True, look_for_keys=True)
    except Exception as e:
        _log.error("could not ssh to %s for file listing. Passwordless ssh needed to target disk server: %s", diskserver, str(e))
        return 'SSH connection failed'

    for module in modules:
        module.start(ssh)

    # obtain list of all directories on given filesystem
    _log.debug("%s:%s find directories", diskserver, diskserverfs)
    try:
        cmd = "find '%s' -type d" % diskserverfs.replace(r"'", r"'\''")
        exit_code, stdout_lines, stderr_lines = run_ssh_command(ssh, cmd)
        if exit_code != 0:
            raise Exception("find failed with exit code %i" % exit_code)
        if len(stderr_lines) > 0:
            raise Exception("find returned non-empty stderr with %i lines (first error): %s" % (len(stderr_lines), stderr_lines[0]))
        diskdirs = set([ x.rstrip('\n') for x in stdout_lines ])
    except Exception as e:
        _log.error("remote directory find over SSH failed on %s:%s: %s", diskserver, diskserverfs, str(e))
        return 'SSH read dirlist failed'

    # obtain list of all files on given filesystem
    _log.debug("%s:%s find files", diskserver, diskserverfs)
    try:
        cmd = "find '%s' -type f" % diskserverfs.replace(r"'", r"'\''")
        exit_code, stdout_lines, stderr_lines = run_ssh_command(ssh, cmd)
        if exit_code != 0:
            raise Exception("find failed with exit code %i" % exit_code)
        if len(stderr_lines) > 0:
            raise Exception("find returned non-empty stderr with %i lines (first error): %s" % (len(stderr_lines), stderr_lines[0]))
        diskfiles = set([ x.rstrip('\n') for x in stdout_lines ])
    except Exception as e:
        _log.error("remote file find over SSH failed on %s:%s: %s", diskserver, diskserverfs, str(e))
        return 'SSH read filelist failed'

    _log.info("%s:%s found %i directories and %i files", diskserver, diskserverfs, len(diskdirs), len(diskfiles))

    # read also all files for diskserver:diskserverfs from DPM database
    _log.debug("%s:%s read database replicas", diskserver, diskserverfs)
    conn = cursor = None
    try:
        dbfiles = {}
        conn = DBConn.new('cns_db')
        cursor = conn.cursor(pymysql_cursors.SSDictCursor)
        #cursor.execute("select sfn, rowid, fileid, status from Cns_file_replica where host = %s and fs = %s", (diskserver, diskserverfs))
        # MySQL doesn't support FULL OUTER JOIN, use UNION and two separate LEFT+RIGHT exclusive joins
        cols = [
            'replica.fileid', 'replica.rowid', 'replica.status', 'replica.sfn',
            'metadata.rowid', 'metadata.filesize', 'metadata.csumtype', 'metadata.csumvalue',
        ]
        colstr = ', '.join(["{0} AS `{1}`".format(x, x.replace('.', '_', 1)) for x in cols])
        sql = """SELECT SQL_BIG_RESULT {0}
FROM Cns_file_metadata AS metadata LEFT JOIN Cns_file_replica AS replica ON metadata.fileid = replica.fileid
WHERE host = %s and fs = %s
UNION ALL
SELECT SQL_BIG_RESULT {0}
FROM Cns_file_metadata AS metadata RIGHT JOIN Cns_file_replica AS replica ON metadata.fileid = replica.fileid
WHERE replica.rowid IS NULL AND host = %s and fs = %s
""".format(colstr)
        cursor.execute(sql, (diskserver, diskserverfs, diskserver, diskserverfs))
        for row in cursor:
            if row['replica_rowid'] == None:
                _log.warn("no replica for metadata: %s", str(row))
                continue
            if row['metadata_rowid'] == None:
                _log.warn("no metadata for replica: %s", str(row))
            filename = row['replica_sfn'].decode('utf-8').split(':', 1)[1]
            dbfiles[filename] = row
    except Exception as e:
        _log.error("unable to get data from database for %s:%s: %s", diskserver, diskserverfs, str(e))
        return 'DB read failed'
    finally:
        if cursor: cursor.close()
        if conn: conn.close()

    _log.info("%s:%s database has %i entries", diskserver, diskserverfs, len(dbfiles))

    # find lost files (files in DPM database with missing file on diskserver)
    for sfn_local in dbfiles:
        try:
            data = {'db': dbfiles[sfn_local]}
            if sfn_local in diskfiles:
                for module in modules:
                    if not module.run_normal: continue
                    module.normal(sfn_local, data)
            else:
                data['parent_dir_exists'] = os.path.dirname(sfn_local) in diskdirs
                for module in modules:
                    if not module.run_lost: continue
                    module.lost(sfn_local, data)
        except Exception as e:
            _log.error("%s:%s file -> db comparison failed for %s: %s", diskserver, diskserverfs, sfn_local, str(e))

    _log.info("%s:%s finished file -> db check", diskserver, diskserverfs)

    # find dark files (files on diskserver that have no record in DPM database)
    for sfn_local in diskfiles:
        try:
            if sfn_local in dbfiles: continue
            data = {}
            for module in modules:
                if not module.run_dark: continue
                module.dark(sfn_local, data)
        except Exception as e:
            _log.error("%s:%s db -> file comparision failed for %s: %s", diskserver, diskserverfs, sfn_local, str(e))

    _log.info("%s:%s finished db -> file check", diskserver, diskserverfs)

    ssh.close()

    for module in modules:
        module.finish()

    return 'DONE'



def process_diskserverfs(executor, poolname, diskserver, diskserverfs, fsstatus, modules):
    _log.debug("process_diskserverfs(%s, %s, %s, %s, %s)", poolname, diskserver, diskserverfs, fsstatus, [x.__class__.__name__ for x in modules])

    online = fsstatus == '0'
    if online:
        # set DPM filesystem read-only before checking for DARK/LOST data
        # not to let DPM create/delete data files diskpool and in database
        # during our attempt to find inconsistencies
        try:
            _log.debug("setting FS read-only on %s:%s", diskserver, diskserverfs)
            _, err = executor.modifyfs(diskserver, diskserverfs, poolname, 2) # read-only
            if err:
                raise Exception("dome_modifyfs failed (%s)" % str(err))
        except Exception as e:
            _log.error("unable to change FS read-only on %s:%s: %s", diskserver, diskserverfs, str(e))
            return (diskserver, diskserverfs, 'FS RDONLY failed')

    retval = 'EMPTY'
    try:
        retval = diskserverfs_check(diskserver, diskserverfs, modules)
    except Exception as e:
        _log.error("unexpected exception %s:%s: %s", diskserver, diskserverfs, str(e))
        retval = "Unexpected error %s" % str(e)
    finally:
        if online:
            # set DPM filesystem back online in case it was set read-only by this script
            try:
                _log.debug("setting FS online on %s:%s", diskserver, diskserverfs)
                _, err = executor.modifyfs(diskserver, diskserverfs, poolname, 0) # online
                if err:
                    raise Exception("dome_modifyfs failed (%s)" % str(err))
            except Exception as e:
                _log.error("unable to change FS read-only on %s:%s: %s", diskserver, diskserverfs, str(e))
                return (diskserver, diskserverfs, 'FS ONLINE failed')

    return (diskserver, diskserverfs, retval)



def run(tasks, modules, executor, processes=10, timeout=12*60*60):
    _log.info("%d filesystems checked with %d processes", len(tasks), processes)

    starttime = time.time()

    # random order of disknodefs not to stress one disknode
    # with up to options.processes at the same time
    random.shuffle(tasks)

    # disable SIGINT and SIGTERM in child processes
    original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
    #original_sigterm_handler = signal.signal(signal.SIGTERM, signal.SIG_IGN)
    pool = multiprocessing.Pool(processes=processes)
    #signal.signal(signal.SIGTERM, original_sigterm_handler)
    signal.signal(signal.SIGINT, original_sigint_handler)

    futures = []
    for poolname, diskserver, diskserverfs, fsstatus in tasks:
        # initialize modules
        modinsts = []
        for mclass, mrun, mparams in modules:
            modinst = mclass(poolname, diskserver, diskserverfs, run=mrun, **mparams)
            modinsts.append(modinst)

        # run constency check asynchronoutsly in separate processes
        future = pool.apply_async(process_diskserverfs, (executor, poolname, diskserver, diskserverfs, fsstatus, modinsts), callback=lambda x: _log.info("finished check: %s", x))
        futures.append(future)

    try:
        timed_out = False
        for future in futures:
            time_remaining = timeout+10 - (time.time() - starttime)
            if time_remaining > 0:
                future.wait(time_remaining)
            else:
                timed_out = True
                break

        if timed_out:
            _log.error("childs terminated, reached global script run timeout %i", timeout)
            pool.terminate()
        else:
            pool.close()

    except KeyboardInterrupt as e:
        _log.info("process terminated: %s", str(e))

        pool.terminate()

    pool.join()



def parseConfig(filename):
    _log.debug("parsing config file %s", filename)
    ret = {}
    with open(filename) as f:
        reComment = re.compile(r'^ *#')
        reKeyValue = re.compile(r'^(.*?):\s*(.*?)\s*$')
        for line in f.readlines():
            if reComment.match(line): continue
            res = reKeyValue.match(line)
            if res == None: continue
            k = res.group(1)
            v = res.group(2)
            ret[k] = v
    return ret



def main(argv):
    import optparse
    import getpass
    import inspect

    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.WARN)

    # parse options from command line
    def opt_set_loglevel(option, opt, value, parser):
        loglevel = option.default
        if value != None:
            loglevel = int({
                'CRITICAL': logging.CRITICAL,
                'DEBUG': logging.DEBUG,
                'ERROR': logging.ERROR,
                'FATAL': logging.FATAL,
                'INFO': logging.INFO,
                'NOTSET': logging.NOTSET,
                'WARN': logging.WARN,
                'WARNING': logging.WARNING,
            }.get(value, value))

        _log.setLevel(loglevel)
        setattr(parser.values, option.dest, loglevel)

    parser = optparse.OptionParser(usage = "usage: %prog [options] [--all | --pool poolname ] [ server1:fs1 [ server2:fs2 [ ... ] ] ]", version="%prog")
    parser.add_option("-v", "--verbose", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.DEBUG, help="set log level to DEBUG")
    parser.add_option("-q", "--quiet", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.ERROR, help="set log level to ERROR")
    parser.add_option("--log-level", dest="loglevel", action="callback", callback=opt_set_loglevel, type="string", help="set log level (default: %default)")
    parser.add_option("--log-file", dest="logfile", metavar="FILE", help="set log file (default: %default)")
    parser.add_option("--log-size", dest="logsize", type="int", default=10*1024*1024, help="maximum size of log file (default: %default)")
    parser.add_option("--log-backup", dest="logbackup", type="int", default=2, help="number of log backup files (default: %default)")
    # DOME configuration
    parser.add_option("-c", "--config", dest="config", help="DOME config file")
    parser.add_option('--host', dest='host', help="DOME host, if no DOME config given")
    parser.add_option('--port', dest='port', help="DOME port, if no DOME config given")
    parser.add_option('--urlprefix', dest='urlprefix', help="DOME base url prefix, if no config given")
    parser.add_option("--cert", dest='cert', help="DOME host certificate, if no config given")
    parser.add_option("--key", dest='key', help="DOME host key, if no config given")
    # Lost and dark processing
    parser.add_option("-a", "--all", dest='all', action="store_true", default=False, help='check all pools (default: %default)')
    parser.add_option("-p", "--pool", dest='pools', action='append', default=[], help='list of pools to check (default: %default).')
    parser.add_option("-n", "--processes", dest="processes", type="int", default=1, help="skip filesystems with RDONLY status (default: %default)")
    parser.add_option("-t", "--timeout", dest="timeout", type="int", default=12*60*60, help="consistency check timeout (default: %default)")
    parser.add_option("-r", "--include-fs-rdonly", dest="fsrdonly", action="store_true", default=False, help="include filesystems with RDONLY status (default: %default)")
    parser.add_option("-b", "--include-fs-disabled", dest="fsdisabled", action="store_true", default=False, help="include filesystems with DISABLED status (default: %default)")
    parser.add_option("--module-stat", dest="module_stat", default=None, help="stat diskpool normal or dark files (default: %default)")
    parser.add_option("--module-chksum", dest="module_chksum", default=None, help="checksum diskpool normal or dark files (default: %default)")
    parser.add_option("--module-output", dest="module_output", default=None, help="include information about normal/dark/lost in the output file (default: %default)")
    parser.add_option("--module-output-filename", dest="module_output_filename", default="stdout://", help="output filename template can include %(DISKSERVER)s, %(DISKSERVERFS)s, %(POOL)s, %(TIMESTAMP)s (default: %default)")
    parser.add_option("--module-fix", dest="module_fix", default=None, help="fix dark/lost inconsistencies - dangerous (default: %default)")

    (options, args) = parser.parse_args(argv[1:])

    if options.logfile == '-':
        _log.removeHandler(streamHandler)
        streamHandler = logging.StreamHandler(sys.stdout)
        streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(streamHandler)
    elif options.logfile != None and options.logfile != '':
        #fileHandler = logging.handlers.TimedRotatingFileHandler(options.logfile, 'midnight', 1, 4)
        fileHandler = logging.handlers.RotatingFileHandler(options.logfile, maxBytes=options.logsize, backupCount=options.logbackup)
        fileHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(fileHandler)
        _log.removeHandler(streamHandler)

    _log.info("command: %s", " ".join(argv))
    _log.info("script: %s", os.path.abspath(inspect.getfile(inspect.currentframe())))
    _log.info("user: %s", getpass.getuser())

    # validate command line options
    if len(args) == 0 and not options.all and len(options.pools) == 0:
        _log.error("wrong number of args, use `%s -h` for basic help", argv[0])
        return 1

    if not options.config and not options.host:
        _log.error("Missing required configuration options \"config\" or \"host\"")
        return 1

    config = {}
    if options.config:
        if not os.path.exists(options.config):
            _log.error("DOME configuration file %s doesn't exist", options.config)
            sys.exit(1)
        config = parseConfig(options.config)

    dome_host = options.host if options.host else DEFAULT_HOST
    dome_port = options.port if options.port else DEFAULT_HEAD_PORT
    dome_urlprefix = options.urlprefix if options.urlprefix else config.get('glb.auth.urlprefix', DEFAULT_URLPREFIX)
    if not dome_urlprefix.startswith('/'): dome_urlprefix = "/{1}".format(dome_urlprefix)
    if not dome_urlprefix.endswith('/'): dome_urlprefix = "{1}/".format(dome_urlprefix)

    dome_cert = options.cert if options.cert else config.get('glb.restclient.cli_certificate', DEFAULT_CERT)
    dome_key = options.key if options.key else config.get('glb.restclient.cli_private_key', DEFAULT_KEY)
    dome_capath = DEFAULT_CAPATH

    dome_x509 = M2Crypto.X509.load_cert(dome_cert, M2Crypto.X509.FORMAT_PEM)
    dome_hostDN = "/{0}".format(dome_x509.get_subject().as_text(flags=M2Crypto.m2.XN_FLAG_SEP_MULTILINE).replace('\n', '/'))
    dome_command_base = "https://{0}:{1}{2}".format(dome_host, dome_port, dome_urlprefix)

    executor = DomeExecutor(dome_command_base, dome_cert, dome_key, dome_capath, dome_hostDN, dome_host)

    spaceinfo, err = executor.getspaceinfo()
    if err:
        _log.error("unable to get storage space configuration: %s", str(err))
        return 1

    tasks = []
    if options.all or len(options.pools) > 0:
        for poolname, pooldata in sorted(spaceinfo['poolinfo'].items()):
            fs_online = 0
            if pooldata['poolstatus'] != '0': continue
            if not options.all and poolname not in options.pools: continue
            for server, serverdata in sorted(pooldata['fsinfo'].items()):
                for fs, fsdata in sorted(serverdata.items()):
                    if not options.fsdisabled and fsdata['fsstatus'] == '1': continue # FS_DISABLED
                    if not options.fsrdonly and fsdata['fsstatus'] == '2': continue # FS_RDONLY
                    fs_online += 1
                    tasks.append((poolname, server, fs, fsdata['fsstatus']))
            if fs_online <= options.processes:
                # TODO: this is not perfect because it doesn't prevent
                # all read-only filesystems in one storage pool
                _log.warn("%d processes could set all %d online filesystems read-only for pool %s during check", options.processes, fs_online, poolname)

    for diskfs in args:
        diskserver, diskserverfs = diskfs.split(':', 1)
        poolname = spaceinfo['fsinfo'].get(diskserver, {}).get(diskserverfs, {}).get('poolname')
        fsstatus = spaceinfo['fsinfo'].get(diskserver, {}).get(diskserverfs, {}).get('fsstatus')
        if not poolname:
            _log.warn("unable to find pool for %s:%s", diskserver, diskserverfs)
            continue
        if (poolname, diskserver, diskserverfs, fsstatus) in tasks: continue
        tasks.append((poolname, diskserver, diskserverfs, fsstatus))

    if len(tasks) == 0:
        _log.error("no filesystem found to be checked")
        return 1

    # order of modules is important, because data can be
    # passed to the module called later in the chain
    modules = []
    if options.module_stat:
        modules.append((StatModule, options.module_stat.split(','), {}))
    if options.module_chksum:
        modules.append((ChksumModule, options.module_chksum.split(','), {}))
    if options.module_output:
        modules.append((FileModule, options.module_output.split(','), {'filename': options.module_output_filename}))
    if options.module_fix:
        modules.append((FixModule, options.module_fix.split(','), {'executor': executor}))

    if len(modules) == 0:
        _log.error("no module defined")
        return 1

    starttime = time.time()

    run(tasks, modules, executor, options.processes, options.timeout)

    spaceinfo_new, err = executor.getspaceinfo()
    if err:
        _log.error("unable to get storage space configuration: %s", str(err))
        _log.warn("check online status for following servers/filesystems:")
        for server, serverdata in sorted(spaceinfo['fsinfo'].items()):
            for fs, fsdata in sorted(serverdata.items()):
                if fsdata['fsstatus'] == '0':
                    _log.warn("  %s:%s", server, fs)
        return 1

    # doublecheck that filesystems that were online at the beginning
    # are set back to online before the end of this script
    for poolname, pooldata in sorted(spaceinfo_new['poolinfo'].items()):
        if pooldata['poolstatus'] != '0': continue
        for server, serverdata in sorted(pooldata['fsinfo'].items()):
            for fs, fsdata in sorted(serverdata.items()):
                if fsdata['fsstatus'] == '0': # already back online
                    continue

                if 'fsstatus' not in spaceinfo['fsinfo'].get(server, {}).get(fs, {}):
                    _log.error("unknown %s:%s online status?!?!", server, fs)
                    continue

                if spaceinfo['fsinfo'][server][fs]['fsstatus'] != '0':
                    continue

                # set DPM filesystem back online in case it was set read-only by this script
                try:
                    _log.debug("setting FS online on %s:%s", server, fs)
                    _, err = executor.modifyfs(server, fs, poolname, 0) # online
                    if err:
                        raise Exception("dome_modifyfs failed (%s)" % str(err))
                except Exception as e:
                    _log.error("unable to change FS read-only on %s:%s: %s", server, fs, str(e))

    _log.info("total runtime: %.2fs", time.time()-starttime)

    return os.EX_OK
