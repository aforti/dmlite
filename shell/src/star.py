# Module to generate an accounting record following the EMI StAR specs
# in the version 1.2, for details see
# * http://cds.cern.ch/record/1452920/files/GFD.201.pdf
# * https://wiki.egi.eu/wiki/APEL/Storage
#
# Syntax:
#
# star-accounting [-h] [--help]
# .. to get the help screen
#
# Dependencies:
#  yum install MySQL-python python-lxml python-uuid
#
#  v1.0.0 initial release
#  v1.0.2 removed site debug printouts that were screwing up the output
#  v1.0.3 avoid summing the size fields for directories. Fixes EGI doublecounting
#  v1.0.4 marks the report as regarding the past period, not the next one; parse
#         the DPM group names into VO and role
#  v1.3.0 Petr Vokac (petr.vokac@cern.ch), February 7, 2019
#         * replace SQL join with simple queries to improve performance
#         * compatibility with python 3
#  v1.4.0 Petr Vokac (petr.vokac@cern.ch), June 28, 2020
#         * integrated with dmlite-shell
#
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import socket
import base64
import io
import lxml.builder as lb
from lxml import etree
import uuid
import datetime
import logging

import ssl
import json
from io import BytesIO

try:
    from urllib.parse import urlparse, urljoin
    import http.client as http_client
except ImportError:
    from urlparse import urlparse, urljoin
    import httplib as http_client

from M2Crypto import BIO, Rand, SMIME

from .dbutils import DBConn


__version__ = '1.4.0'
__author__ = 'Fabrizio Furano'

_log = logging.getLogger('dmlite-shell')

SR_NAMESPACE = "http://eu-emi.eu/namespaces/2011/02/storagerecord"
SR = "{%s}" % SR_NAMESPACE
NSMAP = {"sr": SR_NAMESPACE}



def addrecord(xmlroot, hostname, group, user, site, filecount, resourcecapacityused, logicalcapacityused, validduration, recordid=None):
    # update XML
    rec = etree.SubElement(xmlroot, SR+'StorageUsageRecord')
    rid = etree.SubElement(rec, SR+'RecordIdentity')
    rid.set(SR+"createTime",
            datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))

    if hostname:
        ssys = etree.SubElement(rec, SR+"StorageSystem")
        ssys.text = hostname

    recid = recordid
    if not recid:
        recid = hostname+"-"+str(uuid.uuid1())
    rid.set(SR+"recordId", recid)

    subjid = etree.SubElement(rec, SR+'SubjectIdentity')

    if group:
        grouproles = group.split('/')

        # If the last token is Role=... then we fetch the role and add it to the record
        tmprl = grouproles[-1]
        if tmprl.find('Role=') != -1:
            splitroles = tmprl.split('=')
            if (len(splitroles) > 1):
                role = splitroles[1]
                grp = etree.SubElement(subjid, SR+"GroupAttribute")
                grp.set(SR+"attributeType", "role")
                grp.text = role
            # Now drop this last token, what remains is the vo identifier
            grouproles.pop()

        # The voname is the first token
        voname = grouproles.pop(0)
        grp = etree.SubElement(subjid, SR+"Group")
        grp.text = voname

        # If there are other tokens, they are a subgroup
        if len(grouproles) > 0:
            subgrp = '/'.join(grouproles)
            grp = etree.SubElement(subjid, SR+"GroupAttribute")
            grp.set(SR+"attributeType", "subgroup")
            grp.text = subgrp

    if user:
        usr = etree.SubElement(subjid, SR+"User")
        usr.text = user

    if site:
        st = etree.SubElement(subjid, SR+"Site")
        st.text = site

    e = etree.SubElement(rec, SR+"StorageMedia")
    e.text = "disk"

    if validduration:
        e = etree.SubElement(rec, SR+"StartTime")
        d = datetime.datetime.utcnow() - datetime.timedelta(seconds=validduration)
        e.text = d.strftime("%Y-%m-%dT%H:%M:%SZ")

    e = etree.SubElement(rec, SR+"EndTime")
    e.text = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

    if filecount:
        e = etree.SubElement(rec, SR+"FileCount")
        e.text = str(filecount)

    if not resourcecapacityused:
        resourcecapacityused = 0

    e1 = etree.SubElement(rec, SR+"ResourceCapacityUsed")
    e1.text = str(resourcecapacityused)

    e3 = etree.SubElement(rec, SR+"ResourceCapacityAllocated")
    e3.text = str(resourcecapacityused)

    if not logicalcapacityused:
        logicalcapacityused = 0

    e2 = etree.SubElement(rec, SR+"LogicalCapacityUsed")
    e2.text = str(logicalcapacityused)


#
# Return dictionary with reserved space by given column (s_uid, s_gid)
#
def getreservedspace(column):
    with DBConn.get('dpm_db').cursor() as cursor:
        cursor.execute('SELECT {0}, SUM(t_space) FROM dpm_space_reserv GROUP BY {0}'.format(column))

        ret = {}
        for row in cursor:
            ret[row[0]] = row[1]

    return ret


#
# Return dictionary with key / value for given table
#
def getdbkv(table, ckey, cval):
    with DBConn.get('cns_db').cursor() as cursor:
        cursor.execute('SELECT {0}, {1} FROM {2}'.format(ckey, cval, table))

        ret = {}
        for row in cursor:
            ret[row[0]] = row[1]

    return ret


def star(reportgroups, reportusers, record_id, site, hostname, validduration):
    # Init the xml generator
    xmlroot = etree.Element(SR+"StorageUsageRecords", nsmap=NSMAP)

    if reportgroups:
        # Report about groups
        _log.debug("Groups reporting: starting")

        groups2space = getreservedspace('groups')
        gid2name = getdbkv('Cns_groupinfo', 'gid', 'groupname')

        # find all unique groups associated with some quotatokens
        allgroups = []
        for groups in groups2space.keys():
            for group in [ int(x) for x in groups.split(',') ]:
                if group == 0: continue # ignore 'root' group
                if group not in gid2name: continue
                name = gid2name[group]
                if name in allgroups: continue
                allgroups.append(name)
        # filter only base VO groups assigned to quotatokens
        maingroups = []
        for group in allgroups:
            if any([ group.startswith("%s/" % x) for x in allgroups ]):
                continue
            maingroups.append(group)
        # sum space for base VO groups
        gname2space = {}
        for groups, space in groups2space.items():
            used = []
            for group in [ int(x) for x in groups.split(',') ]:
                if group == 0: continue # ignore 'root' group
                if group not in gid2name: continue
                name = gid2name[group]
                # attempt to map group name to one of base VO group
                matchedgroups = [x for x in maingroups if x == name or name.startswith("%s/" % x)]
                if len(matchedgroups) != 1: continue
                maingroup = matchedgroups[0]
                if maingroup in used: continue
                gname2space[maingroup] = gname2space.get(maingroup, 0) + space
                used.append(maingroup)

        gname2used_files = {}
        gname2used_space = {}
        with DBConn.get('cns_db').cursor() as cursor:
            cursor.execute('SELECT gid, COUNT(*), SUM(filesize) FROM Cns_file_metadata WHERE filemode & 16384 = 0 GROUP BY gid')

            for row in cursor:
                if _log.getEffectiveLevel() < logging.DEBUG:
                    _log.debug(row)
                if row[0] not in gid2name:
                    continue
                if row[0] == 0: # ignore data owned by 'root'
                    continue

                name = gid2name[row[0]]
                matchedgroups = [x for x in maingroups if x == name or name.startswith("%s/" % x)]
                if len(matchedgroups) != 1: continue
                name = matchedgroups[0]
                gname2used_files[name] = gname2used_files.get(name, 0) + row[1]
                gname2used_space[name] = gname2used_space.get(name, 0) + row[2]

            for name in gname2used_files.keys():
                # update XML
                addrecord(xmlroot, hostname, name, None, site, gname2used_files[name], gname2space[name], gname2used_space[name], validduration, record_id)

            _log.debug("Groups reporting: number of rows returned: %d (only %d maingroups)", cursor.rowcount, len(maingroups))


    if reportusers:
        #
        # Report about users
        #
        _log.debug("Users reporting: starting")

        uid2space = getreservedspace('s_uid')
        uid2name = getdbkv('Cns_userinfo', 'userid', 'username')

        with DBConn.get('cns_db').cursor() as cursor:
            cursor.execute('SELECT owner_uid, COUNT(*), SUM(filesize) FROM Cns_file_metadata WHERE filemode & 16384 = 0 GROUP BY owner_uid')

            for row in cursor:
                if _log.getEffectiveLevel() < logging.DEBUG:
                    _log.debug(row)
                if row[0] not in uid2name:
                    continue

                # update XML
                addrecord(xmlroot, hostname, None, uid2name[row[0]], site, row[1], uid2space.get(row[0], 0), row[2], validduration, record_id)

            _log.debug("Users reporting: number of rows returned: %d", cursor.rowcount)

    DBConn.close()

    out = io.BytesIO()
    et = etree.ElementTree(xmlroot)
    et.write(out, pretty_print=True, encoding="utf-8")

    return out.getvalue().decode('utf-8')



class StARPublisher(object):

    def __init__(self, ams_host='msg.argo.grnet.gr', ams_port=443, ams_auth_port=8443, cert='/etc/grid-security/hostcert.pem', key='/etc/grid-security/hostkey.pem', capath='/etc/grid-security/certificates'):
        self._ams_host = ams_host
        self._ams_port = ams_port
        self._ams_auth_port = ams_auth_port
        self._ams_project = 'accounting'
        self._ams_topic = 'eu-egi-storage-accounting'
        self._cert = cert
        self._key = key
        self._capath = capath
        self._token = None


    def _request(self, url, method='GET', body=None, cert=None, key=None, capath=None):
        headers = { 'Content-Type': 'application/json' }
        urlquery = "{0}?{1}".format(url.path, url.query) if url.query else url.path

        _log.debug('request: %s', url.geturl())

        ctx = ssl.create_default_context()
        if cert and key:
            ctx.load_cert_chain(certfile=cert, keyfile=key)
        ctx.load_verify_locations(capath=capath)

        conn = http_client.HTTPSConnection(url.hostname, port=url.port, context=ctx)
        conn.request(method=method, url=urlquery, body=body, headers=headers)

        res = conn.getresponse()
        if res.status != 200:
            raise Exception("http request status %i: %s" % (res.status, res.reason))

        return json.loads(res.read())


    def _get_token(self):
        if not self._token:
            _log.debug("get_token from %s:%s", self._ams_host, self._ams_auth_port)

            route = 'https://{0}:{1}/v1/service-types/ams/hosts/{0}:authx509'
            url = urlparse(route.format(self._ams_host, self._ams_auth_port))
            data = self._request(url, cert=self._cert, key=self._key, capath=self._capath)
            self._token = str(data['token'])

            _log.debug("retrieved token %s", self._token)

        return self._token


    def _sign_bytes(self, data):
        buf = BIO.MemoryBuffer(data)

        smime = SMIME.SMIME()
        smime.load_key(self._key, self._cert)
        p7 = smime.sign(buf, SMIME.PKCS7_TEXT|SMIME.PKCS7_DETACHED)
        # use non-default signature algorithm
        #if M2Crypto.version_info < (0, 26, 0):
        #    p7 = smime.sign(buf, SMIME.PKCS7_TEXT|SMIME.PKCS7_DETACHED)
        #else:
        #    p7 = smime.sign(buf, SMIME.PKCS7_TEXT|SMIME.PKCS7_DETACHED, algo='sha256')

        out = BIO.MemoryBuffer()
        buf = BIO.MemoryBuffer(data)
        smime.write(out, p7, buf, SMIME.PKCS7_TEXT)

        return out.read()


    def topics():
        _log.debug("list %s topics %s:%s", self._ams_project, self._ams_host, self._ams_port)

        route = 'https://{0}:{1}/v1/projects/{2}/topics?key={3}'
        token = self._get_token()
        url = urlparse(route.format(self._ams_host, self._ams_port, self._ams_project, token))

        return self._request(url, capath=self._capath)


    def publish(self, data):
        _log.debug("publish %s topic %s to %s:%s", self._ams_project, self._ams_topic, self._ams_host, self._ams_port)

        route = 'https://{0}:{1}/v1/projects/{2}/topics/{3}:publish?key={4}'
        token = self._get_token()
        url = urlparse(route.format(self._ams_host, self._ams_port, self._ams_project, self._ams_topic, token))
        currtime = datetime.datetime.utcnow().strftime("%Y%m%d%H%M%S")
        msg = {
            'attributes': {'empaid': "{0}/{1}".format(currtime[:8], currtime)},
            'data': base64.encodestring(self._sign_bytes(data)).decode('utf-8')
        }

        return self._request(url, method='POST', body=json.dumps({"messages": [msg]}), capath=self._capath)



################################
# Main code - legacy interface #
################################
def main(argv):
    import optparse

    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.WARN)

    parser = optparse.OptionParser()
    parser.add_option('--reportgroups', dest='reportgroups', action='store_true', default=False, help="Report about all groups")
    parser.add_option('--reportusers', dest='reportusers', action='store_true', default=False, help="Report about all users")
    parser.add_option('-v', '--debug', dest='verbose', action='count', default=0, help='Increase verbosity level for debugging (on stderr)')
    parser.add_option('--hostname', dest='hostname', default=socket.getfqdn(), help="The hostname string to use in the record. Default: this host.")
    parser.add_option('--site', dest='site', default="", help="The site string to use in the record. Default: none.")
    parser.add_option('--recordid', dest='recordid', default=None, help="The recordid string to use in the record. Default: a newly computed unique string.")
    parser.add_option("--nsconfig", dest="nsconfig", default=None, help="LEGACY OPTION, NO LONGER IN USE (NSCONFIG file with sql connection info)")
    parser.add_option("--dpmconfig", dest="dpmconfig", default=None, help="LEGACY OPTION, NO LONGER IN USE (DPMCONFIG file with sql connection info)")
    parser.add_option('--dbhost', dest='dbhost', default=None, help="Database host, if no NSCONFIG given")
    parser.add_option('--dbuser', dest='dbuser', default=None, help="Database user, if no NSCONFIG given")
    parser.add_option('--dbpwd', dest='dbpwd', default=None, help="Database password, if no NSCONFIG given")
    parser.add_option('--nsdbname', dest='nsdbname', default='cns_db', help="NS Database name, if no NSCONFIG given")
    parser.add_option('--dpmdbname', dest='dpmdbname', default='dpm_db', help="DPM Database name, if no DPMCONFIG given")
    parser.add_option('--validduration', dest='validduration', default=86400, help="Valid duration of this record, in seconds (default: 1 day)")
    parser.add_option('--ams-host', default="msg.argo.grnet.gr", help="APEL accounting server hostname, default: %(default)")
    parser.add_option('--ams-port', default=443, help="APEL accounting server port, default: %(default)")
    parser.add_option('--ams-auth-port', default=8443, help="APEL accounting server authentication port, default: %(default)")
    parser.add_option('--cert', default='/etc/grid-security/hostcert.pem', help="Host certificate for access to APEL accounting server, default: %(default)")
    parser.add_option('--key', default='/etc/grid-security/hostkey.pem', help="Host key for access to APEL accounting server, default: %(default)")
    parser.add_option('--capath', default='/etc/grid-security/certificates', help="Trusted CA directory for APEL accounting server verification, default: %(default)")

    options, args = parser.parse_args(sys.argv[1:])

    if options.verbose == 0: _log.setLevel(logging.ERROR)
    elif options.verbose == 1: _log.setLevel(logging.WARN)
    elif options.verbose == 2: _log.setLevel(logging.INFO)
    else: _log.setLevel(logging.DEBUG)

    if options.dbhost != None:
        # try to use database connection data from CLI
        if options.dbuser == None or options.dbpwd == None:
            _log.error("no database user or password defined as command line options")
            return 1

        DBConn.configure('user', {
            'host': options.dbhost,
            #'port': 0,
            'user': options.dbuser,
            'pass': options.dbpwd,
            'cns_db': options.nsdbname,
            'dpm_db': options.dpmdbname,
        })

    data = star(options.reportgroups, options.reportusers, options.recordid, options.site, options.hostname, options.validduration)
    sys.stdout.write(data)

    _log.debug('done')

    return os.EX_OK
