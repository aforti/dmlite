#!/usr/bin/python2
################################################################################
## Original script is now integrated in the dmliteshell which should be used  ##
## instead of calling this legacy but fully backward compatible CLI interface ##
################################################################################
# WLCG Storage Resource Reporting implementation for DPM
# * https://docs.google.com/document/d/1yzCvKpxsbcQC5K9MyvXc-vBF1HGPBk4vhjw3MEXoXf8/edit
# * https://twiki.cern.ch/twiki/bin/view/LCG/AccountingTaskForce
# Configuration
# * https://twiki.cern.ch/twiki/bin/view/DPM/DpmSetupManualInstallation#Publishing_space_usage
import sys
from dmliteshell import srr

srr._log.warn("Calling directly %s is deprecated, use `dmlite-shell -e 'srr'`" % sys.argv[0])
sys.exit(srr.main(sys.argv))
