-- Adds the pooltype and poolmeta fields used by dmlite to support
-- multiple pool types.
-- Updates schema_version_dpm table from 3.4.0 to 3.5.0
ALTER TABLE dpm_space_reserv ADD COLUMN path VARCHAR(255);
ALTER TABLE dpm_space_reserv ADD UNIQUE (path);

UPDATE schema_version_dpm SET major = 3, minor = 5, patch = 0;
