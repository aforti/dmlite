/*
 * Copyright 2015 CERN
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



/** @file   DomeGenQueue.cpp
 * @brief  A helper class that implements a thread safe, generic task queue
 * @author Fabrizio Furano
 * @date   Dec 2015
 */

#include "DomeGenQueue.h"
#include "utils/logger.h"
#include "DomeStatus.h"
#include "DomeLog.h"

#include <boost/make_shared.hpp>
#include <iostream>

// using namespace dmlite;

using boost::shared_ptr;
using boost::recursive_mutex;


void GenPrioQueueItem::update(std::string name, GenPrioQueueItem::QStatus st, int prio, const std::vector<std::string> &qual) {
  namekey = name;
  status = st;
  priority = prio;
  qualifiers = qual;
}

GenPrioQueue::GenPrioQueue(int timeoutsecs, std::vector<size_t> qualifiercountlimits): timeout(timeoutsecs),limits(qualifiercountlimits),running(0) {
  // populate the active structure
  for(unsigned int i = 0; i < limits.size(); i++) {
    active.push_back(std::map<std::string, size_t>());
  }
}

GenPrioQueue::~GenPrioQueue() {
}

void GenPrioQueue::addToTimesort(GenPrioQueueItem_ptr item) {
  accesstimeKey key;
  key.accesstime = item->accesstime;
  key.namekey = item->namekey;
  timesort[key] = item;
}

void GenPrioQueue::removeFromTimesort(GenPrioQueueItem_ptr item) {
  accesstimeKey key;
  key.accesstime = item->accesstime;
  key.namekey = item->namekey;
  timesort.erase(key);
}

void GenPrioQueue::updateAccessTime(GenPrioQueueItem_ptr item) {
  struct timespec newtime;
  clock_gettime(CLOCK_MONOTONIC, &newtime);

  accesstimeKey key;
  key.accesstime = item->accesstime;
  key.namekey = item->namekey;
  timesort.erase(key);

  key.accesstime = newtime;
  item->accesstime = newtime;
  timesort[key] = item;
}

int GenPrioQueue::insertItem(GenPrioQueueItem_ptr item) {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);

  item->insertiontime = now;
  item->accesstime = now;
  addToTimesort(item);

  if(item->status == GenPrioQueueItem::Waiting) {
    addToWaiting(item);
  }
  else if(item->status == GenPrioQueueItem::Running) {
    item->accesstime_running = now;
    addToRunning(item);
  }
  else {
    Log(Logger::Lvl4, domelogmask, domelogname, " WARNING: Tried to add item with status neither Waiting nor Running");
    return -1;
  }

  items[item->namekey] = item;
  return 0;
}

void GenPrioQueue::updateStatus(GenPrioQueueItem_ptr item, GenPrioQueueItem::QStatus status) {
  if(item->status == status) return;

  if(item->status == GenPrioQueueItem::Waiting) {
    removeFromWaiting(item);
  }

  if(item->status == GenPrioQueueItem::Running) {
    removeFromRunning(item);
  }

  if(status == GenPrioQueueItem::Waiting) {
    addToWaiting(item);
  }

  if(status == GenPrioQueueItem::Running) {
    clock_gettime(CLOCK_MONOTONIC, &item->accesstime_running);
    addToRunning(item);
  }

  item->status = status;
}

void GenPrioQueue::addToWaiting(GenPrioQueueItem_ptr item) {
  waitingKey key(item->priority, item->insertiontime, item->namekey);
  waiting[key] = item;
}

void GenPrioQueue::removeFromWaiting(GenPrioQueueItem_ptr item) {
  waitingKey key(item->priority, item->insertiontime, item->namekey);
  waiting.erase(key);
}

// this function makes no checks if the limits are violated - it is intentional
void GenPrioQueue::addToRunning(GenPrioQueueItem_ptr item) {
  for(unsigned int i = 0; i < item->qualifiers.size(); i++) {
    if(i >= limits.size()) break;
    active[i][item->qualifiers[i]]++;
  }
  running++;
}

void GenPrioQueue::removeFromRunning(GenPrioQueueItem_ptr item) {
  for(unsigned int i = 0; i < item->qualifiers.size(); i++) {
    if(i >= limits.size()) break;
    active[i][item->qualifiers[i]]--;

    if(active[i][item->qualifiers[i]] == 0) {
      active[i].erase(item->qualifiers[i]);
    }
  }
  running--;
}

bool GenPrioQueue::possibleToRun(GenPrioQueueItem_ptr item) {
  for(unsigned int i = 0; i < item->qualifiers.size(); i++) {
    if(i >= limits.size()) break;

    if(active[i][item->qualifiers[i]] >= limits[i]) {
      return false;
    }
  }
  return true;
}

int GenPrioQueue::touchItemOrCreateNew(std::string namekey, GenPrioQueueItem::QStatus status, int priority, const std::vector<std::string> &qualifiers) {
  scoped_lock lock(*this);
  Log(Logger::Lvl4, domelogmask, domelogname, " Touching new item to the queue with name: " << namekey << ", status: " << status <<
      "priority: " << priority);

  GenPrioQueueItem_ptr item = items[namekey];

  // is this a new item to add to the queue?
  if(item == NULL) {
    item = boost::make_shared<GenPrioQueueItem>();
    item->update(namekey, status, priority, qualifiers);
    insertItem(item);
  }
  // nope, but maybe I need to update it
  else {
    updateAccessTime(item);
    if(status == GenPrioQueueItem::Running)
      clock_gettime(CLOCK_MONOTONIC, &item->accesstime_running);
    
    // FF 25/07/2018 Modified and commented out the deletion
    // of completed and finished items
    // This is to allow subsequent transactions to check if there had been a recent previous one
    // for example to see if a checksum had recently failed
    //
    // is it finished? remove the item
    //if(status == GenPrioQueueItem::Completed || status == GenPrioQueueItem::Finished) {
    //  removeItem(namekey);
    //} else
      
    // difficult updates with consequences on internal data structures
    // need to remove and re-insert
    if(priority != item->priority /* || qualifiers != item->qualifiers*/) {
      // only allow forward changes to the status, even in this case
      GenPrioQueueItem::QStatus newStatus = item->status;
      if(status > item->status) newStatus = status;

      removeItem(namekey);
      item->update(namekey, newStatus, priority, qualifiers);
      insertItem(item);
    }
    // easy update - only progress status
    else if(item->status < status) {
      updateStatus(item, status);
    }
    else {
      // nothing has changed, nothing to do
    }
  }

  return 0;
}

size_t GenPrioQueue::nWaiting() {
  return waiting.size();
}

size_t GenPrioQueue::nRunning() {
  return running;
}

size_t GenPrioQueue::nTotal() {
  return items.size();
}

GenPrioQueueItem_ptr GenPrioQueue::removeItem(std::string namekey) {
  scoped_lock lock(*this);

  std::map<std::string, boost::shared_ptr<GenPrioQueueItem> >::iterator it;
  it = items.find(namekey);
  if (it == items.end()) return GenPrioQueueItem_ptr();
  
  
  GenPrioQueueItem_ptr item = it->second;
  items.erase(namekey);
  
  if(item == NULL) return item;

  updateStatus(item, GenPrioQueueItem::Finished);
  removeFromTimesort(item);

  return item;
}

int GenPrioQueue::peekItemStatus(std::string namekey, GenPrioQueueItem::QStatus &status) {
  scoped_lock lock(*this);
  
  std::map<std::string, boost::shared_ptr<GenPrioQueueItem> >::iterator it;
  it = items.find(namekey);
  if (it == items.end()) return -1;
  

  status = it->second->status;
  return 0;

}

GenPrioQueueItem_ptr GenPrioQueue::getNextToRun() {
  scoped_lock lock(*this);
  std::map<waitingKey, GenPrioQueueItem_ptr>::iterator it;
  for(it = waiting.begin(); it != waiting.end(); it++) {
    GenPrioQueueItem_ptr item = it->second;

    if(possibleToRun(item)) {
      updateStatus(item, GenPrioQueueItem::Running);
      return item;
    }
  }
  return GenPrioQueueItem_ptr();
}

int GenPrioQueue::getStats(std::vector<int32_t> &counts) {
  
  // This is the number of elements in the QStatus enum
  counts.resize(5);
  for (int i = 0; i < 5; i++)
    counts[i] = 0;
  
  std::map<accesstimeKey, GenPrioQueueItem_ptr>::iterator it;
  {
    scoped_lock lock(*this);
  
    for(it = timesort.begin(); it != timesort.end(); it++) {
      int c = (int)it->second->status;
      if (c < (int)counts.size())
        counts[ c ]++;
    }
  }
  
  return 0;
}


int GenPrioQueue::tick() {
  scoped_lock lock(*this);
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);

  std::map<accesstimeKey, GenPrioQueueItem_ptr>::iterator it;

  // good old ugly hack
  bool repeat;
  do {
    repeat = false;
    for(it = timesort.begin(); it != timesort.end(); it++) {
      GenPrioQueueItem_ptr item = it->second;
      
      // Items marked as running shall become failed if they
      // have not been touched by their running process
      if (item->status == GenPrioQueueItem::Running) {
        if(now.tv_sec > item->accesstime_running.tv_sec + timeout) {
          Log(Logger::Lvl1, domelogmask, domelogname, " Running queue item with key '" << item->namekey << "' set to Completed after " << timeout << " seconds of inactivity.");
            updateStatus(item, GenPrioQueueItem::Completed);
            continue;
       }
      }
      
      // Remove items that have not been touched at all for too long
      if(now.tv_sec > item->accesstime.tv_sec + timeout) {
        if (item->status != GenPrioQueueItem::Finished) {
          Log(Logger::Lvl1, domelogmask, domelogname, " Queue item with key '" << item->namekey << "' timed out after " << timeout << " seconds. Status: " << item->status);
        } else {
          Log(Logger::Lvl3, domelogmask, domelogname, " Finished queue item with key '" << item->namekey << "' removed after " << timeout << " seconds. Status: " << item->status);
        }
        
        // don't modify status through removal
        GenPrioQueueItem::QStatus status = item->status;
        timesort.erase(it);
        removeItem(item->namekey);
        item->status = status;
        repeat = true;
        break;
      }
      else {
        return 0; // the rest of the items are guaranteed to be newer
      }
    }
  } while (repeat);
  
  return 0;
}
