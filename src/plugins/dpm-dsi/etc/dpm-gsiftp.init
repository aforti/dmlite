#!/bin/sh
# dpm-gsiftp         Start/Stop DPM Gridftp server.
#
# chkconfig: - 85 15
#  
# description: DPM-enabled Globus gridftp server
# config: /etc/sysconfig/dpm-gsiftp

X509_USER_CERT=/etc/grid-security/hostcert.pem
export X509_USER_CERT
X509_USER_KEY=/etc/grid-security/hostkey.pem
export X509_USER_KEY
X509_CERT_DIR=/etc/grid-security/certificates
export X509_CERT_DIR
GRIDMAP=/etc/grid-security/grid-mapfile
export GRIDMAP
GRIDMAPDIR=/etc/grid-security/gridmapdir
export GRIDMAPDIR
CSEC_MECH=ID
export CSEC_MECH
FTPLOGFILE=/var/log/dpm-gsiftp/dpm-gsiftp.log
GLOBUS_TCP_PORT_RANGE="20000,25000"
export GLOBUS_TCP_PORT_RANGE
GLOBUS_GSSAPI_CIPHERS='RC4-SHA:AES128-SHA:HIGH:!aNULL:!MD5:!RC4'
export GLOBUS_GSSAPI_CIPHERS

sysname=`uname -s`

# source function library
if [ -r /etc/rc.d/init.d/functions ]; then
    . /etc/rc.d/init.d/functions
    DAEMON=daemon
    FAILURE=failure
    ECHO_FAILURE=echo_failure
    SUCCESS=success
    ECHO_SUCCESS=echo_success
    ECHO_END=echo
    if [ $sysname = "Linux" ]; then
        ECHOOPT=-n
    fi
else
    DAEMON=
    FAILURE=
    ECHO_FAILURE=
    SUCCESS=
    ECHO_SUCCESS=
    ECHOOPT=
    ECHO_END=
fi

###############  Standard Globus configuration  ######################

if [ -f /etc/sysconfig/globus ]; then
    . /etc/sysconfig/globus
fi

GLOBUS_LOCATION=${GLOBUS_LOCATION:-/opt/globus}
if [ ! -d "$GLOBUS_LOCATION" ]; then
GLOBUS_LOCATION=/usr
fi
export GLOBUS_LOCATION

if [ -n "$GLOBUS_TCP_PORT_RANGE" ]; then
    export GLOBUS_TCP_PORT_RANGE
fi

if [ -n "$GLOBUS_TCP_SOURCE_RANGE" ]; then
    export GLOBUS_TCP_SOURCE_RANGE
fi

if [ -n "$GLOBUS_UDP_PORT_RANGE" ]; then
    export GLOBUS_UDP_PORT_RANGE
fi

if [ -n "$GLOBUS_THREAD_MODEL" ]; then
    export GLOBUS_THREAD_MODEL
fi

if [ -n "$GLOBUS_GSSAPI_FORCE_TLS" ]; then
    export GLOBUS_GSSAPI_FORCE_TLS
fi

if [ -n "$GLOBUS_GSSAPI_MIN_TLS_PROTOCOL" ]; then
    export GLOBUS_GSSAPI_MIN_TLS_PROTOCOL
fi

if [ -n "$GLOBUS_GSSAPI_MAX_TLS_PROTOCOL" ]; then
    export GLOBUS_GSSAPI_MAX_TLS_PROTOCOL
fi

if [ -n "$GLOBUS_GSSAPI_CIPHERS" ]; then
    export GLOBUS_GSSAPI_CIPHERS
fi

if [ -n "$GLOBUS_GSSAPI_SERVER_CIPHER_ORDER" ]; then
    export GLOBUS_GSSAPI_SERVER_CIPHER_ORDER
fi

if [ -n "$GLOBUS_GSSAPI_BACKWARD_COMPATIBLE_MIC" ]; then
    export GLOBUS_GSSAPI_BACKWARD_COMPATIBLE_MIC
fi

if [ `uname -m` = "x86_64" ]; then
    LD_LIBRARY_PATH=/opt/glite/lib64:/opt/lcg/lib64:$GLOBUS_LOCATION/lib
else
    LD_LIBRARY_PATH=/opt/glite/lib:/opt/lcg/lib:$GLOBUS_LOCATION/lib
fi
export LD_LIBRARY_PATH

######################################################################

RETVAL=0
prog="dpm-gsiftp"
port=2811
PIDFILE=/var/run/dpm-gsiftp.pid
PIDDIR=/var/run
SUBSYS=/var/lock/subsys/dpm-gsiftp
SUBSYSDIR=/var/lock/subsys
[ -z "$SILENTSTOP" ] && SILENTSTOP=0
FTPD=$GLOBUS_LOCATION/sbin/globus-gridftp-server
if [ ! -f $FTPD ]; then
    echo "Globus GridFTP binary not found: $FTPD"
    exit 1
fi
#
# -S  stand alone mode
# -l  activity log
# -d  debug level
# -p  port number
# -Z  transfer log
#
OPTIONS="-S -p $port -auth-level 0 -dsi dmlite -disable-usage-stats"

if [ -r /opt/globus/etc/dpm-gsiftp.conf ]; then
    SYSCONFIGFTP=/opt/globus/etc/dpm-gsiftp.conf
elif [ -r /etc/sysconfig/dpm-gsiftp ]; then
    SYSCONFIGFTP=/etc/sysconfig/dpm-gsiftp
elif [ -r /etc/default/dpm-gsiftp ]; then
    SYSCONFIGFTP=/etc/default/dpm-gsiftp
fi

#
## Blindly attempt to create useful directories
#
# [ ! -d $PIDDIR ] && mkdir -p $PIDDIR > /dev/null 2>&1
# [ ! -d $SUBSYSDIR ] && mkdir -p $SUBSYSDIR > /dev/null 2>&1

if [ $sysname = "HP-UX" ]; then
    export UNIX95=1
fi
if [ $sysname = "SunOS" ]; then
    format4comm="fname"
elif [ $sysname = "Darwin" ]; then
    format4comm="ucomm"
else
    format4comm="comm"
fi

case "$1" in
  start)
    echo $ECHOOPT "Starting $prog: "
    netstat -an | egrep "[:.]$port\b" | egrep 'LISTEN *$' > /dev/null
    if [ $? -eq 0 ]; then
        echo $ECHOOPT "globus-gridftp-server already started: "
        [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "globus-gridftp-server already started: "
        RETVAL=0
    else
        if [ -n "$SYSCONFIGFTP" ]; then
            #
            ## Source the configuration
            #
            . $SYSCONFIGFTP
            export DPM_HOST
            export DPNS_HOST
            if [ "${RUN_DPMFTP}" != "yes" ]; then
                echo $ECHOOPT "$SYSCONFIGFTP says NO: "
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "$SYSCONFIGFTP says NO: "
                    RETVAL=0
                    $ECHO_END
                    exit $RETVAL
            fi
            if [ -n "${ULIMIT_N}" ]; then
                ulimit -n ${ULIMIT_N}
            fi
        fi
        if [ $RETVAL -eq 0 ]; then
            cd /
            mkdir -p `dirname $FTPLOGFILE`
            chown dpmmgr:dpmmgr `dirname $FTPLOGFILE`
            alog=`dirname $FTPLOGFILE`/gridftp.log
            chown -f dpmmgr:dpmmgr $FTPLOGFILE $alog
            X509_USER_CERT=/etc/grid-security/dpmmgr/dpmcert.pem
            export X509_USER_CERT
            X509_USER_KEY=/etc/grid-security/dpmmgr/dpmkey.pem
            export X509_USER_KEY
            $DAEMON "su dpmmgr -c \"umask 0; $FTPD $OPTIONS -l $alog -Z $FTPLOGFILE\""
            if [ $? -eq 0 ]; then
                [ -d $SUBSYSDIR ] && touch $SUBSYS
                if [ -d $PIDDIR ]; then
                    pid=`ps -eo pid,ppid,$format4comm | grep " 1 globus-g" | awk '{print $1}'`
                    # The point of $PIDFILE is that it kills only
                    # the master daemon.
                    rm -f $PIDFILE
                    echo $pid > $PIDFILE
                fi
                RETVAL=0
            else
                echo $ECHOOPT "globus-gridftp-server not started: "
                [ -n "$FAILURE" ] && $FAILURE "globus-gridftp-server not started: "
                RETVAL=1
            fi
        fi
    fi
    $ECHO_END
    ;;
  stop)
    if [ -f $PIDFILE ]; then
        echo $ECHOOPT "Stopping $prog: "
        #
        ## We just attempt to kill the main daemon
        ## Remaining child are allowed to continue transfer up to
        ## the end
        #
        kill -9 `cat $PIDFILE` > /dev/null 2>&1
        RETVAL=$?
        if [ $RETVAL -eq 0 ]; then
            rm -f $PIDFILE
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Stopping $prog: "
        else
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Stopping $prog: "
        fi
    else
        # globus-gridftp-server might have been started by hand
        pid=`ps -eo pid,ppid,$format4comm | grep " 1 globus-g" | awk '{print $1}'`
        if [ -n "$pid" ]; then
            echo $ECHOOPT "Stopping $prog: "
            #
            ## We just attempt to kill the main daemon
            ## Remaining child are allowed to continue transfer up to
            ## the end
            #
            kill -9 $pid > /dev/null 2>&1
            RETVAL=$?
            if [ $RETVAL -eq 0 ]; then
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Stopping $prog: "
            else
                [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Stopping $prog: "
            fi
        else
            echo $ECHOOPT "globus-gridftp-server already stopped: "
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "globus-gridftp-server already stopped: "
            [ $SILENTSTOP -eq 0 ] && RETVAL=0 || RETVAL=1
        fi
    fi

    [ -d $SUBSYSDIR ] && rm -f $SUBSYS
    [ -n "$ECHO_END" ] && $ECHO_END
    ;;
  restart | force-reload)
    $0 stop
    if [ $? -eq 0 -o $SILENTSTOP -eq 0 ]; then
        $0 start
        RETVAL=$?
    else
        RETVAL=0
    fi
    ;;
  condrestart | try-restart)
    SILENTSTOP=1
    export SILENTSTOP
    $0 restart
    RETVAL=$?
    ;;
  reload)
    ;;
  status)
    pid=`ps -eo pid,ppid,$format4comm | grep " 1 globus-g" | awk '{print $1}'`
    if [ -n "$pid" ]; then
        echo $ECHOOPT "globus-gridftp-server (pid $pid) is running..."
        $ECHO_SUCCESS
        $ECHO_END
        RETVAL=0
    else
        if [ -f $PIDFILE ]; then
            pid=`head -1 $PIDFILE`
            if [ "$pid" != "" ] ; then
                echo $ECHOOPT "globus-gridftp-server dead but pid file exists"
                $ECHO_FAILURE
                $ECHO_END
                RETVAL=1
            fi
        else
            if [ -f $SUBSYS ]; then
                echo $ECHOOPT "globus-gridftp-server dead but subsys ($SUBSYS) locked"
                RETVAL=2
            else
                echo $ECHOOPT "globus-gridftp-server is stopped"
                RETVAL=3
            fi
            $ECHO_FAILURE
            $ECHO_END
        fi
    fi
    ;;
  *)
    echo "Usage: $0 {start|stop|status|restart|condrestart}"
    RETVAL=1
    ;;
esac

exit $RETVAL
