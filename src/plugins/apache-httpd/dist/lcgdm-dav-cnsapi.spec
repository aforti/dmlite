%{!?_httpd_mmn: %{expand: %%global _httpd_mmn %%(cat %{_includedir}/httpd/.mmn || echo missing-httpd-devel)}}

Name:		lcgdm-dav-cnsapi
Version:	0.18.1
Release:	1%{?dist}
Summary:	HTTP implementation of the DPNS API
Group:		Applications/Internet
License:	ASL 2.0
URL:		https://svnweb.cern.ch/trac/lcgdm
# The source of this package was pulled from upstream's vcs. Use the
# following commands to generate the tarball:
# svn export http://svn.cern.ch/guest/lcgdm/lcgdm-dav/tags/lcgdm-webdav_0_12_0 lcgdm-dav-0.12.0
# tar -czvf lcgdm-dav-0.12.0.tar.gz lcgdm-dav-0.12.0
Source0:	%{name}-%{version}.tar.gz
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  cmake%{?_isa}
%if %{?fedora}%{!?fedora:0} >= 10 || %{?rhel}%{!?rhel:0} >= 6
BuildRequires:  libcurl-devel
%else
BuildRequires:  curl-devel
%endif
BuildRequires:  dmlite-devel%{?_isa}
BuildRequires:  dpm-devel%{?_isa}
BuildRequires:  gridsite-devel%{?_isa}
BuildRequires:  gsoap-devel%{?_isa}
BuildRequires:  httpd-devel%{?_isa}
BuildRequires:  jansson-devel%{?_isa}
BuildRequires:  neon-devel%{?_isa}
BuildRequires:  gnutls-devel%{?_isa}
BuildRequires:  openssl-devel%{?isa}
BuildRequires:  subversion%{?_isa}

Requires:	neon%{?_isa}

%description
This package provides the HTTP based client implementation of the DPNS API.

%prep
%setup -q -n %{name}-%{version}

%build
%cmake . -DBUILD_CNSAPI=yes -DUSE_GNUTLS=yes -DBUILD_DAV=no -DBUILD_HTCOPY=no -DCMAKE_INSTALL_PREFIX=/

cd src/cnsapi
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT

cd src/cnsapi
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
#%{_bindir}/*
%{_libdir}/*
#%{_mandir}/man1/*
%doc src/client/README LICENSE
%{_includedir}/dpm/dav_api.h

%changelog
* Fri May 31 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 0.9.0-1
- Initial build
