# This script assumes that:
# - pwd is the top source directory

echo "Current dir: `pwd`"
mkdir -p src/plugins/apache-httpd/external/curl/
cd src/plugins/apache-httpd/external/curl
echo "Current dir: `pwd`"
ls -l
cmake3 . -DCMAKE_INSTALL_PREFIX=/tmp/curl/bogusinstall -DBUILD_CURL_EXE=false -DBUILD_TESTING=false -DBUILD_SHARED_LIBS=false  -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC -DCMAKE_USE_LIBSSH2=false
make -j2 install
cd -
echo "Current dir: `pwd`"
