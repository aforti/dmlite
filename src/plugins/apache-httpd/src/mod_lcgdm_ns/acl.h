/**
 * @file   acl.h
 * @brief  ACL functions (see RFC 3744).
 * @author Alejandro Álvarez Ayllón <aalvarez.cern.ch>
 */
#ifndef ACL_H_
#define ACL_H_

#include "mod_lcgdm_ns.h"

/** Return the set of supported privilege sets.
 * This is always the same.
 */
const char* dav_ns_supported_privilege_set(void);

/**
 * Format the ACL as XML.
 * @param r    The request.
 * @param acl  The ACL as stored in the xstat.
 * @return     A string with the XML format of the ACL.
 */
char* dav_ns_acl_format(request_rec *r, const char *acl);

#endif	/* ACL_H_ */

