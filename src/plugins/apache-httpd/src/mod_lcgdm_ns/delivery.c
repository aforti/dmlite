/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_pools.h>
#include <apr_strings.h>
#include <dmlite/c/catalog.h>
#include <dmlite/c/pool.h>
#include <dmlite/c/io.h>
#include <ctype.h>
#include <fcntl.h>
#include <httpd.h>
#include <http_log.h>
#include <http_request.h>
#include <inttypes.h>
#include <sys/stat.h>
#include "delivery.h"
#include "../shared/config.h"
#include "../shared/utils.h"

static const char* safe_uri(apr_pool_t* pool, const char* uri)
{
    return apr_xml_quote_string(pool, ap_os_escape_path(pool, uri, 0), 1);
}

static const char* safe_href(apr_pool_t* pool, const char* uri, const char* name)
{
    const char *escaped_name, *escaped_uri;
    escaped_name = apr_xml_quote_string(pool, name, 0);
    escaped_uri = safe_uri(pool, uri);
    return apr_pstrcat(pool, "<a href=\"", escaped_uri, "\">", escaped_name, "</a>", NULL);
}


static dav_error *dav_ns_deliver_collection_legacy(const dav_resource *resource,
        ap_filter_t *output_filters, apr_bucket_brigade *bb)
{
    apr_pool_t *subpool = NULL;
    dav_resource_private *info = resource->info;
    void *dir;
    dmlite_xstat *entry;
    const char *p, *p2;
    char *base_dir;
    int i;
    const dmlite_security_context *security_context;

    security_context = dmlite_get_security_context(resource->info->ctx);

    /* Open */
    if (dmlite_chdir(info->ctx, info->sfn) != 0 ||
        (dir = dmlite_opendir(info->ctx, ".")) == NULL) {
        dav_error *err = dav_shared_new_error(info->request, info->ctx, 0,
                            "Could not open directory %s", info->sfn);
        dmlite_chdir(info->ctx, "/");
        return err;
    }

    /* Generate HTML content for the dir */
    ap_fprintf(output_filters, bb,
            "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
                    "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                    "<head>\n"
                    "<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"/>\n"
                    "<link rel=\"stylesheet\" type=\"text/css\" href=\"/static/css/lcgdm-dav.css\"/>\n"
                    "<link rel=\"icon\" type=\"image/png\" href=\"/static/icons/dpm20.png\"/>\n"
                    "<script src=\"/static/js/ui.js\"></script>"
                    "<title>%s</title>\n"
                    "</head>\n"
                    "<body>\n"
                    "<div id=\"header\"><h1>", apr_xml_quote_string(resource->pool, resource->uri, 0));

    /* Path info contains everything that is AFTER whatever matched with Location
     * If LocationMatch was used, then this can be used to get the part that did match.
     * */
    base_dir = apr_pstrdup(resource->pool, resource->uri);
    i = strlen(resource->uri) - strlen(info->request->path_info);
    base_dir[i] = '\0';

    ap_fputs(output_filters, bb, safe_href(resource->pool, base_dir, base_dir));
    if (base_dir[1] != '\0')
        ap_fputs(output_filters, bb, "/");

    p = resource->uri + i;
    while (p && *p) {
        while (*p == '/') {
            ++p;
        }

        p2 = strchr(p, '/');
        if (p2) {
            const char* uri = apr_pstrmemdup(resource->pool, resource->uri, p2 - resource->uri);
            const char* name = apr_pstrmemdup(resource->pool, p, p2 - p);
            ap_fputs(output_filters, bb, safe_href(resource->pool, uri, name));
            ap_fputs(output_filters, bb, "/");
        }
        else {
            ap_fputs(output_filters, bb, apr_xml_quote_string(resource->pool, p, 0));
        }
        p = p2;
    }
    ap_fputs(output_filters, bb, "</h1></div>\n");

    ap_fputs(output_filters, bb, "<table id=\"ft\">\n"
    "<thead><tr>\n"
    "<th class=\"mode\">Mode</th>"
    "<th class=\"nlinks\">Links</th>"
    "<th class=\"uid\">UID</th>"
    "<th class=\"gid\">GID</th>"
    "<th class=\"size\">Size</th>"
    "<th class=\"datetime\">Modified</th>"
#ifdef WITH_METALINK
            "<th class=\"metalink\"></th>"
#endif
            "<th class=\"name\">Name</th>"
            "</tr></thead>\n");

    apr_pool_create(&subpool, resource->pool);

    while ((entry = dmlite_readdirx(info->ctx, dir))) {
        char mode_buf[11], size_buf[32], date_buf[64], link_buf[8];
        char uid_buf[5], gid_buf[5];
        char sym_target[PATH_MAX];
        const char *target;
        mode_t type, mode;
        dmlite_xstat aux;

        memset(&aux, 0, sizeof(aux));

        /* Human-readable stats */
        dav_shared_mode_str(mode_buf, entry->stat.st_mode);
        dav_shared_size_str(size_buf, sizeof(size_buf), entry->stat.st_size);

        dav_shared_format_datetime(date_buf, sizeof(date_buf),
                entry->stat.st_mtime, DAV_DPM_RFC2068);

        snprintf(uid_buf, sizeof(uid_buf), "%" PRIuMAX, (uintmax_t)entry->stat.st_uid);
        snprintf(gid_buf, sizeof(gid_buf), "%" PRIuMAX, (uintmax_t)entry->stat.st_gid);
        snprintf(link_buf, sizeof(link_buf), "%" PRIuMAX, (uintmax_t)entry->stat.st_nlink);

        /* Print common data */
        ap_fputstrs(output_filters, bb,
                S_ISLNK(entry->stat.st_mode) ? "<tr class=\"link\">" : "<tr>",
                "<td>", mode_buf, "</td>"
                "<td>", link_buf, "</td>"
                "<td>", uid_buf, "</td><td>", gid_buf, "</td>"
                "<td>", size_buf, "</td>"
                "<td>", date_buf, "</td>", NULL );

        /* Print file name */
        type = entry->stat.st_mode & S_IFMT;
        mode = entry->stat.st_mode & ~S_IFMT;

        target = "";

        switch (type) {
            case S_IFLNK:
                /* Special case. Stat the destination to see if it is a directory
                 * or a file */
                dmlite_readlink(info->ctx, entry->name, sym_target,
                        sizeof(sym_target));
                target = apr_xml_quote_string(subpool, sym_target, 0);
                target = apr_pstrcat(subpool, "&nbsp;=&gt; ", target, NULL );

                dmlite_statx(info->ctx, entry->name, &aux);
                if (S_ISDIR(aux.stat.st_mode)) {
                    mode = aux.stat.st_mode & ~S_IFMT;

            case S_IFDIR:
#ifdef WITH_METALINK
                    ap_fputstrs(output_filters, bb,
                            "<td class=\"metalink\"></td>", NULL);
#endif
                    snprintf(mode_buf, sizeof(mode_buf), "M%o", mode);
                    ap_fputstrs(output_filters, bb, "<td class=\"folder ", mode_buf, "\">",
                            safe_href(subpool,  entry->name,  entry->name),
                            target, "</td></tr>\n", NULL);
                    break;
                }
                else {
            default:
#ifdef WITH_METALINK
                    ap_fputstrs(output_filters, bb, "<td class=\"metalink\">"
                            "<a href=\"", safe_uri(subpool, entry->name),
                            "?metalink\">"
                            "<img title=\"Metalink\" alt=\"[Metalink]\" src=\"/static/icons/metalink16.png\"/>"
                            "</a></td>", NULL);
#endif
                    ap_fputstrs(output_filters, bb, "<td class=\"file\">",
                            safe_href(subpool, entry->name, entry->name),
                            target, "</td></tr>\n",
                            NULL);
                    break;
                }
        }

        apr_pool_clear(subpool);
    }

    apr_pool_destroy(subpool);
    subpool = NULL;

    /* Finish table */
    ap_fputs(output_filters, bb, "</table>\n");
    ap_fputs(output_filters, bb, "<div id=\"footer\">\n");

    if (security_context) {
        const char *vo = "No proxy";
        if (security_context->credentials->nfqans)
            vo = security_context->credentials->fqans[0];

        ap_fprintf(output_filters, bb,
                "<p><span id=\"requestby\">Request by %s (%s)</span>\n",
                security_context->credentials->client_name, vo);
    }
    else {
        ap_fprintf(output_filters, bb,
                "<p><span id=\"requestby\">Authentication disabled</span>\n");
    }

    ap_fputs(output_filters, bb,
            "<br/>Powered by LCGDM-DAV " LCGDM_DAV_VERSION " (<a href=\"javascript:setNewUI();\">New UI</a>)</p>\n");
    ap_fputs(output_filters, bb, "</div></body>\n</html>");

    dmlite_chdir(info->ctx, "/");
    if (dmlite_closedir(info->ctx, dir))
        return dav_shared_new_error(info->request, info->ctx, 0,
                "Could not close %s", info->sfn);

    return NULL ;
}


dav_error *dav_ns_deliver_collection(const dav_resource *resource,
        ap_filter_t *output_filters, apr_bucket_brigade *bb)
{
    if (resource->info->new_ui) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, resource->info->request,
                "Requesting new UI");
        ap_internal_redirect("/static/DPMbox/index.html", resource->info->request);
        return NULL;
    }
    else {
        return dav_ns_deliver_collection_legacy(resource, output_filters, bb);
    }
}













dav_error *dav_ns_deliver_metalink(const dav_resource *resource,
        ap_filter_t *output_filters, apr_bucket_brigade *bb)
{
    dav_resource_private *info = resource->info;
    dmlite_replica *replicas;
    unsigned nreplies, i;
    char buffer[64];

    if (dmlite_getreplicas(info->ctx, info->sfn, &nreplies, &replicas) != 0)
        return dav_shared_new_error(info->request, info->ctx, 0,
                "Could not get replicas");

    /* Structure and metadata */
    dav_shared_format_datetime(buffer, sizeof(buffer), info->stat.stat.st_mtime,
            DAV_DPM_RFC2068);
    ap_fprintf(output_filters, bb,
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                    "<metalink version=\"3.0\" xmlns=\"http://www.metalinker.org/\" xmlns:lcgdm=\"LCGDM:\" "
                    "generator=\"lcgdm-dav\" pubdate=\"%s\">\n"
                    "<files>\n", buffer);

    const char* xml_escaped_name = apr_xml_quote_string(resource->pool, info->stat.name, 0);

    ap_fprintf(output_filters, bb, "<file name=\"%s\">\n", xml_escaped_name);
    ap_fprintf(output_filters, bb, "\t<size>%ld</size>\n",
            info->stat.stat.st_size);

    if (info->stat.csumvalue[0] != '\0') {
        char lower_csumtype[CSUMTYPE_MAX + 1];
        for (i = 0; i < CSUMTYPE_MAX && info->stat.csumtype[i]; ++i)
            lower_csumtype[i] = tolower(info->stat.csumtype[i]);
        lower_csumtype[i] = '\0';

        ap_fputs(output_filters, bb, "\t<verification>\n");
        ap_fprintf(output_filters, bb, "\t\t<hash type=\"%s\">%s</hash>\n",
                lower_csumtype, info->stat.csumvalue);
        ap_fputs(output_filters, bb, "\t</verification>\n");
    }

    /* Build url list */
    apr_pool_t *subpool;

    apr_pool_create(&subpool, resource->pool);
    ap_fputs(output_filters, bb, "\t<resources>\n");

    for (i = 0; i < nreplies; ++i) {
      dmlite_location* location = 0;

      // dmlite_getlocation will return 0 if the pool implementation in use cannot produce chunks
      // In this case, chunks will not be expected, just whole replicas
      if ( (info->s_conf->type != DAV_NS_NODE_HEAD) || !((location = dmlite_getlocation(info->ctx, &replicas[i]))) ) {

          dmlite_url *url = dmlite_parse_url(replicas[i].rfn);
          const char* url_str = dav_shared_build_url(subpool, url, &info->d_conf->redirect, 0);
          const char* xml_url = apr_xml_quote_string(subpool, url_str, 0);

          ap_fprintf(output_filters, bb, "\t\t<url type=\"%s\">%s</url>\n", info->d_conf->redirect.scheme, xml_url);
          dmlite_url_free(url);

      }
      else {
          unsigned j;

          for (j = 0; location && j < location->nchunks; ++j) {
            char *url = dav_shared_build_url(subpool, &(location->chunks[j].url), &info->d_conf->redirect, 0);
            const char *xml_url = apr_xml_quote_string(subpool, url, 0);
            ap_fprintf(output_filters, bb,
                         "\t\t<url type=\"%s\" lcgdm:offset=\"%lu\" lcgdm:size=\"%lu\">%s</url>\n",
                info->d_conf->redirect.scheme,
                location->chunks[j].offset, location->chunks[j].size,
                xml_url);
          }
          dmlite_location_free(location);

      }

    }

    apr_pool_clear(subpool);

    ap_fputs(output_filters, bb, "\t</resources>\n");

    /* Close and free */
    ap_fputs(output_filters, bb, "</file>\n</files>\n</metalink>");
    dmlite_replicas_free(nreplies, replicas);
    apr_pool_destroy(subpool);

    return NULL ;
}


dav_error *dav_ns_deliver_virtual(const dav_resource *resource,
        ap_filter_t *output_filters, apr_bucket_brigade *bb)
{
    dmlite_fd* fd = dmlite_fopen(resource->info->ctx,
            resource->info->virtual_location->chunks[0].url.path,
            O_RDONLY,
            resource->info->virtual_location->chunks[0].url.query);
    if (!fd)
        return dav_shared_new_error(resource->info->request, resource->info->ctx, 0,
                "Could not open to read");

    char buffer[2048];
    ssize_t rbytes = 0;
    while ((rbytes = dmlite_fread(fd, buffer, sizeof(buffer))) > 0) {
        ap_fwrite(output_filters, bb, buffer, rbytes);
    }

    dav_error* err = NULL;
    if (rbytes < 0) {
        err = dav_shared_new_error(resource->info->request, NULL, HTTP_INTERNAL_SERVER_ERROR,
                "Could not read: %s", dmlite_ferror(fd));
    }
    dmlite_fclose(fd);

    dmlite_location_free(resource->info->virtual_location);
    resource->info->virtual_location = NULL;
    return err;
}
