cmake_minimum_required (VERSION 2.6)

find_library(JANSSON_LIBRARIES
    NAMES jansson
    HINTS /usr/lib /usr/lib64 /lib /lib64
    DOC "jansson library"
)

include_directories(
    ${CMAKE_SOURCE_DIR}/src/plugins/apache-httpd/external/libmacaroons
    ${CMAKE_CURRENT_SOURCE_DIR}
)

add_library(mod_lcgdm_ns MODULE
    acl.c
    checksum.c
    dbm.c
    delivery.c
    liveprops.c
    mime.c
    mod_lcgdm_ns.c
    repository.c
    replicas.c
    mymacaroons.c
    myoidc.c
    ../shared/checksum.c
    ../shared/security.c
    ../shared/delegation.c
    ../shared/utils.c
)

add_dependencies     (mod_lcgdm_ns lcgdmhtext)
set_target_properties(mod_lcgdm_ns PROPERTIES PREFIX "")
target_link_libraries(mod_lcgdm_ns dmlite ${JANSSON_LIBRARIES} ${APR_LIBRARIES} ${APRUTIL_LIBRARIES} dmlitemacaroons)

install(TARGETS     mod_lcgdm_ns
        DESTINATION usr/lib${LIB_SUFFIX}/httpd/modules)
