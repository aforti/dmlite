/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOD_LCGDM_NS_H_
#define MOD_LCGDM_NS_H_

#include <apr_tables.h>
#include <dmlite/c/dmlite.h>
#include <dmlite/c/inode.h>
#include <dmlite/c/pool.h>
#include <httpd.h>
#include <http_config.h>
#include <mod_lcgdm_dav.h>
#include "../shared/security.h"
#include "../shared/utils.h"
#include "use_module.h"

#define DAV_NS_WRITE              0x01
#define DAV_NS_NOAUTHN            0x02
#define DAV_NS_REMOTE_COPY        0x04
#define DAV_NS_REMOTE_COPY_DMLITE 0x08

/** Node types. */
typedef enum
{
    DAV_NS_NODE_HEAD, DAV_NS_NODE_LFC, DAV_NS_NODE_PLAIN
} node_type;
typedef unsigned char dir_flags;

/* Data that needs to be available outside. */
extern module AP_MODULE_DECLARE_DATA lcgdm_ns_module;
extern const dav_hooks_repository dav_ns_hooks_repository;
extern const dav_liveprop_group dav_ns_liveprop_group;
extern const dav_hooks_db dav_ns_hooks_db;

/** Data structure for server configuration. */
struct dav_ns_server_conf
{
    dmlite_manager *manager;
    node_type type;
};
typedef struct dav_ns_server_conf dav_ns_server_conf;

/** Data structure for directory configuration. */
struct dav_ns_dir_conf
{
    const char *context_key;
    dmlite_manager *manager;

    struct redirect_cfg redirect;

    char *anon_user, *anon_group;
    dir_flags flags;

    unsigned max_replicas;

    apr_array_header_t *trusted_dns;

    const char *macaroon_secret;
    size_t macaroon_secret_size;
    
    const char *delegation_service;
    const char *proxy_cache;
};
typedef struct dav_ns_dir_conf dav_ns_dir_conf;

/**
 * @brief This structure handles the data the module needs to keep track of
 *        a resource coming from a DPM service typedef done in mod_dav.h.
 */
struct dav_resource_private
{
    request_rec *request;
    dav_ns_server_conf *s_conf;
    dav_ns_dir_conf *d_conf;

    dmlite_context *ctx; /**< dmlite context. */

    const char *sfn;
    const char *redirect; /**< Allocated only when needed. */
    dmlite_xstat stat;

    char metalink; /**< != 0 if the user requested the metalink */
    char new_ui;   /**< != 0 if the user requested the new UI */
    char force_secure; /**< != 0 if the user requested secure redirection */
    char is_virtual;
    dmlite_location* virtual_location;
    dmlite_credentials* user_creds;
    
    int copy_already_redirected;
};

/**
 * Insert all supported liveprops into the output.
 * @param r        The request.
 * @param resource The resource being processed.
 * @param what     What to insert.
 * @param phdr     Where to insert.
 */
void dav_ns_insert_all_liveprops(request_rec *r, const dav_resource *resource,
        dav_prop_insert what, apr_text_header *phdr);

/**
 * Find the property ID so the value can be retrieved later.
 * @param resource The resource being processed.
 * @param ns_uri   The property namespace (e.g. DAV:)
 * @param name     The property name      (e.g. getcontentlength)
 * @param hooks    Where to put the liveprop hooks.
 * @return         The property ID, or 0 if not found.
 */
int dav_ns_find_liveprop(const dav_resource *resource, const char *ns_uri,
        const char *name, const dav_hooks_liveprop **hooks);

/**
 * Get the location of a file, and set the redirect if needed.
 * @param pool                  Pool for memory allocation.
 * @param info                  Input/output parameter. sfn must be set. location will be set.
 * @param force_secure_redirect If != 0, the redirection will be https
 * @return     NULL on success.
 */
dav_error *dav_ns_get_location(apr_pool_t *pool, dav_resource_private *info,
        char force_secure_redirect);

/**
 * Handle POST method.
 */
dav_error *dav_ns_handle_post(const dav_resource *resource, ap_filter_t *output);

/**
 * Handle Want-Digest header
 * @param r         The request.
 * @param resource  The resource being processed.
 * @param output    The checksum is put here if Want-Digest is passed, left as it was othersize.
 * @param outsize   Size of the buffer pointed by output
 * @return NULL on success.
 */
dav_error* dav_ns_digest_header(request_rec *r, const dav_resource *resource,
    char* output, size_t outsize);

#endif
