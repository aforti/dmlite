/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DELEGATION_H
#define DELEGATION_H

#include <httpd.h>

/** @brief  Return a pointer to a malloc'd string holding a URL-encoded
 *          (RFC 1738) version of "in" . Only A-Z a-z 0-9 . _ - are passed
 *          through unmodified.
 */
char *dav_deleg_client_name_encode(apr_pool_t *pool, const char *in);

/** @brief Generates a delegation ID from the values exported by gridsite */
char* dav_deleg_make_delegation_id(apr_pool_t* pool, apr_table_t* table);

/** @brief Returns the path of the delegated proxy, if existing and valid. NULL
 *         otherwise.
 */
char* dav_deleg_get_proxy(request_rec* r, const char* proxy_dir,
        const char* user_dn);

#endif
