/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <httpd.h>
#include <http_log.h>

#include "mod_lcgdm_disk.h"
#include "../shared/utils.h"

/** Internal structure for supported properties */
struct prop_handler
{
    const char *ns;
    const char *name;
    char *(*handler)(dav_db *db);
};

/** Internal structure for property DB handling */
struct dav_db
{
    /* Context */
    apr_pool_t *pool;
    const dav_resource *resource;
    request_rec *request;
    /* Read-only? */
    int ro;
    /* Used to iterate by dav_disk_propdb_*name */
    struct prop_handler *iter;
};

/**
 * Open the properties DB
 * @param p The pool used to allocate memory
 * @param resource The resource being processed
 * @param ro       Read-only mode (1)
 * @param pdb      Here we put the created dav_db
 * @return         NULL on success
 */
static dav_error *dav_disk_propdb_open(apr_pool_t *p,
        const dav_resource *resource, int ro, dav_db **pdb)
{
    dav_db *db;

    /* Dead properties not supported yet (for read only, we pretend, so mod_dav works) */
    if (!ro)
        return dav_shared_new_error(resource->info->request, NULL,
                HTTP_NOT_IMPLEMENTED, "Write mode for properties not supported");

    /* Initialize */
    db = apr_pcalloc(p, sizeof(dav_db));
    db->pool = p;
    db->resource = resource;
    db->request = resource->info->request;
    db->ro = ro;

    db->iter = NULL;

    *pdb = db;

    return NULL ;
}

/**
 * Closes the property handler
 * @param db The DB to close
 */
static void dav_disk_propdb_close(dav_db *db)
{
    /* Nothing to do here */
    (void) db;
}

/**
 * Define namespaces the values may need
 * @param db
 * @param xi
 * @return
 */
static dav_error *dav_disk_propdb_define_namespaces(dav_db *db,
        dav_xmlns_info *xi)
{
    (void) xi;
    return dav_shared_new_error(db->request, NULL, HTTP_NOT_IMPLEMENTED,
            "dav_disk_propdb_define_namespaces");
}

/**
 * Put the value for the specified property into phdr
 * @param db    The database being examinated
 * @param name  The property name
 * @param xi    Namespace handling
 * @param phdr  Where to attach the value
 * @param found Must be set to 1 if the attribute is found
 * @return      NULL on success
 */
static dav_error *dav_disk_propdb_output_value(dav_db *db,
        const dav_prop_name *name, dav_xmlns_info *xi, apr_text_header *phdr,
        int *found)
{
    /* Ignore */
    (void) db;
    (void) name;
    (void) xi;
    (void) phdr;

    *found = 0;
    return NULL ;
}

/**
 * Build a mapping from "global" namespaces into provider-local namespace identifiers.
 * This is done before calling store hook
 * @param db
 * @param namespaces
 * @param mapping
 * @return
 */
static dav_error *dav_disk_propdb_map_namespaces(dav_db *db,
        const apr_array_header_t *namespaces, dav_namespace_map **mapping)
{
    (void) namespaces;
    (void) mapping;
    return dav_shared_new_error(db->request, NULL, HTTP_NOT_IMPLEMENTED,
            "dav_disk_propdb_map_namespaces");
}

/**
 * Store a property value
 * @param db
 * @param name
 * @param elem
 * @param mapping
 * @return
 */
static dav_error *dav_disk_propdb_store(dav_db *db, const dav_prop_name *name,
        const apr_xml_elem *elem, dav_namespace_map *mapping)
{
    (void) name;
    (void) elem;
    (void) mapping;
    return dav_shared_new_error(db->request, NULL, HTTP_NOT_IMPLEMENTED,
            "dav_disk_propdb_store");
}

/**
 * Remove a property
 * @param db
 * @param name
 * @return
 */
static dav_error *dav_disk_propdb_remove(dav_db *db, const dav_prop_name *name)
{
    (void) name;
    return dav_shared_new_error(db->request, NULL, HTTP_NOT_IMPLEMENTED,
            "dav_disk_propdb_remove");
}

/**
 * Check if a given property is already defined
 * @param db
 * @param name
 * @return
 */
static int dav_disk_propdb_exists(dav_db *db, const dav_prop_name *name)
{
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, db->request,
            "dav_disk_propdb_exists not implemented (%s:%s)", name->ns,
            name->name);
    return 0;
}

/**
 * Return the first supported property
 * @param db    The active DB instance
 * @param pname Where to put the property
 * @return      NULL on success
 */
static dav_error *dav_disk_propdb_first_name(dav_db *db, dav_prop_name *pname)
{
    (void) db;
    /* Not supported. mod_dav is picky, though, and we will have trouble if this
     * returns a "not-implemented" error */
    pname->ns = pname->name = NULL;
    return NULL ;
}

/**
 * Iterates to the next supported property
 * @param db    The active DB instance
 * @param pname Where to put the property
 * @return      NULL on success
 */
static dav_error *dav_disk_propdb_next_name(dav_db *db, dav_prop_name *pname)
{
    (void) db;
    /* Not supported. mod_dav is picky, though, and we will have trouble if this
     * returns a "not-implemented" error */
    pname->ns = pname->name = NULL;
    return NULL ;
}

/**
 * Get rollback context
 * @param db
 * @param name
 * @param prollback
 * @return
 */
static dav_error *dav_disk_propdb_get_rollback(dav_db *db,
        const dav_prop_name *name, dav_deadprop_rollback **prollback)
{
    (void) name;
    (void) prollback;
    return dav_shared_new_error(db->request, NULL, HTTP_NOT_IMPLEMENTED,
            "dav_disk_propdb_get_rollback");
}

/**
 * Apply rollback
 * @param db
 * @param rollback
 * @return
 */
static dav_error *dav_disk_propdb_apply_rollback(dav_db *db,
        dav_deadprop_rollback *rollback)
{
    (void) rollback;
    return dav_shared_new_error(db->request, NULL, HTTP_NOT_IMPLEMENTED,
            "dav_disk_apply_rollback");
}

/** Properties hooks */
const dav_hooks_db dav_disk_hooks_db = {
        dav_disk_propdb_open, dav_disk_propdb_close,
        dav_disk_propdb_define_namespaces, dav_disk_propdb_output_value,
        dav_disk_propdb_map_namespaces, dav_disk_propdb_store,
        dav_disk_propdb_remove, dav_disk_propdb_exists,
        dav_disk_propdb_first_name, dav_disk_propdb_next_name,
        dav_disk_propdb_get_rollback, dav_disk_propdb_apply_rollback, NULL /* Context */
};
