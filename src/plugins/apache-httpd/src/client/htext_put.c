/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <curl/curl.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "htext.h"
#include "htext_private.h"

/**
 * Method called by Curl to read data from the file
 * @param buffer Where to put the data
 * @param size   The size of the element
 * @param nmemb  The number of elements
 * @param pp     A pointer to the htext_partial
 * @return       The number of bytes read
 */
static size_t htext_put_read(char *buffer, size_t size, size_t nmemb, void *pp)
{
    htext_chunk *partial = (htext_chunk*) pp;
    size_t remaining, reqsize;

    reqsize = size * nmemb;
    remaining = partial->end - (GETIO(partial->handle) ->tell(partial->fd)) + 1;

    if (reqsize > remaining)
        reqsize = remaining;

    return GETIO(partial->handle) ->read(buffer, reqsize, 1, partial->fd);
}

/**
 * Method called by Curl to seek (and other actions) in a file
 * @param handle The CURL handle
 * @param cmd    The command to perform
 * @param pp     A pointer to the htext_partial
 * @return       CURLIOE_OK o on success
 */
static curlioerr htext_put_ioctl(CURL *handle, int cmd, void *pp)
{
    (void) handle;

    htext_chunk *partial = (htext_chunk*) pp;
    switch (cmd) {
        case CURLIOCMD_NOP:
            break;
        case CURLIOCMD_RESTARTREAD:
            GETIO(partial->handle) ->seek(partial->fd, partial->start,
                    SEEK_SET);
            break;
        default:
            return CURLIOE_UNKNOWNCMD;
    }
    return CURLIOE_OK;
}

/**
 * Implements a partial download
 * @param pp A pointer to a htext_partial structure
 * @return NULL on return
 */
static void* htext_put_subthread(void *pp)
{
    htext_chunk *partial = (htext_chunk*) pp;
    char err_buffer[CURL_ERROR_SIZE];
    char range_buffer[128];

    /* Set the function's user data */
    curl_easy_setopt(partial->curl, CURLOPT_HEADERDATA, partial);
#if LIBCURL_VERSION_NUM >= 0x073200
    curl_easy_setopt(partial->curl, CURLOPT_XFERINFODATA, partial);
#else
    curl_easy_setopt(partial->curl, CURLOPT_PROGRESSDATA, partial);
#endif
    curl_easy_setopt(partial->curl, CURLOPT_ERRORBUFFER, err_buffer);
    curl_easy_setopt(partial->curl, CURLOPT_WRITEDATA, partial);
    curl_easy_setopt(partial->curl, CURLOPT_READDATA, partial);
    curl_easy_setopt(partial->curl, CURLOPT_IOCTLDATA, partial);
    curl_easy_setopt(partial->curl, CURLOPT_DEBUGDATA, partial);
    curl_easy_setopt(partial->curl, CURLOPT_SSL_CTX_DATA, partial);

    /* Range (not for last empty PUT) */
    if (partial->nchunks > 1 && partial->index >= 0) {
        sprintf(range_buffer, "Content-Range: bytes %zu-%zu/%zu",
                partial->start, partial->end, partial->total);
        partial->headers = curl_slist_append(partial->headers, range_buffer);
    }
    curl_easy_setopt(partial->curl, CURLOPT_HTTPHEADER, partial->headers);

    curl_easy_setopt(partial->curl, CURLOPT_INFILESIZE_LARGE,
            *(partial->chunk_total));

    /* Perform */
    if (curl_easy_perform(partial->curl) != CURLE_OK) {
        partial->error_string = strdup(err_buffer);
        partial->handle->status = HTEXTS_FAILED;
        sem_post(&(partial->final));
    }

    /* Curl blocks, so here we are done */
    return NULL ;
}

void *htext_put_method(void *h)
{
    htext_handle *handle = (htext_handle*) h;
    CURL *curl;
    htext_chunk *partial_array;
    int i, nchunks;
    size_t stream_size, last_size;
    char err_buffer[CURL_ERROR_SIZE];
    off_t fsize;
    int canceled;
    curl_version_info_data *curl_ver = curl_version_info(CURLVERSION_NOW);

    /* Create and initialize CURL handle with common stuff */
    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buffer);
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, htext_header_callback);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
#if LIBCURL_VERSION_NUM >= 0x073200
    if (curl_ver->version_num >= 0x073200) {
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, htext_progress_callback);
    } else {
        curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, htext_progress_old_callback);
    }
#else
    curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, htext_progress_old_callback);
#endif
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, htext_write_callback);
    curl_easy_setopt(curl, CURLOPT_READFUNCTION, htext_put_read);
    curl_easy_setopt(curl, CURLOPT_IOCTLFUNCTION, htext_put_ioctl);
    curl_easy_setopt(curl, CURLOPT_URL, GETSTR(handle, HTEXTOP_DESTINATIONURL));
    curl_easy_setopt(curl, CURLOPT_USERAGENT, GETSTR(handle, HTEXTOP_CLIENTID));
    curl_easy_setopt(curl, CURLOPT_CAPATH, GETSTR(handle, HTEXTOP_CAPATH));
    curl_easy_setopt(curl, CURLOPT_CAINFO, GETSTR(handle, HTEXTOP_CAFILE));
    // CURL doesn't provide direct interface for "CRLPATH", use openssl context directly
    curl_easy_setopt(curl, CURLOPT_SSL_CTX_FUNCTION, htext_sslctx_callback);
    curl_easy_setopt(curl, CURLOPT_CRLFILE, GETSTR(handle, HTEXTOP_CRLFILE));
    curl_easy_setopt(curl, CURLOPT_SSLCERT, GETSTR(handle, HTEXTOP_USERCERTIFICATE));
    curl_easy_setopt(curl, CURLOPT_SSLKEY, GETSTR(handle, HTEXTOP_USERPRIVKEY));
    curl_easy_setopt(curl, CURLOPT_SSLKEYPASSWD, GETSTR(handle, HTEXTOP_USERPRIVKEYPASS));
    //curl_easy_setopt(curl, CURLOPT_SSL_CIPHER_LIST, htext_cipher_suite());
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, GETINT(handle, HTEXTOP_VERIFYPEER));
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, GETINT(handle, HTEXTOP_VERIFYPEER)?2:0);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);

    if (GETINT(handle, HTEXTOP_VERBOSITY)> 1) {
        curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, htext_debug_callback);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    }

#if LIBCURL_VERSION_NUM >= 0x072600
    if (curl_ver->version_num >= 0x072600) {
        // Require a minimum speed from the transfer
        // (default: must move at least 1MB every 2 minutes - roughly 8KB/s)
        if (GETINT(handle, HTEXTOP_LOW_SPEED_TIME) > 0 && GETINT(handle, HTEXTOP_LOW_SPEED_LIMIT) > 0) {
            curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, GETINT(handle, HTEXTOP_LOW_SPEED_TIME));
            curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, GETINT(handle, HTEXTOP_LOW_SPEED_LIMIT));
        }
    }
#endif

    /* Shared */
    curl_easy_setopt(curl, CURLOPT_SHARE, handle->curl_share);

    /* Calculate stream size and final stream size */
    nchunks = GETINT(handle, HTEXTOP_NUMBEROFSTREAMS);

    fsize = GETIO(handle) ->size(GETSTR(handle, HTEXTOP_SOURCEURL),
    GETPTR(handle, HTEXTOP_IO_HANDLER_DATA));
    if (fsize < 0) {
        htext_strerror_r(errno, err_buffer, sizeof(err_buffer));
        htext_error(handle, err_buffer);
        curl_easy_cleanup(curl);
        return NULL ;
    }

    if (fsize % nchunks == 0) {
        stream_size = last_size = fsize / nchunks;
    }
    else {
        stream_size = fsize / nchunks;
        last_size = stream_size + (fsize % nchunks);
    }

    /* Set up partials */
    handle->partial_total = calloc(sizeof(size_t), nchunks);
    handle->partial_done = calloc(sizeof(size_t), nchunks);
    handle->partial_rconn = calloc(sizeof(char*), nchunks);
    handle->partials = nchunks;
    partial_array = calloc(sizeof(htext_chunk), nchunks);

    /* Spawn a thread per partial */
    handle->status = HTEXTS_WAITING;
    htext_log(handle, "Doing the first PUT to see where to perform");

    for (i = 0; i < nchunks; ++i) {
        htext_partial_init(&(partial_array[i]));
        partial_array[i].index = i;
        partial_array[i].nchunks = nchunks;
        partial_array[i].handle = handle;
        partial_array[i].curl = curl_easy_duphandle(curl);
        partial_array[i].chunk_total = &(handle->partial_total[i]);
        partial_array[i].chunk_done = &(handle->partial_done[i]);
        partial_array[i].chunk_rconn = &(handle->partial_rconn[i]);
        partial_array[i].total = fsize;
        partial_array[i].headers = htext_copy_slist(handle->headers);

        /* duphandle doesn't duplicate this :( */
        curl_easy_setopt(partial_array[i].curl, CURLOPT_SHARE,
                handle->curl_share);

        /* Open */
        partial_array[i].fd = GETIO(handle) ->open(
                GETSTR(handle, HTEXTOP_SOURCEURL),
                "rb",
                GETPTR(handle, HTEXTOP_IO_HANDLER_DATA));
        if (!partial_array[i].fd) {
            htext_strerror_r(errno, err_buffer, sizeof(err_buffer));
            htext_error(handle, err_buffer);

            break;
        }

        /* Range and seek */
        partial_array[i].start = i * stream_size;
        if (i < handle->partials - 1)
            partial_array[i].end = partial_array[i].start + stream_size - 1;
        else
            partial_array[i].end = partial_array[i].start + last_size - 1;

        *(partial_array[i].chunk_total) = partial_array[i].end
                - partial_array[i].start + 1;

        if (GETIO(handle)->seek(partial_array[i].fd, partial_array[i].start,
                SEEK_SET) < 0) {
            handle->status = HTEXTS_FAILED;
            break;
        }

        /* Launch */
        pthread_create(&(partial_array[i].thread), NULL, htext_put_subthread,
                &(partial_array[i]));

        /* Lock until the first one gets the final location */
        if (i == 0) {
            sem_wait(&(partial_array[0].final));

            /* Check code */
            if (partial_array[0].http_status >= 400) {
                handle->http_status = partial_array[0].http_status;
                handle->status = HTEXTS_FAILED;
            }

            /* If sem_wait returned, and we haven't failed, then, we are running! */
            if (handle->status != HTEXTS_FAILED) {
                handle->status = HTEXTS_RUNNING;
                htext_log(handle, "Uploading (%i streams)", nchunks);
                /* If we got a new location, propagate */
                if (partial_array[0].location) {
                    curl_easy_setopt(curl, CURLOPT_URL,
                            partial_array[0].location);
                }
            }
            /* Something broke :( */
            else {
                break;
            }
        }
    }

    /* Wait for all of them and clean up */
    /* Done backwards in case we failed */
    if (i == nchunks)
        --i;
    while (i >= 0) {
        canceled = 0;
        if (partial_array[i].thread != 0) {
            /* Wait, or kill if failed */
            if (handle->status == HTEXTS_FAILED) {
                htext_log(handle, "Cancelling put thread idx %d", i);
                pthread_cancel(partial_array[i].thread);
                canceled = 1;
            }
            
            pthread_join(partial_array[i].thread, NULL );
        }

        if (handle->status != HTEXTS_FAILED) {
            handle->http_status = partial_array[i].http_status;

            if (handle->http_status < 200 || handle->http_status >= 300) {
                handle->status = HTEXTS_FAILED;

                if (!handle->error_string) {
                    /* Propagate first error */
                    if (partial_array[i].error_string)
                        handle->error_string = strdup(partial_array[i].error_string);
                    else if (partial_array[i].http_response)
                        handle->error_string = strdup(partial_array[i].http_response);
                }

                htext_log(handle, "Bad HTTP code %d in put thread idx %d: %s", i, handle->http_status, handle->error_string ? handle->error_string : "");
            }
        }
        else if (!canceled && partial_array[i].error_string) {
            /* Report all errors for finished threads */
            htext_log(handle, "Error in put thread idx %d: %s", i, partial_array[i].error_string);

            if (!handle->error_string)
                handle->error_string = strdup(partial_array[i].error_string);
        }

        htext_partial_clean(&(partial_array[i]));

        --i;
    }

    /* HEAD to close */
    if (handle->status == HTEXTS_RUNNING
        && !GETINT(handle, HTEXTOP_NOHEAD)
        && nchunks > 1) {
        size_t unused;
        htext_chunk control;

        htext_partial_init(&control);
        control.index = -1;
        control.start = control.end = 0;
        control.handle = handle;
        control.curl = curl_easy_duphandle(curl);
        control.chunk_done = control.chunk_total = &unused;

        htext_log(handle, "Closing");

        /* No writing */
        curl_easy_setopt(control.curl, CURLOPT_UPLOAD, 0);
        curl_easy_setopt(control.curl, CURLOPT_NOBODY, 1);
        curl_easy_setopt(control.curl, CURLOPT_HEADERDATA, &control);

        /* Indicate this is a HEAD-for-closing */
        control.headers = curl_slist_append(control.headers,
                "X-Multistreams: close");
        curl_easy_setopt(control.curl, CURLOPT_HTTPHEADER, control.headers);

        /* duphandle doesn't duplicate this :( */
        curl_easy_setopt(control.curl, CURLOPT_SHARE, handle->curl_share);

        /* We don't want this output, as it might be a failure that
         * can be ignored, and it will just confuse the user */
        curl_easy_setopt(control.curl, CURLOPT_WRITEFUNCTION,htext_dummy_write_callback);

    /* Debug */
    if (GETINT(handle, HTEXTOP_VERBOSITY) > 1) {
      curl_easy_setopt(control.curl, CURLOPT_DEBUGFUNCTION, htext_debug_callback);
      curl_easy_setopt(control.curl, CURLOPT_VERBOSE,       1);
      curl_easy_setopt(control.curl, CURLOPT_DEBUGDATA,     &control);
    }

    /* Perform */
    if (curl_easy_perform(control.curl) != CURLE_OK)
      htext_error(handle, err_buffer);

    handle->http_status = control.http_status;
    if (control.http_status < 200 || control.http_status >= 300)
        htext_error(handle, "Final HEAD failed with code %d (%s)", control.http_status,
                    control.http_response ? control.http_response : "no response header");

    htext_partial_clean(&control);
  }

    /* Done */
    if (handle->status == HTEXTS_RUNNING) {
        handle->status = HTEXTS_SUCCEEDED;
    }

    /* Call transfer done callback */
    if (GETPTR(handle, HTEXTOP_COPY_DONE)) {
        void (*copy_done)(htext_handle *, int, void *) = GETPTR(handle, HTEXTOP_COPY_DONE);
        copy_done(handle, handle->status, GETPTR(handle, HTEXTOP_COPY_DONE_DATA));
    }

    curl_easy_cleanup(curl);
    free(partial_array);

    return NULL ;
}
