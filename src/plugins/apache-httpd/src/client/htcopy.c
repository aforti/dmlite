/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <getopt.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "htext.h"

#define CLEAR_LINE       "\e[2K"
#define LABEL_OPEN_ATTR  "\e[1m"
#define LABEL_CLOSE_ATTR "\e[0m"

static char     statusStr[80] = "";
pthread_mutex_t statusMutex;

static int outputIsTty = 0;



static void usage(const char *bin)
{
    printf(
            "\
Usage: %s (options) [source] [destination]\n\
\n\
  -h, --help          Shows this help\n\
  -v, --verbose       Verbose\n\
  -l, --log           Log file\n\
\n\
  -n, --stream-number Number of streams to use\n\
  -b, --buffer-size   Internal buffer size\n\
\n\
  -E, --user-cert         The user certificate in PEM format\n\
      --user-key          The user private key in PEM format\n\
      --key-pass          The private key password\n\
      --ca-path           Use the certificate directory to verify the remote certificate\n\
      --ca-bundle         Use the certificate file to verify the remote certificate\n\
      --insecure          Do no check the peer certificate\n\
      --no-head           Do not perform HEAD requests (for file size in get/put)\n\
      --dest-is-active    Do a COPY on the destination using the Source header (extension)\n\
                          Normal behavior implies COPY on the source using Destination header\n\
\n\
  -D, --delegation        The delegation CGI path (i.e. '/gridsite-delegation')\
\n\n\
Report bugs to dpm-devel@cern.ch\n",
            bin);
}

/**
 * Parse the options and set the corresponding in the output parameters
 * @return The number of set options. 0 for help.
 */
static int get_options(int argc, char **argv, int *verbose, char **log,
        int *nstreams, int *buffersize, char **ucert, char **ukey, char **upass,
        char **delegation, char **capath, char **cafile, int *insecure,
        int *nohead, char **source, char **destination, int *use_source_header)
{
    int help = 0;
    int option_index = 0;

    static struct option long_options[] =
    {
        {"help",          no_argument      , 0, 'h'},
        {"verbose",       no_argument      , 0, 'v'},
        {"log",           required_argument, 0, 'l'},
        {"insecure",      no_argument      , 0, 'i'},
        {"stream-number", required_argument, 0, 'n'},
        {"buffer-size",   required_argument, 0, 'b'},
        {"user-cert",     required_argument, 0, 'E'},
        {"user-key",      required_argument, 0, 'k'},
        {"key-pass",      required_argument, 0, 'p'},
        {"delegation",    required_argument, 0, 'D'},
        {"ca-path",       required_argument, 0, '1'},
        {"ca-file",       required_argument, 0, '2'},
        {"no-head",       no_argument      , 0, '3'},
        {"dest-is-active",no_argument,       0, 'f'},
        {0, 0, 0, 0}
    };

    const char *short_options = "hvl:n:b:E:D:f";

    /* Options */
    while (1) {
        int c;

        c = getopt_long(argc, argv, short_options, long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
            case 'h':
                help = 1;
                break;
            case 'v':
                (*verbose)++;
                break;
            case 'l':
                *log = optarg;
                break;
            case 'n':
                *nstreams = atoi(optarg);
                break;
            case 'b':
                *buffersize = atoi(optarg);
                break;
            case 'i':
                *insecure = 1;
                break;
            case 'E':
                *ucert = optarg;
                break;
            case 'k':
                *ukey = optarg;
                break;
            case 'p':
                *upass = optarg;
                break;
            case 'D':
                *delegation = optarg;
                break;
            case '1':
                *capath = optarg;
                break;
            case '2':
                *cafile = optarg;
                break;
            case '3':
                *nohead = 1;
                break;
            case 'f':
                *use_source_header = 1;
                break;
            default:
                exit(2);
        }
    }

    /* Get from environment some of them if not specified */
    if (!*ucert) {
        if (!(*ucert = getenv("X509_USER_PROXY")))
            *ucert = getenv("X509_USER_CERT");
    }
    if (!*ukey) {
        if (*ucert != getenv("X509_USER_CERT"))
            *ukey = *ucert;
        else
            *ukey = getenv("X509_USER_KEY");
    }
    if (!*capath) {
        *capath = getenv("X509_CERT_DIR");
    }
    if (!*cafile) {
        *cafile = *ucert; // Otherwise, in some platforms the full proxy won't be sent
    }

    /* Other defaults*/
    if (!*log) {
        *log = "htcopy.log";
    }

    /* Remaining */
    if (optind < argc)
        *source = argv[optind];
    if (optind + 1 < argc)
        *destination = argv[optind + 1];

    return help ? 0 : argc - 1;
}

/**
 * Log callback
 * @param handle
 * @param type
 * @param msg
 * @param size
 * @param ud
 */
static void log_callback(htext_handle *handle, HTEXT_LOG_TYPE type,
        const char *msg, size_t size, void *ud)
{
    (void) handle;

    FILE *logF = (FILE*) ud;

    pthread_mutex_lock(&statusMutex);

    switch (type) {
        case HTEXT_LOG:
            strncpy(statusStr, msg, sizeof(statusStr));
            statusStr[sizeof(statusStr) - 1] = '\0';
            break;
        case HTEXT_HEADER_IN:
            if (logF)
                fputs("> ", logF);
            break;
        case HTEXT_HEADER_OUT:
            if (logF)
                fputs("< ", logF);
            break;
        case HTEXT_TEXT:
            if (logF)
                fputs("| ", logF);
            break;
        default:
            break;
    }

    if (logF) {
        fwrite(msg, sizeof(char), size, logF);
        if (msg[size - 1] != '\n')
            fputc('\n', logF);
        fflush(logF);
    }

    pthread_mutex_unlock(&statusMutex);
}

/**
 * Generates a printable version of the size
 * @param str  Where to put the string
 * @param max  Max length
 * @param size The size in bytes
 * @return     str
 */
static char *size_str(char *str, size_t max, size_t size)
{
    if (size > 1073741823)
        snprintf(str, max, "%.1fG", (float) size / 1073741824.0);
    else if (size > 1048575)
        snprintf(str, max, "%.1fM", (float) size / 1048576.0);
    else if (size > 1023)
        snprintf(str, max, "%.1fK", (float) size / 1024.0);
    else
        snprintf(str, max, "%zu bytes", size);
    return str;
}

/**
 * Generates a printable time as hours:minutes:seconds
 * @param str  Where to put the string
 * @param max  Max length
 * @param size The size in bytes
 * @return     str
 */
static char *time_str(char *str, size_t max, time_t time)
{
    int s, m, h;
    s = time % 60;
    m = time / 60;
    h = m / 60;
    m = m % 60;

    snprintf(str, max, "%02d:%02d:%02d", h, m, s);
    return str;
}

/**
 * Prints Label:value, with control codes if stdout is a terminal
 * @param label The part before with hightlight
 * @param value The value
 */
static void print_label(const char *label, const char *value, ...)
{
    va_list args;

    va_start(args, value);

    if (outputIsTty == 1) {
        printf(CLEAR_LINE LABEL_OPEN_ATTR "%s" LABEL_CLOSE_ATTR, label);
        vprintf(value, args);
    }
    else {
        printf("%s", label);
        vprintf(value, args);
    }

    va_end(args);
}


/**
 * Main function
 * @param argc The number of arguments
 * @param argv The arguments. The first must be the binary name.
 * @return 0 on success.
 */
int main(int argc, char **argv)
{
    htext_handle *handle;
    int nstreams = 1, insecure = 0, verbose = 0, buffersize = 0, nohead = 0;
    int use_source_header = 0;
    int status, retcode;
    char *ucert = NULL, *ukey = NULL, *upass = NULL;
    char *capath = NULL, *cafile = NULL;
    char *delegation = NULL;
    char *source = NULL, *destination = NULL;
    char strbuf[24];
    const char *err_str = NULL;
    char *log = NULL;
    FILE *logF = NULL;

    /* Printing to TTY? */
    outputIsTty = isatty(fileno(stdout));

    /* Defaults and options */
    ucert = ukey = upass = NULL;
    capath = cafile = NULL;
    source = destination = NULL;

    if (!get_options(argc, argv, &verbose, &log, &nstreams, &buffersize, &ucert,
            &ukey, &upass, &delegation, &capath, &cafile, &insecure, &nohead,
            &source, &destination, &use_source_header)) {
        usage(argv[0]);
        return 0;
    }

    /* Check */
    if (!source) {
        fputs("Source is mandatory!\n", stderr);
        exit(1);
    }
    if (!destination) {
        fputs("Destination is mandatory!\n", stderr);
        exit(1);
    }

    /* Create and initialize handle */
    if (htext_global_init() != 0) {
        fputs("Error: Could not initialize globals\n", stderr);
        exit(1);
    }
    if (!(handle = htext_init())) {
        fputs("Error: Not enough memory\n", stderr);
        exit(1);
    }

    /* Summarize if verbose */
    if (verbose) {
        print_label("Source:            ", "%s\n", source);
        print_label("Destination:       ", "%s\n", destination);
        print_label("Number of streams: ", "%d\n", nstreams);
        print_label("Buffer size:       ", "%d\n", buffersize);
        print_label("User certificate:  ", "%s\n", ucert ? ucert : "Not set");
        print_label("User key:          ", "%s\n", ukey ? ukey : "Not set");
        print_label("Delegation         ", "%s\n", delegation ? delegation : "Use default header");
        print_label("CA Path:           ", "%s\n", capath ? capath : "Not set");
        print_label("CA File:           ", "%s\n", cafile ? cafile : "Not set");
        print_label("Verify peer:       ", "%d\n", !insecure);
        print_label("Log file:          ", "%s\n", log);
        putchar('\n');

        logF = fopen(log, "w");
        if (logF == NULL ) {
            perror("Could not open the log file");
            return 1;
        }

    }
    else
        logF = NULL;

    /* Init the mutex for the status string */
    pthread_mutex_init(&statusMutex, NULL );

    /* Setup the handle */
    htext_setopt(handle, HTEXTOP_SOURCEURL, source);
    htext_setopt(handle, HTEXTOP_DESTINATIONURL, destination);
    htext_setopt(handle, HTEXTOP_NUMBEROFSTREAMS, nstreams);
    htext_setopt(handle, HTEXTOP_BUFFERSIZE, buffersize);

    htext_setopt(handle, HTEXTOP_USERCERTIFICATE, ucert);
    htext_setopt(handle, HTEXTOP_USERPRIVKEY, ukey);
    htext_setopt(handle, HTEXTOP_USERPRIVKEYPASS, upass);

    htext_setopt(handle, HTEXTOP_DELEGATION_URL, delegation);

    htext_setopt(handle, HTEXTOP_CAPATH, capath);
    htext_setopt(handle, HTEXTOP_CAFILE, cafile);
    htext_setopt(handle, HTEXTOP_VERIFYPEER, !insecure);
    htext_setopt(handle, HTEXTOP_NOHEAD, nohead);
    htext_setopt(handle, HTEXTOP_USE_COPY_FROM_SOURCE, use_source_header);

    htext_setopt(handle, HTEXTOP_LOGCALLBACK, log_callback);
    htext_setopt(handle, HTEXTOP_LOGCALLBACK_DATA, logF);
    if (verbose > 1)
        htext_setopt(handle, HTEXTOP_VERBOSITY, verbose - 1);

    /* Run */
    if (htext_perform(handle) != 0) {
        perror("Error: Could not perform the action");
        return 1;
    }

    /* Progress */
    time_t start, currentTime, prevTime;
    size_t currentSpeed = 0, peakSpeed = 0;
    size_t globalTotal = 0;
    size_t globalDone = 0, prevDone = 0;

    prevTime = start = time(NULL );
    do {
        size_t *total, *done, n = 0, i;
        char **rconn;
        float percent;

        globalDone = 0;
        globalTotal = 0;

        htext_progress(handle, &n, &total, &done, &rconn);
        status = htext_status(handle);

        for (i = 0; status == HTEXTS_RUNNING && i < n; ++i) {
            globalDone += done[i];
            globalTotal += total[i];
        }

        if (globalTotal > 0)
            percent = 100.0 * ((float) globalDone / globalTotal);
        else
            percent = 0;

        /* Speed */
        currentTime = time(NULL );
        if (currentTime > prevTime) {
            currentSpeed = (globalDone - prevDone) / (currentTime - prevTime);
            prevTime = currentTime;
            prevDone = globalDone;

            if (currentSpeed > peakSpeed)
                peakSpeed = currentSpeed;
        }

        /* Print */
        print_label("Progress: ", "");
        printf("%s", size_str(strbuf, sizeof(strbuf), globalDone));
        printf("/");
        printf("%s", size_str(strbuf, sizeof(strbuf), globalTotal));
        printf(" (%.2f%%)\n", percent);

        print_label("\tElapsed:    ", "%s\n", time_str(strbuf, sizeof(strbuf), currentTime - start));
        if (currentSpeed > 0 && globalTotal > 0) {
            print_label("\tRemaining:  ", "%s\n", time_str(strbuf, sizeof(strbuf),
                        (globalTotal - globalDone) / currentSpeed));
        }
        else {
            print_label("\tRemaining:  ", "???\n");
        }
        print_label("\tSpeed:      ", "%s/s\n", size_str(strbuf, sizeof(strbuf), currentSpeed));
        print_label("\tPeak:       ", "%s/s\n", size_str(strbuf, sizeof(strbuf), peakSpeed));
        putchar('\n');

        print_label("", "%s\n", statusStr);

        if (outputIsTty)
            printf("\e[7A");

        /* Sleep a while */
        usleep(10000);

        status = htext_status(handle);
    } while (status != HTEXTS_SUCCEEDED && status != HTEXTS_FAILED &&
             status != HTEXTS_ABORTED);

    /* Last print was a jump back! */
    if (outputIsTty)
        printf("\e[7B");

    /* The user probably wants to know */
    err_str = htext_error_string(handle);
    switch (status) {
        case HTEXTS_SUCCEEDED:
            retcode = 0;
            printf("\nSuccess!\n");
            break;
        case HTEXTS_FAILED:
            retcode = 1;
            printf("\nFailed!\n");
            printf("HTTP code: %d\n", htext_http_code(handle));
            if (err_str)
                printf("Error: %s\n", err_str);
            break;
        case HTEXTS_ABORTED:
            retcode = 2;
            printf("\nAborted!\n");
            break;
    }

    /* Done here */
    htext_destroy(handle);
    if (logF)
        fclose(logF);
    pthread_mutex_destroy(&statusMutex);
    return retcode;
}
