/// @file   DomeAdapterDriver.h
/// @brief  Dome adapter
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>
#ifndef DOME_ADAPTER_DRIVER_H
#define DOME_ADAPTER_DRIVER_H

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/io.h>
#include <dmlite/cpp/poolmanager.h>
#include <dmlite/cpp/pooldriver.h>
#include <fstream>
#include "utils/DavixPool.h"


namespace dmlite {

  class DomeTalker;
  
  extern Logger::bitmask domeadapterlogmask;
  extern Logger::component domeadapterlogname;

  class DomeAdapterFactory;
  class DomeAdapterPoolHandler;

  class DomeAdapterPoolDriver : public PoolDriver {
  public:
    DomeAdapterPoolDriver(DomeAdapterFactory* factory);
    ~DomeAdapterPoolDriver();

    std::string getImplId() const throw();

    void setStackInstance(StackInstance* si)  ;
    void setSecurityContext(const SecurityContext*)  ;

    PoolHandler* createPoolHandler(const std::string& poolName)  ;

    void toBeCreated(const Pool& pool)  ;
    void justCreated(const Pool& pool)  ;
    void update(const Pool& pool)  ;
    void toBeDeleted(const Pool& pool)  ;
  private:
    StackInstance* si_;
    const SecurityContext *secCtx_;
    std::string userId_;

    /// The corresponding factory.
    DomeAdapterFactory* factory_;
    DomeTalker *talker__;
    
    friend class DomeAdapterPoolHandler;
  };

  class DomeAdapterPoolHandler: public PoolHandler {
  public:
    DomeAdapterPoolHandler(DomeAdapterPoolDriver *driver, const std::string& poolname);
    ~DomeAdapterPoolHandler();

    std::string getPoolType    (void)  ;
    std::string getPoolName    (void)  ;
    uint64_t    getTotalSpace  (void)  ;
    uint64_t    getFreeSpace   (void)  ;
    bool        poolIsAvailable(bool)  ;

    bool     replicaIsAvailable(const Replica& replica)  ;
    Location whereToRead       (const Replica& replica)  ;

    void removeReplica(const Replica&)  ;
    Location whereToWrite(const std::string&)  ;
    void cancelWrite(const Location& loc)  ;

  private:
    enum DomeFsStatus {
      FsStaticActive = 0,
      FsStaticDisabled,
      FsStaticReadOnly
    };

    std::string poolname_;
    DomeAdapterPoolDriver *driver_;

    uint64_t getPoolField(const std::string &field, uint64_t def)  ;
  };
}

#endif
