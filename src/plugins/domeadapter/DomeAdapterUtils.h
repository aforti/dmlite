/// @file   DomeAdapterUtils.h
/// @brief  Dome adapter
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>
#ifndef DOME_ADAPTER_UTILS_H
#define DOME_ADAPTER_UTILS_H

#include <dmlite/cpp/poolmanager.h>
#include <dmlite/cpp/inode.h>
#include <dmlite/cpp/utils/security.h>
#include <boost/foreach.hpp>

inline void ptree_to_xstat(const boost::property_tree::ptree &ptree, dmlite::ExtendedStat &xstat) {
  xstat.stat.st_size = ptree.get<uint64_t>("size");
  xstat.stat.st_mode = ptree.get<mode_t>("mode");
  xstat.stat.st_ino   = ptree.get<ino_t>("fileid");
  xstat.parent = ptree.get<ino_t>("parentfileid");
  xstat.stat.st_atime = ptree.get<time_t>("atime");
  xstat.stat.st_ctime = ptree.get<time_t>("ctime");
  xstat.stat.st_mtime = ptree.get<time_t>("mtime");
  xstat.stat.st_nlink = ptree.get<nlink_t>("nlink");
  xstat.stat.st_gid = ptree.get<gid_t>("gid");
  xstat.stat.st_uid = ptree.get<uid_t>("uid");
  int status = ptree.get<int>("status");
  xstat.status = static_cast<dmlite::ExtendedStat::FileStatus>(status);
  xstat.name = ptree.get<std::string>("name");
  xstat.csumtype = ptree.get<std::string>("legacycktype", "");
  xstat.csumvalue = ptree.get<std::string>("legacyckvalue", "");
  xstat.acl = dmlite::Acl(ptree.get<std::string>("acl", ""));
  xstat.deserialize(ptree.get<std::string>("xattrs", ""));
}

inline void ptree_to_groupinfo(const boost::property_tree::ptree &ptree, dmlite::GroupInfo &groupInfo) {
  groupInfo.name = ptree.get<std::string>("groupname");
  groupInfo["gid"] = ptree.get<uint64_t>("gid");
  groupInfo["banned"] = ptree.get<uint64_t>("banned");
}

inline void ptree_to_userinfo(const boost::property_tree::ptree &ptree, dmlite::UserInfo &userInfo) {
  userInfo.name = ptree.get<std::string>("username");
  userInfo["uid"] = ptree.get<uint64_t>("userid");
  userInfo["banned"] = ptree.get<int>("banned");

  std::string xattr = ptree.get<std::string>("xattr");
  if(!xattr.empty()) {
    userInfo.deserialize(xattr);
  }
}

inline void ptree_to_replica(const boost::property_tree::ptree &ptree, dmlite::Replica &replica) {
  replica.replicaid = ptree.get<int64_t>("replicaid");
  replica.fileid = ptree.get<int64_t>("fileid");
  replica.nbaccesses = ptree.get<int64_t>("nbaccesses");
  replica.atime = ptree.get<time_t>("atime");
  replica.ptime = ptree.get<time_t>("ptime");
  replica.ltime = ptree.get<time_t>("ltime");

  replica.rfn = ptree.get<std::string>("rfn", "");
  
  int status = ptree.get<int>("status");
  int type = ptree.get<int>("type");
  int rtype = ptree.get<int>("rtype");
  replica.status = static_cast<dmlite::Replica::ReplicaStatus>(status);
  replica.type = static_cast<dmlite::Replica::ReplicaType>(type);
  replica.rtype = static_cast<dmlite::Replica::ReplicaPS>(rtype);

  replica.server = ptree.get<std::string>("server");
  replica.setname = ptree.get<std::string>("setname");
  replica.deserialize(ptree.get<std::string>("xattrs"));
}



inline void displaytree(const int depth, const boost::property_tree::ptree& tree) {  
  BOOST_FOREACH( const boost::property_tree::ptree::value_type &v, tree.get_child("") ) {  

    std::string nodestr = tree.get<std::string>(v.first);  
    
    // print current node  
    std::cerr << std::string("").assign(depth*2,' ') << " - ";  
    std::cerr << v.first;  
    if ( nodestr.length() > 0 )   
      std::cerr << "=\"" << tree.get<std::string>(v.first) << "\"";  
    std::cerr << std::endl;  
    
    // recursive go down the hierarchy  
    displaytree(depth+1, v.second);  
  }  
};  


inline dmlite::Pool deserializePool(boost::property_tree::ptree::const_iterator it) {
    using namespace dmlite;
    using namespace boost::property_tree;

    
    Pool p;
    p.name = it->first;
    p.type = "filesystem";
    p["freespace"] = it->second.get<uint64_t>("freespace", 0);
    p["physicalsize"] = it->second.get<uint64_t>("physicalsize", 0);

    p["poolstatus"] = it->second.get<std::string>("poolstatus", "");
    p["s_type"] = it->second.get<std::string>("s_type", "");
    p["defsize"] = it->second.get<uint64_t>("defsize", 0);

    // fetch info about the filesystems
    std::vector<boost::any> filesystems;

    if(it->second.count("fsinfo") != 0) {
      ptree fsinfo = it->second.get_child("fsinfo");
      //displaytree(0, it->second);
      
      // iterating over servers
      for(ptree::const_iterator it2 = fsinfo.begin(); it2 != fsinfo.end(); it2++) {
        // iterating over filesystems
        for(ptree::const_iterator it3 = it2->second.begin(); it3 != it2->second.end(); it3++) {
          
            
          Extensible fs;
          uint64_t dbg;
          fs["server"] = it2->first;
          fs["fs"] = it3->first;
          
          dbg = it3->second.get<uint64_t>("fsstatus", 0);
          fs["status"] = dbg;
          
          dbg = it3->second.get<uint64_t>("activitystatus", 0);
          fs["activitystatus"] = dbg;
          
          dbg = it3->second.get<uint64_t>("freespace", 0);
          fs["freespace"] = dbg;
          
          dbg = it3->second.get<uint64_t>("physicalsize", 0);
          fs["physicalsize"] = dbg;

          filesystems.push_back(fs);
        }
      }
    }

    p["filesystems"] = filesystems;
    return p;
}

#endif
