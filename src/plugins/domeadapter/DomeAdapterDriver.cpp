/// @file   DomeAdapterDriver.cpp
/// @brief  Dome adapter catalog
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/catalog.h>
#include <iostream>

#include "DomeAdapter.h"
#include "DomeAdapterDriver.h"
#include "DomeAdapterPools.h"
#include "utils/DomeTalker.h"
#include "utils/DomeUtils.h"

#include "DomeAdapterUtils.h"
#include <boost/property_tree/json_parser.hpp>

using namespace dmlite;
#define SSTR(message) static_cast<std::ostringstream&>(std::ostringstream().flush() << message).str()

DomeAdapterPoolDriver::DomeAdapterPoolDriver(DomeAdapterFactory *factory) {
  factory_ = factory;
  secCtx_ = NULL;
  talker__ = new DomeTalker(factory_->davixPool_, secCtx_, factory_->domehead_, "GET", "dome_access");
}

DomeAdapterPoolDriver::~DomeAdapterPoolDriver() {
  delete talker__;
  talker__ = 0;
}

std::string DomeAdapterPoolDriver::getImplId() const throw() {
  return "DomeAdapterPoolDriver";
}

void DomeAdapterPoolDriver::setStackInstance(StackInstance* si)   {
  si_ = si;
}

void DomeAdapterPoolDriver::setSecurityContext(const SecurityContext* secCtx)   {
  secCtx_ = secCtx;

  // Id mechanism
  if (factory_->tokenUseIp_)
    userId_ = secCtx_->credentials.remoteAddress;
  else if (factory_->tokenUseDn_)
    userId_ = secCtx_->credentials.clientName;
  else
    userId_ = "";
}

PoolHandler* DomeAdapterPoolDriver::createPoolHandler(const std::string& poolname)   {
  return new DomeAdapterPoolHandler(this, poolname);
}


DomeAdapterPoolHandler::DomeAdapterPoolHandler(DomeAdapterPoolDriver *driver, const std::string& poolname) {
  driver_ = driver;
  poolname_ = poolname;
}

DomeAdapterPoolHandler::~DomeAdapterPoolHandler() {

}

std::string DomeAdapterPoolHandler::getPoolType(void)   {
  return "filesystem";
}

std::string DomeAdapterPoolHandler::getPoolName(void)   {
  return this->poolname_;
}

void DomeAdapterPoolDriver::toBeCreated(const Pool& pool)   {
  {
    
    
    DomeCredentials dc(secCtx_);
    talker__->setcommand(dc, "POST", "dome_addpool");

    if(!talker__->execute("poolname", pool.name)) {
      throw DmException(talker__->dmlite_code(), talker__->err());
  }
  }

  std::vector<boost::any> filesystems = pool.getVector("filesystems");
  for(unsigned i = 0; i < filesystems.size(); ++i) {
    Extensible fs = boost::any_cast<Extensible>(filesystems[i]);

    DomeCredentials dc(secCtx_);
    talker__->setcommand(dc, "POST", "dome_addfstopool");
    

    boost::property_tree::ptree params;
    params.put("server", fs.getString("server"));
    params.put("fs", fs.getString("fs"));
    params.put("poolname", pool.name);

    if(!talker__->execute(params)) {
      throw DmException(talker__->dmlite_code(), talker__->err());
    }
  }
}

void DomeAdapterPoolDriver::justCreated(const Pool& pool)   {
  // nothing to do here
}

bool contains_filesystem(std::vector<boost::any> filesystems, std::string server, std::string filesystem) {
  for(unsigned i = 0; i < filesystems.size(); ++i) {
    Extensible fs = boost::any_cast<Extensible>(filesystems[i]);
    if(fs.getString("server") == server && fs.getString("fs") == filesystem) {
      return true;
    }
  }
  return false;
}

void DomeAdapterPoolDriver::update(const Pool& pool)   {
  
  
  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "GET", "dome_statpool");
  

  if(!talker__->execute("poolname", pool.name)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }

  try {
    Pool oldpool = deserializePool(talker__->jresp().get_child("poolinfo").begin());
    std::vector<boost::any> oldfilesystems = oldpool.getVector("filesystems");
    std::vector<boost::any> filesystems = pool.getVector("filesystems");

    // detect which filesystems have been removed
    for(unsigned i = 0; i < oldfilesystems.size(); i++) {
      Extensible ext = boost::any_cast<Extensible>(oldfilesystems[i]);
      std::string server = ext.getString("server");
      std::string fs = ext.getString("fs");
      if(!contains_filesystem(filesystems, server, fs)) {
        // send rmfs to dome to remove this filesystem
        Log(Logger::Lvl1, domeadapterlogmask, domeadapterlogname, "Removing filesystem '" << fs << "' on server '" << server << "'");

        talker__->setcommand(dc, "POST", "dome_rmfs");


        boost::property_tree::ptree params;
        params.put("server", server);
        params.put("fs", fs);

        if(!talker__->execute(params)) {
          throw DmException(talker__->dmlite_code(), talker__->err());
        }
      }
    }

    // detect which filesystems need to be added
    for(unsigned i = 0; i < filesystems.size(); i++) {
      Extensible ext = boost::any_cast<Extensible>(filesystems[i]);
      std::string server = ext.getString("server");
      std::string fs = ext.getString("fs");

      std::cout << "checking " << server << " - " << fs << std::endl;
      if(!contains_filesystem(oldfilesystems, server, fs)) {
        // send addfs to dome to add this filesystem
        Log(Logger::Lvl1, domeadapterlogmask, domeadapterlogname, "Adding filesystem '" << fs << "' on server '" << server << "'");
        
        talker__->setcommand(dc, "POST", "dome_addfstopool");


        boost::property_tree::ptree params;
        params.put("server", server);
        params.put("fs", fs);
        params.put("poolname", pool.name);

        if(!talker__->execute(params)) {
          throw DmException(talker__->dmlite_code(), talker__->err());
        }
      }
    }
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << talker__->response() << "'"));
  }
}

void DomeAdapterPoolDriver::toBeDeleted(const Pool& pool)   {
  
  DomeCredentials dc(secCtx_);
  talker__->setcommand(dc, "POST", "dome_rmpool");
  

  if(!talker__->execute("poolname", pool.name)) {
    throw DmException(talker__->dmlite_code(), talker__->err());
  }
}

uint64_t DomeAdapterPoolHandler::getPoolField(const std::string &field, uint64_t def)   {
  
  DomeCredentials dc(driver_->secCtx_);
  driver_->talker__->setcommand(dc, "GET", "dome_statpool");


  if(!driver_->talker__->execute("poolname", poolname_)) {
    throw DmException(driver_->talker__->dmlite_code(), driver_->talker__->err());
  }

  try {
    return driver_->talker__->jresp().get_child("poolinfo").begin()->second.get<uint64_t>(field, def);
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(DMLITE_SYSERR(DMLITE_MALFORMED), SSTR("Error parsing json response when retrieving field '" << field << "'. Error: '" << e.what() << "' Response: '" << driver_->talker__->response() << "'"));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << driver_->talker__->response() << "'"));
  }
  
  return 0;
}

uint64_t DomeAdapterPoolHandler::getTotalSpace(void)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " Entering ");
  return this->getPoolField("physicalsize", 0);
}

uint64_t DomeAdapterPoolHandler::getFreeSpace(void)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " Entering ");
  return this->getPoolField("freespace", 0);
}

bool DomeAdapterPoolHandler::poolIsAvailable(bool write)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " Entering ");
  uint64_t poolstatus = this->getPoolField("poolstatus", std::numeric_limits<uint64_t>::max());

  if(poolstatus == FsStaticActive) {
    return true;
  }
  if(poolstatus == FsStaticDisabled) {
    return false;
  }
  if(poolstatus == FsStaticReadOnly) {
    return ! write;
  }

  throw DmException(EINVAL, SSTR("Received invalid value from Dome for poolstatus: " << poolstatus));
}

bool DomeAdapterPoolHandler::replicaIsAvailable(const Replica& replica)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " rfn: " << replica.rfn);

  if (replica.status != dmlite::Replica::kAvailable) {
    Log(Logger::Lvl3, domeadapterlogmask, domeadapterlogname, " poolname:" << poolname_ << " replica: " << replica.rfn << " has status " << replica.status << " . returns false");
    return false;
  }

  
  DomeCredentials dc(driver_->secCtx_);
  driver_->talker__->setcommand(dc, "GET", "dome_statpool");
  

  if(!driver_->talker__->execute("poolname", poolname_)) {
    throw DmException(driver_->talker__->dmlite_code(), driver_->talker__->err());
  }

  try {
    using namespace boost::property_tree;
    std::string filesystem = Extensible::anyToString(replica["filesystem"]);
    ptree fsinfo = driver_->talker__->jresp().get_child("poolinfo").get_child(poolname_).get_child("fsinfo");

    ptree::const_iterator begin = fsinfo.begin();
    ptree::const_iterator end = fsinfo.end();
    for(ptree::const_iterator it = begin; it != end; it++) {
      if(it->first == replica.server) {
        for(ptree::const_iterator it2 = it->second.begin(); it2 != it->second.end(); it2++) {
          if(it2->first == filesystem) {
            bool active = it2->second.get<int>("fsstatus") != FsStaticDisabled;
            return active;
          }
        }
      }
    }
    return false;
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << driver_->talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << driver_->talker__->response() << "'"));
  }
  
  return false;
}

void DomeAdapterPoolHandler::removeReplica(const Replica& replica)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " rfn: " << replica.rfn);
  
  
  DomeCredentials dc(driver_->secCtx_);
  driver_->talker__->setcommand(dc, "POST", "dome_delreplica");


  boost::property_tree::ptree params;
  params.put("server", DomeUtils::server_from_rfio_syntax(replica.rfn));
  params.put("pfn", DomeUtils::pfn_from_rfio_syntax(replica.rfn));

  if(!driver_->talker__->execute(params)) {
    throw DmException(driver_->talker__->dmlite_code(), driver_->talker__->err());
  }
}

void DomeAdapterPoolHandler::cancelWrite(const Location& loc)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " Entering. ");
  Replica replica;
  replica.rfn = loc[0].url.domain + ":" + loc[0].url.path;
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " rfn: " << replica.rfn);
  this->removeReplica(replica);
}

Location DomeAdapterPoolHandler::whereToWrite(const std::string& lfn)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " lfn: " << lfn);
  
  
  if (driver_->si_->contains("overwrite") && Extensible::anyToBoolean(driver_->si_->get("overwrite"))) {
    try {
      driver_->si_->getCatalog()->unlink(lfn);
    }
    catch (DmException& e) {
      if (e.code() != ENOENT) throw;
    }
  }
  
  DomeCredentials dc(driver_->secCtx_);
  driver_->talker__->setcommand(dc, "POST", "dome_put");
  

  if(!driver_->talker__->execute("pool", poolname_, "lfn", lfn)) {
    throw DmException(driver_->talker__->dmlite_code(), driver_->talker__->err());
  }

  try {
    Chunk single;

    single.url.domain = driver_->talker__->jresp().get<std::string>("host");
    single.url.path   = driver_->talker__->jresp().get<std::string>("pfn");
    single.offset = 0;
    single.size   = 0;

    single.url.query["sfn"]      = lfn;
    
    std::string userId1;
    if (driver_->si_->contains("replicate"))
      userId1 = dmlite::kGenericUser;
    else
      userId1 = driver_->userId_;
    
    single.url.query["token"]    = dmlite::generateToken(userId1, single.url.path,
                                                         driver_->factory_->tokenPasswd_,
                                                         driver_->factory_->tokenLife_, true);
    return Location(1, single);
  }
  catch(boost::property_tree::ptree_error &e) {
    throw DmException(EINVAL, SSTR("Error when parsing json response: " << e.what() << " - " << driver_->talker__->response()));
  }
  catch( ... ) {
    throw DmException(EINVAL, SSTR("Unknown error when parsing json response: '" << driver_->talker__->response() << "'"));
  }
  
}

Location DomeAdapterPoolHandler::whereToRead(const Replica& replica)   {
  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " poolname:" << poolname_ << " replica:" << replica.rfn);

  Url rloc(replica.rfn);

  Chunk single;

  single.url.domain = rloc.domain;
  single.url.path   = rloc.path;
  single.offset = 0;

  try {
    single.size   = this->driver_->si_->getCatalog()->extendedStatByRFN(replica.rfn).stat.st_size;
  }
  catch (DmException& e) {
    switch (DMLITE_ERRNO(e.code())) {
      case ENOSYS: case DMLITE_NO_INODE:
        break;
      default:
        throw;
    }
    single.size = 0;
  }

  single.url.query["token"] = dmlite::generateToken(driver_->userId_, rloc.path,
                                                    driver_->factory_->tokenPasswd_,
                                                    driver_->factory_->tokenLife_);

  Log(Logger::Lvl4, domeadapterlogmask, domeadapterlogname, " poolname:" << poolname_ << " replica:" << replica.rfn << " returns" << single.toString());
  return Location(1, single);
}
