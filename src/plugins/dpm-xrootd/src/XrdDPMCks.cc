
/* 
 * Copyright (C) 2015 by CERN/IT/ST/AD. All rights reserved.
 *
 * File:   XrdDPMCks.cc
 * Author: Fabrizio Furano
 * 
 * An XrdCks plugin providing a checksum manager
 *
 * Created on Jan 13, 2012, 10:25 AM
 */

#include <XrdDPMCks.hh>
#include <XrdOuc/XrdOucEnv.hh>
#include <XrdVersion.hh>
#include <XrdCompileVersion.hh>

#include <vector>
#include <exception>

#include "dmlite/cpp/dmlite.h"
#include "dmlite/cpp/catalog.h"
#include "XrdDPMTrace.hh"

XrdVERSIONINFO(XrdCksInit,XrdCksManager)

const char *XrdDPMCksManager::XrdDPMCksNames[] = { "adler32", "md5", "crc32" };

/******************************************************************************/
/*                  E r r o r   R o u t i n g   O b j e c t                   */
/******************************************************************************/

namespace DpmCks {
  XrdSysError Say(0, "dpmcks_");
  XrdOucTrace Trace(&Say);
  DpmCommonConfigOptions CommonConfig;
  DpmRedirConfigOptions RedirConfig;
  XrdDmStackStore dpm_ss;
}

using namespace DpmCks;
using namespace dmlite;

XrdDPMCksManager::XrdDPMCksManager(XrdSysError *erP, const char *configfn, const char *parms): XrdCks(erP) {
  
}


int XrdDPMCksManager::Del ( const char *Lfn, XrdCksData &Cks )
{

  Say.Emsg ( __func__, SSTR ( "Not implemented delchecksum(" << Lfn << "')" ).c_str() );
  return -EINVAL;

}

int XrdDPMCksManager::Calc ( const char *Lfn, XrdCksData &Cks, int doSet )
{
  DpmIdentity empty_ident;
  XrdDmStackWrap sw(dpm_ss, empty_ident);
  EPNAME("XrdDPMCksManager::Calc")
  
  dmlite::Catalog *c;
  
  try {
    c = sw->getCatalog();
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, "Unable to get Catalog instance. err: ", e.what() );
    return -EINVAL;
  } catch ( ... ) {
    Say.Emsg ( __func__, "Unable to get Catalog instance." );
    return -EINVAL;
  }
  
  if ( !c ) {
    XrdOucString errstr ( "Unable to acquire dmlite::Catalog instance" );
    errstr += ( ( Lfn && *Lfn ) ? Lfn : "[none]" );
    Say.Emsg ( "Get", errstr.c_str(), "" );
    return -EINVAL;
  }
  
  std::string pfn, cval;
  try {
    // Note that we force the calculation
    c->getChecksum ( Lfn, Cks.Name, cval, pfn, true );
    DEBUG( SSTR("Got checksum. lfn: '" << Lfn << "' ctype: '" << Cks.Name << "' cval: '" << cval << "'").c_str() );
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, SSTR ( "error in getchecksum(" << Lfn << "') err: " ).c_str(), e.what() );
    return -EINVAL;
  } catch ( ... ) {
    Say.Emsg ( __func__, SSTR ( "error in getchecksum(" << Lfn << "')" ).c_str() );
    return -EINVAL;
  }
  
  if ( !cval.size() ) {
    Say.Emsg ( __func__, SSTR ( "empty getchecksum(" << Lfn << "')" ).c_str() );
    return -EINVAL;
  }
  
  // Beware! XrdCksData assumes that the input is hex and with an even number of chars
  if (cval.length() & 1)
    cval.insert(0, 1, '0');
  
  Cks.Set(cval.c_str(), cval.length()*2);
  
  return Cks.Length;
}


int XrdDPMCksManager::Get ( const char *Lfn, XrdCksData &Cks ) {
  DpmIdentity empty_ident;
  XrdDmStackWrap sw(dpm_ss, empty_ident);
  EPNAME("XrdDPMCksManager::Get")
  
  dmlite::Catalog *c;
  
  try {
    c = sw->getCatalog();
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, "Trouble getting a Catalog instance. err: ", e.what() );
    return -EINVAL;
  } catch ( ... ) {
    Say.Emsg ( __func__, "Trouble getting a Catalog instance." );
    return -EINVAL;
  }
  
  
  if ( !c ) {
    XrdOucString errstr ( "Unable to acquire a dmlite::Catalog instance" );
    errstr += ( ( Lfn && *Lfn ) ? Lfn : "[none]" );
    Say.Emsg ( __func__, errstr.c_str(), "" );
    return -EINVAL;
  }
  
  std::string cnam = "checksum.";
  cnam += Cks.Name;
  std::string pfn, cval;
  try {
    c->getChecksum ( Lfn, cnam, cval, pfn );
    DEBUG( SSTR("Got checksum. lfn: '" << Lfn << "' ctype: '" << Cks.Name << "' cval: '" << cval << "'").c_str() );
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, SSTR ( "error in getchecksum(" << Lfn << "') err: " ).c_str(), e.what() );
    return -EINVAL;
  } catch ( ... ) {
    Say.Emsg ( __func__, SSTR ( "error in getchecksum(" << Lfn << "')" ).c_str() );
    return -EINVAL;
  }
  
  // Here it seems we have a checksum. If it's empty we return an error
  if ( !cval.length() ) {
    Say.Emsg ( __func__, SSTR ( "empty getchecksum(" << Lfn << "')" ).c_str() );
    return -EINVAL;
  }
  
  // Beware! XrdCksData assumes that the input is hex and with an even number of chars
  if (cval.length() & 1)
    cval.insert(0, 1, '0');
  
  Cks.Set(cval.c_str(), cval.length());
  
  return Cks.Length;
}


int XrdDPMCksManager::Config(const char *Token, char *Line) {
  return 1;
}


int XrdDPMCksManager::Init(const char *ConfigFN, const char *DfltCalc) {
  return 1;
}


int XrdDPMCksManager::Size( const char  *Name) {
  return 256;
}

int XrdDPMCksManager::Set(  const char *Lfn, XrdCksData &Cks, int myTime ) {
  
  if (!Lfn || !strlen(Lfn)) {
    Say.Emsg ( __func__, SSTR ( "empty Lfn" ).c_str() );
    return -EINVAL;
  }
  
  if ( !Cks.Length || !strlen(Cks.Value)) {
    Say.Emsg ( __func__, SSTR ( "empty chacksum value for Lfn:'" << Lfn << "')" ).c_str() );
    return -EINVAL;
  }
  
  if ( !strlen(Cks.Name) ) {
    Say.Emsg ( __func__, SSTR ( "empty checksum name for Lfn:'" << Lfn << "')" ).c_str() );
    return -EINVAL;
  }
  
  DpmIdentity empty_ident;
  XrdDmStackWrap sw(dpm_ss, empty_ident);
  
  dmlite::Catalog *c;
  
  try {
    c = sw->getCatalog();
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, "Unable to get Catalog instance. err: ", e.what() );
    return -EINVAL;
  } catch ( ... ) {
    Say.Emsg ( __func__, "Unable to get Catalog instance." );
    return -EINVAL;
  }
  
  if ( !c ) {
    XrdOucString errstr ( "Unable to acquire dmlite::Catalog instance" );
    errstr += ( ( Lfn && *Lfn ) ? Lfn : "[none]" );
    Say.Emsg ( __func__, errstr.c_str(), "" );
    return -EINVAL;
  }
  
  std::string cnam = "checksum.";
  cnam += Cks.Name;
  try {
    c->setChecksum ( Lfn, cnam, Cks.Value );
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, SSTR ( "error in setchecksum(" << Lfn << "') err: " ).c_str(), e.what() );
    return -EINVAL;
  } catch ( ... ) {
    Say.Emsg ( __func__, SSTR ( "error in setchecksum(" << Lfn << "')" ).c_str() );
    return -EINVAL;
  }
  
  
  return 0;
}

char *XrdDPMCksManager::List(const char *Lfn, char *Buff, int Blen, char Sep) {
  if (Blen < 4) {
    Say.Emsg ( __func__, "no buffer space to list checksums" );
    return NULL;
  }
  if (!Lfn) {
    XrdOucString s;
    s += "adler";
    s += Sep;
    s += "md5";
    s += Sep;
    s += "crc32";
    
    strncpy(Buff, s.c_str(), Blen-1);
    Buff[Blen] = '\0';
    return Buff;
  }
  
  // Let peek the xattrs of the file and extract the keys of those that look like checksums
  // Nice synthetic code isn't it ?
  DpmIdentity empty_ident;
  XrdDmStackWrap sw(dpm_ss, empty_ident);
  dmlite::Catalog *c;
  
  try {
    c = sw->getCatalog();
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, "Unable to get Catalog instance. err: ", e.what() );
    return NULL;
  } catch ( ... ) {
    Say.Emsg ( __func__, "Unable to get Catalog instance." );
    return NULL;
  }
  if ( !c ) {
    XrdOucString errstr ( "Unable to acquire dmlite::Catalog instance" );
    errstr += ( ( Lfn && *Lfn ) ? Lfn : "[none]" );
    Say.Emsg ( __func__, errstr.c_str(), "" );
    return NULL;
  }
  
  try {
    ExtendedStat xst = c->extendedStat(Lfn);
    std::string s;
    std::vector<std::string> keys = xst.getKeys();
    
    for (unsigned int i = 0; i < keys.size(); i++)
      if (keys[i].compare(0, 9, "checksum.") == 0) {
        s += keys[i].substr(9);
        if (i < keys.size()-1) s += Sep;
      }
      
  } catch (DmException &e) {
    Say.Emsg ( __func__, SSTR ( "error in ExtendedStat(" << Lfn << "') err: " ).c_str(), e.what() );
    return NULL;
  } catch ( ... ) {
    Say.Emsg ( __func__, SSTR ( "error in ExtendedStat(" << Lfn << "')" ).c_str() );
    return NULL;
  }
  
  
  return Buff;
}



int XrdDPMCksManager::Ver(  const char *Lfn, XrdCksData &Cks) {
  DpmIdentity empty_ident;
  XrdDmStackWrap sw(dpm_ss, empty_ident);
  
  dmlite::Catalog *c;
  
  try {
    c = sw->getCatalog();
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, "Unable to get Catalog instance. err: ", e.what() );
    return -EINVAL;
  } catch ( ... ) {
    Say.Emsg ( __func__, "Unable to get Catalog instance." );
    return -EINVAL;
  }
  if ( !c ) {
    XrdOucString errstr ( "Unable to acquire dmlite::Catalog instance" );
    errstr += ( ( Lfn && *Lfn ) ? Lfn : "[none]" );
    Say.Emsg ( __func__, errstr.c_str(), "" );
    return -EINVAL;
  }
  
  std::string cnam = "checksum.";
  cnam += Cks.Name;
  std::string pfn, cval;
  try {
    c->getChecksum ( Lfn, cnam, cval, pfn );
  } catch ( DmException &e ) {
    Say.Emsg ( __func__, SSTR ( "error in getchecksum(" << Lfn << "') err: " ).c_str(), e.what() );
    return -EINVAL;
  } catch ( ... ) {
    Say.Emsg ( __func__, SSTR ( "error in getchecksum(" << Lfn << "')" ).c_str() );
    return -EINVAL;
  }
  
  if ( !cval.size() ) {
    Say.Emsg ( __func__, SSTR ( "empty getchecksum(" << Lfn << "')" ).c_str() );
  }
  
  if (strcmp(Cks.Value, cval.c_str())) {
    // Means that the comparison said that they are dfferent
    return 0;
  }
  
  strncpy ( Cks.Value, cval.c_str(), sizeof ( Cks.Value ) );
  Cks.Value[sizeof ( Cks.Value )-1] = '\0';
    
    return cval.size();
}


const char *XrdDPMCksManager::Name(int seqNum) {
  if (seqNum < 0) return NULL;
  if (seqNum > 2) return NULL;
  return XrdDPMCksNames[seqNum];
}

/******************************************************************************/
/*                       L i b r a r y   i n i t                              */
/******************************************************************************/
extern "C" XrdCks *XrdCksInit(XrdSysError *eDest,
                              const char  *cFN,
                              const char  *Parms
                              )
{
  //EPNAME("XrdCksInit");
  Say.logger(eDest->logger());
  
  
  
  XrdSysError_Table *ETab = XrdDmliteError_Table();
  Say.addTable(ETab);
  
  XrdDmCommonInit(eDest->logger());
  
  
  Say.Say("This is XrdDPMCksManager "
  XrdDPMVERSIONSTR
  " compiled with xroot "
  XrdCompileVersion );
  

  
  if (DpmCommonConfigProc(Say, cFN, CommonConfig, &RedirConfig)) {
    Say.Emsg("Init","problem setting up the common config");
    return NULL;
  }
  Trace.What = CommonConfig.OssTraceLevel;
  
  
  try {
    dpm_ss.SetDmConfFile(CommonConfig.DmliteConfig);
    dpm_ss.SetDmStackPoolSize(CommonConfig.DmliteStackPoolSize);
    // get a dmlite stack to trigger the loading of the dmlite
    // configuration. This may do things like setting variables
    // in the enviornment, which is not thread safe and might cause
    // problems later.
    DpmIdentity empty_ident;
    XrdDmStackWrap sw(dpm_ss, empty_ident);
  } catch(const std::exception &) {
    Say.Emsg("Init","problem setting up the dmlite stack");
    return NULL;
  }
  
  
  try {
    return new XrdDPMCksManager(eDest, cFN, Parms);
  } catch(dmlite::DmException &e) {
    Say.Emsg("NewObject","cannot create a new instance of XrdDPMCksManager err: ", e.what());
  } catch(const std::exception &) {
    Say.Emsg("NewObject","cannot create a new instance of XrdDPMCksManager");
  }
  
  return 0;
}
