/*                         
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * Author: Fabrizio Furano, David Smith <David.Smith@cern.ch>
*/

#include <XrdDPMFinder.hh>
#include <XrdDPMTrace.hh>
#include <XrdOuc/XrdOucStream.hh>
#include <XrdOuc/XrdOuca2x.hh>
#include <XrdSec/XrdSecEntity.hh>
#include <XrdSfs/XrdSfsInterface.hh>
#include <XrdSys/XrdSysPlugin.hh>
#include <XrdOuc/XrdOucPinPath.hh>
#include <XrdNet/XrdNetAddr.hh>
#include <XrdNet/XrdNetUtils.hh>
#include <XrdSys/XrdSysPthread.hh>
#include <XrdVersion.hh>

#include <sys/time.h>
// // // #include <sys/select.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <vector>
#include <memory>
#include <exception>

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/catalog.h>
#include <dmlite/cpp/poolmanager.h>

// globals

XrdVERSIONINFO(XrdCmsGetClient,XrdDPMFinder)
XrdVERSIONINFO(DpmXrdCmsGetConfig,XrdDPMFinder)

namespace DpmFinder {
   XrdSysError Say(0,"dpmfinder_");
   XrdOucTrace Trace(&Say);
   XrdDmStackStore dpm_ss;
   XrdDPMFinder *theFinder = 0;
}

using namespace DpmFinder;

class XrdOss;

// static functions, objects local to this compilation unit
//

// static unsigned int getNonce() {
//    static volatile unsigned int nonce = 0;
//    struct timeval tv;
//    unsigned int res;
// #ifdef HAVE_ATOMICS
//    do {
//       res = __sync_add_and_fetch(&nonce, 1);
//       gettimeofday(&tv,0);
//       tv.tv_sec &= 0xfff;
//       if (res>>20 == (unsigned int)tv.tv_sec) break;
//       unsigned int rnew = tv.tv_sec << 20 | (tv.tv_usec & 0x7ffff);
//       if (__sync_bool_compare_and_swap(&nonce, res, rnew)) {
//          res = rnew;
//          break;
//       }
//    } while(1);
// #else
//    static XrdSysMutex Mtx;
//    Mtx.Lock();
//    res = ++nonce;
//    gettimeofday(&tv,0);
//    tv.tv_sec &= 0xfff;
//    if (res>>20 != (unsigned int)tv.tv_sec) {
//       res = nonce = tv.tv_sec << 20 | (tv.tv_usec & 0x7ffff);
//    }
//    Mtx.UnLock();
// #endif
//    return res;
// }



//
// More performant version of getNonce
// that just drops the requirement of producing
// timestamps that are always different
//
unsigned int getNonce() {
  struct timeval tv;
  unsigned int res;

  gettimeofday(&tv,0);
  tv.tv_sec &= 0xfff;
  res = tv.tv_sec << 20 | (tv.tv_usec & 0x7ffff);



  return res;
}
static XrdOucString EncodeString(const XrdOucString &arg) {
   XrdOucString in(arg);
   XrdOucString out;
   static const char escchars[] = { '%', '&', '?', ' ', '#', '\0' };

   for(int i=0;i<in.length();++i) {
      char c = in[i];
      bool esc=0;
      if (c<32) esc=1;
      for(const char *p=escchars;*p && !esc;++p) {
         if (c == *p) esc=1;
      }
      if (esc) {
         char ct[4];
         snprintf(ct,sizeof(ct),"%%%02x",c);
         out += ct;
      } else {
         out += c;
      }
   }

   return out;
}

static int getSize(const char *theSize, long long &Size)
{ 
   char *eP;
      
   // Convert the size argument
   //
   Size = strtoll(theSize, &eP, 10);
   if (*eP) return 0;
   return 1;
}

static int getMode(const char *theMode, mode_t &Mode)
{
   char *eP;

   // Convert the mode argument
   //
   Mode = strtol(theMode, &eP, 8);
   if (*eP || (Mode >> 9)) return 0;
   return 1;
}

static XrdOucString mk_opaque(
      const char *xrd_fn,
      const DpmFileRequest &req,
      const DpmIdentity &ident,
      const std::vector<unsigned char> &key,
      int grace,
      bool list_voms,
      bool no_hv1) {
   XrdOucString s,nonce,sfn,pfn,rtoken;
   unsigned int inonce;
   time_t t0 = time(0);
   char tbuf[64];
   struct tm tms;
   bool hv1_compat=0;

   s = "&dpm.time=";
   localtime_r(&t0, &tms);
        strftime(tbuf, sizeof(tbuf), "%s", &tms);
   s += tbuf;
   if (grace) {
      snprintf(tbuf,sizeof(tbuf),",%d",grace);
      s += tbuf;
   }

   if (!ident.IsSecEntID()) {
      s += "&dpm.dn=" + EncodeString(ident.Dn());
   }

   if (ident.Groups().length()) {
      if (!ident.IsSecEntID() || list_voms) {
         s += "&dpm.voms=" + EncodeString(ident.Groups());
      }
   } else if (ident.IsSecEntID() && list_voms) {
      s += "&dpm.voms=.";
   }

   s += "&dpm.dhost=" + req.Host();

   const dmlite::Location &loc = req.Location();
   XrdOucString locstr;
   std::vector<XrdOucString> chunkstr;
   LocationToOpaque(loc, locstr, chunkstr);

   XrdOucString hv1_rtoken, hv1_pfn, hv1_sfn;
   if (!no_hv1 && !loc[0].url.path.empty()) {
      hv1_compat = 1;
      hv1_rtoken = loc[0].url.query.getString("dpmtoken").c_str();
      hv1_pfn = loc[0].url.path.c_str();
      hv1_sfn = loc[0].url.query.getString("sfn").c_str();
      if (hv1_sfn == xrd_fn) {
         hv1_sfn.erase();
      }
      if (hv1_sfn.length()) {
         s += "&dpm.surl=" + EncodeString(hv1_sfn);
      }
      s += "&dpm.sfn=" + EncodeString(hv1_pfn);
      if (hv1_rtoken.length()) {
         s += "&dpm.tk=" + hv1_rtoken;
      }
   }

   if (req.ROpts().isPut)
      s+= "&dpm.put=1";

   s += XrdOucString("&dpm.loc=") + EncodeString(locstr);
   size_t n = chunkstr.size();
   for(size_t idx=0;idx<n;++idx) {
      s += XrdOucString("&dpm.chunk") + (int)idx + "=" +
         EncodeString(chunkstr[idx]);
   }

   inonce = getNonce();
   snprintf(tbuf,sizeof(tbuf),"%x",inonce);
   nonce = tbuf;
   s += "&dpm.nonce=" + nonce;

   char *hashes[2];
   calc2Hashes(
            hashes,
            hv1_compat ? 3 : 2,
            xrd_fn,
            SafeCStr(hv1_sfn),
            SafeCStr(req.Host()),
            SafeCStr(hv1_pfn),
            SafeCStr(hv1_rtoken),
            (req.ROpts().isPut ? XRDDPM_IS_PUT : 0) |
               (ident.IsSecEntID() ? 0 : XRDDPM_ID_URL),
            SafeCStr(ident.Dn()),
            SafeCStr(ident.Groups()),
            t0,
            grace,
            SafeCStr(nonce),
            locstr,
            chunkstr,
            &key[0],
            key.size() );

   if ((hv1_compat && !hashes[0]) || !hashes[1]) {
      free(hashes[0]);
      free(hashes[1]);
      throw dmlite::DmException(DMLITE_SYSERR(DMLITE_UNKNOWN_ERROR),
               "Could not calculate hashes");
   }

   if (hv1_compat) {
      s += "&dpm.hv1=";
      s += hashes[0];
   }

   s += "&dpm.hv2=";
   s += hashes[1];

   free(hashes[0]);
   free(hashes[1]);

   return s;
}

static int mkp(dmlite::StackInstance &si, const char *path, mode_t mode)
{
   EPNAME("mkp");
   std::string next_path, this_path(path);
   size_t pos;
   dmlite::ExtendedStat xstat;
   int retc;

   DEBUG("Makepath " << path << " mode=" << std::oct << mode << std::dec);
          
   // Trim off the trailing slash so that we make everything but the last
   // component
   //
   if (this_path.empty())
      throw dmlite::DmException(DMLITE_SYSERR(ENOENT),
               "Empty path given to makepath");
   do {
      std::string::iterator it = this_path.end() - 1;
      if (it == this_path.begin() || *it != '/')
         break;
      this_path.erase(it);
   } while(1);
     
   // Typically, the path exists. So, do a quick check before launching
   // into it
   //
   pos = this_path.find_last_of('/');
   if (pos == std::string::npos || pos == 0)
      return 0;
   next_path.assign(this_path,0,pos);
   retc = 0;
   try {
      xstat = si.getCatalog()->extendedStat(next_path);
   } catch(dmlite::DmException &e) {
      retc = DmExErrno(e);
   }
   if (!retc)
      return 0;
   
   // Start creating directories starting with the root
   //
   pos = 0;
   while((pos = this_path.find_first_of('/',pos+1)) != std::string::npos)
   {
      if (pos+1 < this_path.length() && this_path[pos+1] == '/') continue;
      next_path.assign(this_path,0,pos);

      dmlite::DmStatus st = si.getCatalog()->extendedStat(xstat, next_path);
      if ( !st.ok() ) {
       if (st.code() == ENOENT) {
         try {
           si.getCatalog()->makeDir(next_path, mode);
         } catch(dmlite::DmException &e) {
           if (DmExErrno(e) != EEXIST) {
             throw;
           }
         }
       }
       else throw st.exception();
      }
         

   }
   // All done
   //
   return -1;
}

/******************************************************************************/
/*                       D p m F i l e R e q u e s t                          */
/******************************************************************************/

void DpmFileRequest::DoQuery()  {
   EPNAME("DoQuery");
   bool isCreate = flags & (SFS_O_CREAT | SFS_O_TRUNC);
   withOverwrite = (flags & SFS_O_TRUNC);

   if (MkpathState == 1) {
      mkp(si, SafeCStr(spath), 0775);
      MkpathState = 2;
   }
   if (ReqOpts.isPut) {
      if (!isCreate) {
         dmlite::ExtendedStat xstat;
         dmlite::DmStatus st = 
            si.getCatalog()->extendedStat(xstat, SafeCStr(spath));
            
         if ( !st.ok() ) {
           if (DmExInt2Errno(st.code()) == ENOENT) {
             if (ReqOpts.isMkpath && isCreate && MkpathState==0) {
               init();
               MkpathState = 1;
               DoQuery();
               return;
             }
           }
           else throw st.exception();
         }
         
         if (S_ISREG(xstat.stat.st_mode) && xstat.stat.st_size == 0) {
            DEBUG("Open for update and size is zero so "
                  "handling as overwrite");
            flags |= SFS_O_TRUNC;
            DoQuery();
            return;
         }
         throw dmlite::DmException(ENOTSUP, "Open for update not supported");
      }

      try {
         dmput();

      } catch (dmlite::DmException &e) {
         if (DmExErrno(e) == ENOENT) {
            if (ReqOpts.isMkpath && isCreate && MkpathState==0) {
               init();
               MkpathState = 1;
               DoQuery();
               return;
            }
         }
         throw;
      }
   } else {
      dmget();
   }
   std::vector<dmlite::Chunk>::iterator ci;
   for(ci=r_Location.begin();ci!=r_Location.end();++ci) {
      ci->url.query.erase("token");
   }
}

DpmFileRequest::DpmFileRequest(dmlite::StackInstance &si,
   const XrdOucString &path, int flags, DpmFileRequestOptions &ReqOpts) :
   si(si), spath(path), flags(flags), ReqOpts(ReqOpts) { init(); }

void DpmFileRequest::init() {
   MkpathState = 0;
   withOverwrite = false;
   r_Location.clear();
   r_host.erase();
   XrdDmStackStore::resetStackDpmParams(si);
}

void DpmFileRequest::dmput()  {
   EPNAME("dmput");
   int overwrite = withOverwrite ? 1 : 0;
   std::string s;

   if (ReqOpts.stoken.length()) {
      s = SafeCStr(ReqOpts.stoken);
      si.set("SpaceToken", s);
   } else if (ReqOpts.utoken.length()) {
      s = SafeCStr(ReqOpts.utoken);
      si.set("UserSpaceTokenDescription", s);
   }
   si.set("lifetime", ReqOpts.lifetime);
   si.set("f_type", ReqOpts.ftype);
   si.set("requested_size", (ssize_t)ReqOpts.reqsize);

   if (overwrite) { si.set("overwrite", 1); }

   XrdOucString dbgstr = "calling whereToWrite sfn='";
   dbgstr += spath + "', lifetime=" + (int)ReqOpts.lifetime + ", f_type='";
   if (ReqOpts.ftype) {
      dbgstr += ReqOpts.ftype;
   }
   dbgstr +=  "', requested_size=";
   {
      char buf[21];
      snprintf(buf, sizeof(buf), "%llu",(unsigned long long)ReqOpts.reqsize);
      dbgstr += buf;
   }
   dbgstr += ", ";
   if (ReqOpts.stoken.length()) {
      dbgstr += "s_token='" + ReqOpts.stoken + "', ";
   } else if (ReqOpts.utoken.length()) {
      dbgstr += "u_token='" + ReqOpts.utoken + "', ";
   }
   dbgstr += "overwrite=";
   dbgstr += overwrite;
   DEBUG(dbgstr);

   r_Location = si.getPoolManager()->whereToWrite(SafeCStr(spath));

   if (r_Location.size() == 0) {
      throw dmlite::DmException(DMLITE_SYSERR(ENOENT),
         "No chunks found for file");
   }

   r_host = r_Location[0].url.domain.c_str();
   if (!r_host.length()) {
      throw dmlite::DmException(DMLITE_SYSERR(EINVAL),
         "Could not find destination for redirect");
   }
}

void DpmFileRequest::dmget()  {
   EPNAME("dmget");
   std::string s;

   if (ReqOpts.stoken.length()) {
      s = SafeCStr(ReqOpts.stoken);
      si.set("SpaceToken", s);
   } else if (ReqOpts.utoken.length()) {
      s = SafeCStr(ReqOpts.utoken);
      si.set("UserSpaceTokenDescription", s);
   }
   si.set("lifetime", ReqOpts.lifetime);
   si.set("f_type", ReqOpts.ftype);

   XrdOucString dbgstr = "calling whereToRead sfn='";
   dbgstr += spath + "', lifetime=" + (int)ReqOpts.lifetime + ", f_type='";
   if (ReqOpts.ftype) {
      dbgstr += ReqOpts.ftype;
   }
   dbgstr +=  "'";
   if (ReqOpts.stoken.length()) {
      dbgstr += ", s_token='" + ReqOpts.stoken + "'";
   } else if (ReqOpts.utoken.length()) {
      dbgstr += ", u_token='" + ReqOpts.utoken + "'";
   }
   DEBUG(dbgstr);

   r_Location = si.getPoolManager()->whereToRead(SafeCStr(spath));

   if (r_Location.size() == 0) {
      throw dmlite::DmException(DMLITE_SYSERR(ENOENT),
         "No chunks found for file");
   }

   r_host = r_Location[0].url.domain.c_str();
   if (!r_host.length()) {
      throw dmlite::DmException(DMLITE_SYSERR(EINVAL),
         "Could not find destination for redirect");
   }
}


/******************************************************************************/
/*              D p m F i l e R e q u e s t O p t i o n s                     */
/******************************************************************************/

DpmFileRequestOptions::DpmFileRequestOptions(bool isPut, XrdOucEnv *env,
   const DpmFinderConfigOptions &defaults) : isPut(isPut)
{
   //EPNAME("DpmFileRequestOptions");
   const char *p;

   if (isPut) {
      lifetime = defaults.reqput_lifetime;
      ftype = defaults.reqput_ftype;
      stoken = defaults.reqput_stoken;
      reqsize = defaults.reqput_reqsize;
      if (env && (p = env->Get("dpm.reqsize")) != 0) {
         long long Size;
         if (!XrdOuca2x::a2sz(Say,"invalid reqsize",p,&Size,0)) {
            reqsize = Size;
         } else {
            throw dmlite::DmException(EINVAL,
               "Could not read reqsize in request");
         }
      } else if (env && (p = env->Get("oss.asize")) != 0) {
         long long cgSize;
         if (!XrdOuca2x::a2sz(Say,"invalid asize",p,&cgSize,0)) {
            reqsize = cgSize;
         }
      }
   } else {
      lifetime = defaults.reqget_lifetime;
      ftype = defaults.reqget_ftype;
      stoken = defaults.reqget_stoken;
      reqsize = 0;
   }

   if (env && (p = env->Get("dpm.ftype")) != 0)
      ftype = (*p == '-') ? '\0' : *p;

   if (env && (p = env->Get("oss.cgroup")) != 0) {
      XrdOucString ut(p);
      if (ut.length()>2 && ut[0] == '[' && ut[ut.length()-1] == ']') {
         stoken.assign(ut,1,ut.length()-2);
      } else {
         if (ut != "public") { utoken = ut; }
      }
   }

   if (env && (p = env->Get("dpm.stoken")) != 0) {
      stoken = p;
   }

   if (env && (p = env->Get("dpm.utoken")) != 0) {
      utoken = DecodeString(p);
   }

   if (env && (p = env->Get("dpm.lifetime")) != 0) {
      char *eP;
      lifetime = strtol(p, &eP, 10);
      if (!*p || *eP) {
         throw dmlite::DmException(EINVAL,
            "Could not read lifetime in request");
      }
   }

   isMkpath = defaults.mkpath_bool;
}

/******************************************************************************/
/*                          X r d D P M F i n d e r                           */
/******************************************************************************/

XrdDPMFinder::XrdDPMFinder(XrdCmsClient *cmsClient, XrdSysLogger *lp,
   int whoami, int Port) : XrdCmsClient(XrdCmsClient::amRemote),
   Authorization(0), AuthSecondary(0), defaultCmsClient(cmsClient)
{
   Say.logger(lp);
   XrdSysError_Table *ETab = XrdDmliteError_Table();
   Say.addTable(ETab);
   XrdDmCommonInit(lp);
}

void XrdDPMFinder::DpmAdded(const char *path, int Pend) {
   if (defaultCmsClient)
      defaultCmsClient->Added(path, Pend);
}

void XrdDPMFinder::DpmRemoved(const char *path) {
   if (defaultCmsClient)
      defaultCmsClient->Removed(path);
}

int XrdDPMFinder::Forward(XrdOucErrInfo &Resp, const char *cmd,
                       const char *arg1,  const char *arg2,
                       XrdOucEnv  *Env1,  XrdOucEnv  *Env2)
{
   EPNAME("Forward");
   std::unique_ptr<DpmIdentity> identP;
   XrdDmStackWrap sw;
   try {
      identP.reset(new DpmIdentity(Env1, Opts.RedirConfig.IdentConfig));
      sw.reset(dpm_ss, *identP);
   } catch(dmlite::DmException &e) {
      XrdOucString err(DmExStrerror(e));
      Resp.setErrInfo(DmExErrno(e), SafeCStr(err));
      Say.Emsg("Forward", e.what());
      return SFS_ERROR;
   } catch( ... ) {
      Resp.setErrInfo(EINVAL, "Unexpected server error condition");
      Say.Emsg("Forward", "Unexpected exception");
      return SFS_ERROR;
   }

   try {
      bool isTwoWay;
      if ((isTwoWay = (*cmd == '+'))) cmd++;

      std::string fullpath1 = SafeCStr(TranslatePath(Opts.RedirConfig,arg1,sw));

      DEBUG("Processing forward command=" << cmd <<
         " for " << fullpath1 << " isTwoWay=" << isTwoWay);

      if (!strcmp(cmd, "chmod")) {
         mode_t mode;
         TRACE(chmod,"chmod " << arg1 << " to mode " << arg2);
         if (!getMode(arg2, mode)) {
            Resp.setErrInfo(EINVAL, "invalid mode");
            return SFS_ERROR;
         }
         sw->getCatalog()->setMode(fullpath1, mode);
         return 0;
      }
      if (!strcmp(cmd, "mkpath") || !strcmp(cmd,"mkdir")) {
         mode_t mode;
         TRACE(mkdir,cmd << " " << arg1 << " mode " << arg2);
         if (!getMode(arg2, mode)) {
            Resp.setErrInfo(EINVAL, "invalid mode");
            return SFS_ERROR;
         }
         if (!strcmp(cmd,"mkpath")) {
            mkp(*sw, fullpath1.c_str(), mode);
         }
         sw->getCatalog()->makeDir(fullpath1, mode);
         DpmAdded(arg1);
         return 0;
      }
      if (!strcmp(cmd, "rm")) {
         TRACE(remove,"rm " << arg1);
         sw->getCatalog()->unlink(fullpath1);
         DpmRemoved(arg1);
         return 0;
      }
      if (!strcmp(cmd, "rmdir")) {
         TRACE(remove,"rmdir " << arg1);
         sw->getCatalog()->removeDir(fullpath1);
         DpmRemoved(arg1);
         return 0;
      }
      if (!strcmp(cmd, "mv")) {
         std::string fullpath2;
         TRACE(rename,"rename " << arg1 << " to " << arg2);
         fullpath2 = SafeCStr(TranslatePath(Opts.RedirConfig, arg2, sw));
         sw->getCatalog()->rename(fullpath1, fullpath2);
         DpmRemoved(arg1);
         DpmAdded(arg2);
         return 0;
      }
      if (!strcmp(cmd, "trunc")) {
         long long Size;
         TRACE(truncate,"trunc " << arg1 << " " << arg2);
         if (!getSize(arg2, Size)) {
            Resp.setErrInfo(EINVAL, "invalid size");
            return SFS_ERROR;
         }
         dmlite::ExtendedStat xstat;
         dmlite::DmStatus st =
            sw->getCatalog()->extendedStat(xstat, fullpath1);
         
         if ( !st.ok() ) {
           XrdOucString err(st.what());
           DEBUG(st.what());
           Resp.setErrInfo(DmExInt2Errno(st.code()), SafeCStr(err));
           return SFS_ERROR;
         }
         
         if (!S_ISREG(xstat.stat.st_mode)) {
            throw dmlite::DmException(EINVAL,"Not a regular file");
         }
         if (xstat.stat.st_size != (off_t)Size) {
            if (Size == 0) {
               TRACE(truncate,"overwriting file");
               XrdOucEnv tmpEnv;
               tmpEnv.Put("dpm.reqsize","1");
               DpmFileRequestOptions ReqOpts(1,
                  &tmpEnv, Opts);
               DpmFileRequest req(*sw,fullpath1.c_str(), SFS_O_TRUNC,ReqOpts);
               req.DoQuery();
               return 0;
            }
            TRACE(truncate,"can not truncate to " << Size);
            throw dmlite::DmException(DMLITE_SYSERR(ENOTSUP),
               "trunc to non zero not supported");
         }
         TRACE(truncate,"already of requested length");
         return 0;
      }
   } catch(dmlite::DmException &e) {
      XrdOucString err(DmExStrerror(e));
      DEBUG(e.what());
      Resp.setErrInfo(DmExErrno(e), SafeCStr(err));
      return SFS_ERROR;
   } catch( ... ) {
      Resp.setErrInfo(EINVAL, "Unexpected server error condition");
      Say.Emsg("Forward","Unexpected exception");
      return SFS_ERROR;
   }

   DEBUG("Command not supported");
   Resp.setErrInfo(ENOTSUP, "Command not supported");
   return SFS_ERROR;
}

int XrdDPMFinder::Locate(XrdOucErrInfo &Resp, const char *path, int flags,
                      XrdOucEnv *Info)
{
   EPNAME("Locate");

   DEBUG("Entering Locate, name="
      << Resp.getErrUser() << " flags=" << std::hex << flags << std::dec);

   if (flags & SFS_O_META) {
      // This should have been sent to Forward()
      //
      XrdOucString err =
         "Metadata request sent to the "
         "wrong function. Check ofs.forward directive "
         "enables forwarding for all requests.";
      Say.Emsg("Locate", SafeCStr(err));
      Resp.setErrInfo(EINVAL, "Unexpected request");
      return SFS_ERROR;
   }

   // get the tried list
   const char *p;
   XrdOucString tried;
   if (Info && (p=Info->Get("tried")) && *p) {
      tried = p;
      if (tried.endswith('?'))
         tried.erase(tried.length()-1,1);
      DEBUG(path << " avoiding " << tried);
   }

   bool isCreate = flags & (SFS_O_CREAT | SFS_O_TRUNC);
   bool isPut = isCreate || (flags & (SFS_O_WRONLY | SFS_O_RDWR));
   bool isForOSS = flags & (SFS_O_STAT | SFS_O_LOCATE);

   // define some information about the action we're doing
   Access_Operation ao = AOP_Read;
   const char *action = "open";
   if (isForOSS) {
      ao = AOP_Stat;
      action = "locate";
   } else if (isCreate) {
      ao = AOP_Create;
      action = "create";
   } else if (isPut) {
      ao = AOP_Update;
   }

   // if our cluster name is in the tried list, then we directly give ENOENT
   p = getenv("XRDCMSCLUSTERID");
   if (p) {
      int from = 0;
      XrdOucString item;
      while ((from = tried.tokenize(item, from, ',')) != STR_NPOS) {
         const char *p2 = SafeCStr(item);
         if (*p2 != '+') { continue; }
         ++p2;
         if (!strcmp(p,p2)) {
            DEBUG("cgi shows client has already tried with this cluster");
            XrdOucString errstr("Unable to ");
            errstr += XrdOucString(action) + " " + path + "; ";
            errstr += XrdSysError::ec2text(ENOENT);
            Resp.setErrInfo(ENOENT, SafeCStr(errstr));
            return SFS_ERROR;
         }
      }
   }

   // is it a stat to discover a file,
   // triggered by a proxy xrootd redirector-cmsd at our site?
   // in either case get and set relevant identity
   //
   bool isXPList = 0;
   const char *apath = path;
   std::unique_ptr<DpmIdentity> identP;

   if ((flags & SFS_O_LOCATE) && *path == '*') {
      apath++;
      if (!*apath) { isXPList=1; }
   }

   if (isForOSS && 
      IsMetaManagerDiscover(Info, flags, path, Resp.getErrUser()))
      {
      try {
         identP.reset(new DpmIdentity());
      } catch(dmlite::DmException &e) {
         XrdOucString err(DmExStrerror(e));
         Resp.setErrInfo(DmExErrno(e), SafeCStr(err));
         Say.Emsg("Locate", e.what());
         return SFS_ERROR;
      } catch( ... ) {
         Resp.setErrInfo(EINVAL, "Unexpected server error condition");
         Say.Emsg("Locate", "Unexpected exception");
         return SFS_ERROR;
      }
      TRACE(exists,"Discovery request for " << path);
   } else {
      try {
         identP.reset(new DpmIdentity(Info,Opts.RedirConfig.IdentConfig));
      } catch(dmlite::DmException &e) {
         XrdOucString err(DmExStrerror(e));
         Resp.setErrInfo(DmExErrno(e), SafeCStr(err));
         Say.Emsg("Locate", e.what());
         return SFS_ERROR;
      } catch( ... ) {
         Resp.setErrInfo(EINVAL, "Unexpected server error condition");
         Say.Emsg("Locate", "Unexpected exception");
         return SFS_ERROR;
      }
      // do an authorizaiton check directly to the secondary;
      // to cover cases where the Ofs has not yet already
      // called Authorize or did not also provie the opaque
      // information, or for AOP_Stat the secondary was bypassed.
      if (DpmIdentity::usesPresetID(Info)) {
         const XrdSecEntity *secEntity = Info->secEnv();
         if (!AuthSecondary || (!isXPList &&
               !Authorization->Access(secEntity, apath,
               ao, (flags & SFS_O_LOCATE)? 0: Info))) {
            if (!AuthSecondary) {
               DEBUG("Use of fixed id needs a secondary authorization library "
               "to be configured. Denying");
            }
            XrdOucString errstr("Unable to ");
            errstr += XrdOucString(action) + " " + (*apath ? apath : "[none]");
            errstr += XrdOucString("; ") + XrdSysError::ec2text(EACCES);
            Say.Emsg("Locate", Resp.getErrUser(), SafeCStr(errstr));
            Resp.setErrInfo(EACCES, SafeCStr(errstr));
            return SFS_ERROR;
         }
      }
      if (flags & SFS_O_STAT) {
         TRACE(exists, "Stat request for " << path);
      } else if (flags & SFS_O_LOCATE) {
         TRACE(fsctl, "Locate request for " << path);
      }
   }

   XrdOucString FullPath;
   XrdDmStackWrap sw;
   try {
      sw.reset(dpm_ss, *identP);
      if (!isXPList && !(flags & SFS_O_LOCATE)) {
         FullPath = TranslatePath(Opts.RedirConfig, apath, sw);
      }
   } catch(dmlite::DmException &e) {
      XrdOucString err(DmExStrerror(e,action,path));
      Resp.setErrInfo(DmExErrno(e), SafeCStr(err));
      Say.Emsg("Locate", e.what());
      return SFS_ERROR;
   } catch( ... ) {
      Resp.setErrInfo(EINVAL, "Unexpected server error condition");
      Say.Emsg("Locate", "Unexpected exception");
      return SFS_ERROR;
   }

   if (isForOSS) {
      // Our Oss has to handle this. The authorization check
      // has already been done either by the Ofs or above.
      if (!Info) {
         Resp.setErrInfo(EINVAL, "No environment");
         return SFS_ERROR;
      }
      Info->Put("dpm.dn",SafeCStr(EncodeString(identP->Dn())));
      Info->Put("dpm.voms",SafeCStr(EncodeString(identP->Groups())));
      Info->Put("dpm.surl",SafeCStr(EncodeString(FullPath)));
      Info->Put("dpm.loc","");
      
      // Try to skip an unnecessary lookup to the dpns
      // in the Locate case
      Info->Put("dpm.skiplocate", (flags & SFS_O_LOCATE) ? "1" : "0");
      
      DEBUG("Sending to Oss");
      return 0;
   }

   // needs file access, try to get it and redirect as needed
   //
   int ret;
   try {
      ret = DoFileAccessRequest(Resp, path, flags, Info,
         *sw, FullPath, isPut, *identP, tried);
   } catch(dmlite::DmException &e) {
      XrdOucString s(DmExStrerror(e,action,path));
      DEBUG("Error during DoFileAccessRequest: " << e.what());
      Resp.setErrInfo(DmExErrno(e), SafeCStr(s));
      return SFS_ERROR;
   } catch( ... ) {
      Resp.setErrInfo(EINVAL, "Unexpected server error condition");
      Say.Emsg("Locate","Unexpected exception");
      return SFS_ERROR;
   }

   if (isPut && ret == SFS_REDIRECT)
      DpmAdded(path);

   return ret;
}

int XrdDPMFinder::DoFileAccessRequest(XrdOucErrInfo &Resp, const char *xrd_fn,
   int flags, XrdOucEnv *Info, dmlite::StackInstance &si,
   const XrdOucString &FullPath, bool isPut, const DpmIdentity &ident,
   XrdOucString &tried) 
{
   EPNAME("DoFileAccessRequest");

   DpmFileRequestOptions ReqOpts(isPut, Info, Opts);
   DpmFileRequest req(si, FullPath, flags, ReqOpts);

   try {
    // query dpm
    req.DoQuery();
   } catch (dmlite::DmException &e) {
     
     // If the DPM says that the request is in progress, stall the client 5 secs
     // and then it will retry autonomously
     if (DmExErrno(e) == EINPROGRESS)
       return 5;
     else throw;
   }

   // see if the server hostname is in the tried list
   bool namestocheck = 0;
   bool found = 0;
   int from = 0;
   const char *p;
   XrdOucString item;
   while ((from = tried.tokenize(item, from, ',')) != STR_NPOS) {
      p = SafeCStr(item);
      if (*p == '+') { continue; }
      if (!strcmp(p, SafeCStr(req.Host()))) { found = 1; break; }
      namestocheck = 1;
   }

   // see if any numeric IPs that match our disk server are in the tried list
   if (!found && namestocheck) {
      std::map<std::string, int> hmap;
      p = SafeCStr(req.Host());
      XrdNetAddr *nap = 0;
      int num = 0;
      XrdNetUtils::GetAddrs(p, &nap, num, XrdNetUtils::allIPMap,
         XrdNetUtils::NoPortRaw);
      for(int i=0;i<num;++i) {
         char ipbuff[512];
         nap[i].Format(ipbuff, sizeof(ipbuff), XrdNetAddr::fmtAddr,
            XrdNetAddr::noPort);
         hmap[ipbuff] = 1;
      }
      delete[] nap;
      nap = 0; num = 0;

      from = 0;
      while ((from = tried.tokenize(item, from, ',')) != STR_NPOS) {
         p = SafeCStr(item);
         if (*p == '+') { continue; }
         XrdNetUtils::GetAddrs(p, &nap, num,
            XrdNetUtils::allIPMap, XrdNetUtils::NoPortRaw);
         for(int i=0;i<num;++i) {
            char ipbuff[512];
            nap[i].Format(ipbuff, sizeof(ipbuff),
               XrdNetAddr::fmtAddr, XrdNetAddr::noPort);
            if (hmap.count(ipbuff)) {
               found = 1;
               break;
            }
         }
         delete[] nap;
         nap = 0; num = 0;
         if (found) { break; }
      }
   }

   if (found) {
      throw dmlite::DmException(DMLITE_SYSERR(ENOENT),
         "Tried server previously");
   }

   XrdOucString targ = req.Host() + "?" +
         mk_opaque(
         xrd_fn,
         req,
         ident,
         Opts.key,
         Opts.gracetime,
         Opts.list_voms,
         Opts.no_hv1);

   Resp.setErrInfo(Opts.xrd_server_port, SafeCStr(targ));
   TRACE(redirect,"Redirecting to " << targ);
   return SFS_REDIRECT;
}

int XrdDPMFinder::Space(XrdOucErrInfo &Resp, const char *path, XrdOucEnv *Info)
{
   EPNAME("Space");
   std::unique_ptr<DpmIdentity> identP;

   if (!Info) {
      Resp.setErrInfo(EINVAL, "No environment");
      return SFS_ERROR;
   }

   // do an authorizaiton check directly to the secondary;
   // to cover cases where the Ofs has not yet already
   // called Authorize or did not also provie the opaque
   // information.
   if (DpmIdentity::usesPresetID(Info)) {
      const XrdSecEntity *secEntity = Info->secEnv();
      if (!AuthSecondary ||
            !Authorization->Access(secEntity, path,
            AOP_Stat, 0)) {
         if (!AuthSecondary) {
            DEBUG("Use of fixed id needs a secondary "
            "authorization library to be configured. "
            "Denying");
         }
         XrdOucString errstr("Unable to statfs ");
         errstr += XrdOucString(path) + "; ";
         errstr += XrdSysError::ec2text(EACCES);
         Say.Emsg("Space", Resp.getErrUser(), SafeCStr(errstr));
         Resp.setErrInfo(EACCES, SafeCStr(errstr));
         return SFS_ERROR;
      }
   }

   XrdOucString FullPath;
   try {
      identP.reset(new DpmIdentity(Info, Opts.RedirConfig.IdentConfig));
      XrdDmStackWrap sw(dpm_ss, *identP);
      FullPath = TranslatePath(Opts.RedirConfig, path, sw);
   } catch(dmlite::DmException &e) {
      XrdOucString err(DmExStrerror(e));
      Resp.setErrInfo(DmExErrno(e), SafeCStr(err));
      Say.Emsg("Space", e.what());
      return SFS_ERROR;
   } catch( ... ) {
      Resp.setErrInfo(EINVAL, "Unexpected server error condition");
      Say.Emsg("Space", "Unexpected exception");
      return SFS_ERROR;
   }

   Info->Put("dpm.dn",SafeCStr(EncodeString(identP->Dn())));
   Info->Put("dpm.voms",SafeCStr(EncodeString(identP->Groups())));
   Info->Put("dpm.surl",SafeCStr(EncodeString(FullPath)));
   Info->Put("dpm.loc","");
   DEBUG("Sending to Oss, dpm.surl=" << FullPath);
   return 0;
}

// based on XrdOfs::setupAuth
//
int XrdDPMFinder::setupAuth(XrdSysLogger *lp, const char *cfn)
{
   XrdSysPlugin    *myLib;
   typedef   XrdAccAuthorize *(*dpmept)(XrdSysLogger *, const char *,
      const char *, int, DpmRedirConfigOptions *);

   if (!Opts.authlib.length())
      return 1;

   char libBuf[2048];
   bool noFallBack;
   char *theLib,*altLib;
   if (!XrdOucPinPath(SafeCStr(Opts.authlib), noFallBack, libBuf,
         sizeof(libBuf))) {
      theLib = strdup(SafeCStr(Opts.authlib));
      altLib = 0;
   } else {
      theLib = strdup(libBuf);
      altLib = (noFallBack ? 0 : strdup(SafeCStr(Opts.authlib)));
   }
   dpmept dpmep = 0;
   myLib = new XrdSysPlugin(&Say, theLib);
   if (myLib) {
      dpmep = reinterpret_cast<dpmept>(reinterpret_cast<size_t>
         (myLib->getPlugin("DpmXrdAccAuthorizeObject")));
   }
   if (!dpmep && altLib) {
      delete myLib;
      myLib = new XrdSysPlugin(&Say, altLib);
      if (myLib) {
         dpmep = reinterpret_cast<dpmept>(reinterpret_cast<size_t>
            (myLib->getPlugin("DpmXrdAccAuthorizeObject")));
      }
   }
   free(theLib);
   free(altLib);

   if (!dpmep) return 1;
   if ((Authorization = dpmep(lp, cfn, SafeCStr(Opts.authparm), 1,
         getRedirConfigObj()))) {
      AuthSecondary = 1;
      return 0;
   }

   AuthSecondary = 0;
   Authorization = dpmep(lp, cfn, SafeCStr(Opts.authparm), 0,
      getRedirConfigObj());

   return (Authorization == 0);
}

bool XrdDPMFinder::IsMetaManagerDiscover(XrdOucEnv *Info, int flags,
                        const char *path, const char *user)
{
   EPNAME("IsMetaManagerDiscover");

   if (!(flags & SFS_O_STAT))
      return 0;
   if (!Info)
      return 0;
   if (!path || !*path)
      return 0;
   if (!user || !*user)
      return 0;
   const XrdSecEntity *secEntity = Info->secEnv();
   if (!secEntity)
      return 0;
   XrdNetAddrInfo *ni = secEntity->addrInfo;
   if (!ni)
      return 0;

   bool hfound=0;
   std::vector<XrdNetAddr>::const_iterator itr;

   for(itr=Opts.mmReqHosts.begin(); itr!=Opts.mmReqHosts.end();++itr) {
      if (ni->Same(&(*itr))) {
         hfound=1;
         break;
      }
   }

   if (!hfound) {
      char ipbuff[512];
      ni->Format(ipbuff, sizeof(ipbuff), XrdNetAddr::fmtAddr,
         XrdNetAddr::noPort);
      XrdOucString err =
         "Possible meta-manager discovery request from host "
         + XrdOucString(ipbuff) + " (not listed in dpm.mmreqhost)";
      TRACE(exists,SafeCStr(err));
      return 0;
   }

   return 1;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/

extern "C"
{
XrdCmsClient *XrdCmsGetClient(XrdSysLogger *Logger, // Where messages go
                              int           opMode, // Operational mode
                              int           myPort, // Server's port number
                              XrdOss      *theSS)  // Storage System I/F
{
   if (!theFinder) {
      XrdCmsClient *cmsFinder = XrdCms::GetDefaultClient(Logger,
         XrdCms::IsTarget,myPort);
      theFinder = new XrdDPMFinder(cmsFinder,Logger,opMode,myPort);
   }

   return (XrdCmsClient *)theFinder;
}

DpmRedirConfigOptions *DpmXrdCmsGetConfig()
{
   if (!theFinder)
      return 0;

   return theFinder->getRedirConfigObj();
}
} // extern "C"
