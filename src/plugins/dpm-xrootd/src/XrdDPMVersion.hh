/*
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * Author: David Smith <David.Smith@cern.ch>
 */
#ifndef __XRDDPM_VERSION_H__
#define __XRDDPM_VERSION_H__

#include "XrdCompileVersion.hh"

#if defined(__GNUC__) && defined (__GNUC_PATCHLEVEL__)
#define XRDDPM_GCC_VERSION (__GNUC__ * 10000 \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)
#endif

#endif
