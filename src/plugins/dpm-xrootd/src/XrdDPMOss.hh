/* 
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * File:   XrdDPMOss.hh
 * Author: Fabrizio Furano, David Smith
 * 
 * An Oss plugin that interacts with a DPM
 *
 * Created on December 21, 2011, 10:25 AM
 */

#ifndef XRDDPMOSS_HH
#define XRDDPMOSS_HH

#include <memory>

#include <XrdDPMCommon.hh>
#include <XrdOss/XrdOss.hh>
#include <XrdOuc/XrdOucStream.hh>
#include <XrdSys/XrdSysPthread.hh>

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/catalog.h>
#include <dmlite/cpp/io.h>

#define XRDDPMOSS_EBASE 8001
  
#define XRDDPMOSS_E8001 8001
#define XRDDPMOSS_E8002 8002
#define XRDDPMOSS_E8003 8003
#define XRDDPMOSS_E8004 8004

#define XRDDPMOSS_ELAST 8004
  
#define XRDDPMOSS_T8001 "directory object in use (internal error)"
#define XRDDPMOSS_T8002 "directory object not open (internal error)"
#define XRDDPMOSS_T8003 "file object in use (internal error)"
#define XRDDPMOSS_T8004 "file object not open (internal error)"

class XrdDPMOssDir : public XrdOssDF {
public:
   int Close(long long *retsz = 0);
   int Opendir(const char *, XrdOucEnv &);
   int Readdir(char *buff, int blen);

   // Constructor and destructor
   XrdDPMOssDir(const char *tid, DpmRedirConfigOptions *rconf) :
      tident(tid), dirp(0), RedirConfig(rconf) {}
   ~XrdDPMOssDir();

private:
   XrdDPMOssDir(const XrdDPMOssDir&);
   XrdDPMOssDir& operator=(const XrdDPMOssDir&);

   const char                *tident;
   std::unique_ptr<DpmIdentity> identP;
   XrdDmStackWrap             sw;
   dmlite::Directory         *dirp;
   DpmRedirConfigOptions     *RedirConfig;
};

class XrdDPMOssFile : public XrdOssDF {
public:
   int Close(long long *retsz = 0);
   int Open(const char *, int, mode_t, XrdOucEnv &);
   int Fchmod(mode_t mode);

   int Fstat(struct stat *st);
   int Fsync();
   int Fsync(XrdSfsAio *aiop);
   int Ftruncate(unsigned long long len);
   int getFD();
   off_t getMmap(void **addr);
   int isCompressed(char *cxidp=0);
   ssize_t Read(off_t off, size_t sz);
   ssize_t Read(void *p, off_t off, size_t sz);
   int Read(XrdSfsAio *aiop);
   ssize_t ReadRaw(void *p, off_t off, size_t sz);
   ssize_t Write(const void *p, off_t off, size_t sz);
   int Write(XrdSfsAio *aiop);
   ssize_t ReadV(XrdOucIOVec *readV, int n);
   ssize_t WriteV(XrdOucIOVec *writeV, int n);

   // Constructor and destructor
   XrdDPMOssFile(XrdOssDF *dfroute, const char *tid) : tident(tid), dfroute(dfroute) {}
   virtual ~XrdDPMOssFile();

private:
   XrdDPMOssFile(const XrdDPMOssFile&);
   XrdDPMOssFile& operator=(const XrdDPMOssFile&);

   const char                *tident;
   std::unique_ptr<DpmIdentity> identP;
   dmlite::Location           Loc;
   std::unique_ptr<dmlite::IOHandler> ihP;
   bool                       NeedDoneW;
   XrdOucString               pfn;
   XrdOssDF                  *dfroute;
};

class XrdDPMOss : public XrdOss {
public:
   virtual XrdOssDF *newDir(const char *tident) {
      return (XrdOssDF *)new XrdDPMOssDir(tident,
         GetDpmRedirConfig(CommonConfig.cmslib));
   }
   virtual XrdOssDF *newFile(const char *tident) {
      XrdOssDF *dfroute = 0;
      if (UseDefaultOss) {
         if (!(dfroute = OssRoute->newFile(tident)))
            return dfroute;
      }
      return (XrdOssDF *)new XrdDPMOssFile(dfroute, tident);
   }

   virtual int Chmod(const char *, mode_t mode, XrdOucEnv *eP = 0);
   virtual int Create(const char *, const char *, mode_t, XrdOucEnv &, int opts = 0);
   virtual int Init(XrdSysLogger *, const char *);
   virtual int Mkdir(const char *, mode_t mode, int mkpath = 0, XrdOucEnv *eP = 0);

   virtual int Reloc(const char *, const char *, const char *, const char *x = 0) {
      return -ENOTSUP;
   }
   virtual int Remdir(const char *, int Opts = 0, XrdOucEnv *eP = 0);
   virtual int Rename(const char *, const char *,
      XrdOucEnv *eP1 = 0, XrdOucEnv *eP2 = 0);
   virtual int Stat(const char *, struct stat *, int opts = 0, XrdOucEnv *eP = 0);

   virtual int StatFS(const char *path, char *buff, int &blen, XrdOucEnv *eP = 0);

   virtual int StatLS(XrdOucEnv &env, const char *cgrp, char *buff, int &blen);

   virtual int StatXA(const char *path, char *buff, int &blen, XrdOucEnv *eP = 0);

   virtual int StatXP(const char *path, unsigned long long &attr,
      XrdOucEnv *eP = 0);

   virtual int Truncate(const char *, unsigned long long, XrdOucEnv *eP = 0);

   virtual int Unlink(const char *fn, int Opts = 0, XrdOucEnv *eP = 0);

   virtual int Stats(char *bp, int bl);

   virtual int StatVS(XrdOssVSInfo *sP, const char *sname = 0, int updt = 0) {
      return -ENOTSUP;
   }

   virtual int Lfn2Pfn(const char *Path, char *buff, int blen);

   virtual
   const char *Lfn2Pfn(const char *Path, char *buff, int blen, int &rc);

   XrdDPMOss(XrdOss *OssRoute, char *params): XrdOss(), OssRoute(OssRoute), UseDefaultOss(1) { }
   
   virtual ~XrdDPMOss() { }

private:
   int ConfigProc(XrdSysError &Eroute, const char *configfn);

   DpmCommonConfigOptions CommonConfig;
   XrdOss                *OssRoute;
   bool                   UseDefaultOss;
};
#endif   /* XRDDPMOSS_HH */


/******************************************************************************/
/*           S t o r a g e   S y s t e m   I n s t a n t i a t o r            */
/******************************************************************************/

// This function is called to obtain an instance of a configured XrdOss object.
// It is passed the object that would have been used as the storage system.
// The object is not initialized (i.e., Init() has not yet been called).
// This allows one to easily wrap the native implementation or to completely 
// replace it, as needed. The name of the config file and any parameters
// specified after the path on the ofs.osslib directive are also passed (note
// that if no parameters exist, parms may be null).

extern "C" {
   XrdOss *XrdOssGetStorageSystem(XrdOss *native_oss,
      XrdSysLogger *Logger,
      const char *config_fn,
      const char *parms);
}
