/*
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * Author: Fabrizio Furano, David Smith <David.Smith@cern.ch>
*/

#ifndef _XRDDPM_TRACE_H
#define _XRDDPM_TRACE_H

#include <XrdSys/XrdSysError.hh>
#include <XrdOuc/XrdOucTrace.hh>

#define TRACE_MOST     0x3fcd
#define TRACE_ALL      0xffff
#define TRACE_opendir  0x0001
#define TRACE_readdir  0x0002
#define TRACE_closedir TRACE_opendir
#define TRACE_delay    0x0400
#define TRACE_dir      TRACE_opendir | TRACE_readdir | TRACE_closedir
#define TRACE_open     0x0004
#define TRACE_qscan    0x0008
#define TRACE_close    TRACE_open
#define TRACE_read     0x0010
#define TRACE_redirect 0x0800
#define TRACE_write    0x0020
#define TRACE_IO       TRACE_read | TRACE_write | TRACE_aio
#define TRACE_exists   0x0040
#define TRACE_chmod    TRACE_exists
#define TRACE_remove   0x0080
#define TRACE_rename   TRACE_remove
#define TRACE_sync     0x0100
#define TRACE_truncate 0x0200
#define TRACE_fsctl    0x0400
#define TRACE_getstats 0x0800
#define TRACE_mkdir    0x1000
#define TRACE_aio      0x4000
#define TRACE_debug    0x8000

#ifndef NODEBUG

#include <XrdSys/XrdSysHeaders.hh>

#define DEBUG(y) if (Trace.What & TRACE_debug) TRACEX(y)
#define TRACE(x,y) if (Trace.What & TRACE_ ## x) TRACEX(y)
#define TRACEX(y) {Trace.Beg(epname); cerr <<y; Trace.End();}

#define DEBUG_TID(y) if (Trace.What & TRACE_debug) TRACEX_TID(y)
#define TRACE_TID(x,y) if (Trace.What & TRACE_ ## x) TRACEX_TID(y)
#define TRACEX_TID(y) {Trace.Beg(epname,tident); cerr <<y; Trace.End();}

#define EPNAME(x) static const char *epname = x;

#else

#define DEBUG(y)
#define TRACE(x, y)
#define EPNAME(x)
#define DEBUG_TID(y)
#define TRACE_TID(y)

#endif
#endif
