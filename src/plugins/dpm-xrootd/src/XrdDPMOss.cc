/* 
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * File:   XrdDPMOss.cc
 * Author: Fabrizio Furano, David Smith
 * 
 * An Oss plugin that interacts with a DPM
 *
 * Created on December 21, 2011, 10:25 AM
 */

#include "XrdCompileVersion.hh"
#include "XrdDPMTrace.hh"
#include "XrdDPMOss.hh"

#include <XrdOuc/XrdOucEnv.hh>
#include <XrdSec/XrdSecEntity.hh>
#include <XrdSys/XrdSysPthread.hh>
#include <XrdSfs/XrdSfsAio.hh>
#include <XrdVersion.hh>

#include <list>
#include <vector>
#include <memory>
#include <exception>

#include <fcntl.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>

#include "dmlite/cpp/status.h"
#include "dmlite/cpp/dmlite.h"
#include "dmlite/cpp/catalog.h"
#include "dmlite/cpp/io.h"
#include "dmlite/cpp/poolmanager.h"
#include "dmlite/cpp/utils/logger.h"
#include "utils/DomeUtils.h"

XrdVERSIONINFO(XrdOssGetStorageSystem,XrdDPMOss)

/******************************************************************************/
/*                  E r r o r   R o u t i n g   O b j e c t                   */
/******************************************************************************/

namespace DpmOss {
   XrdSysError Say(0, "dpmoss_");
   XrdOucTrace Trace(&Say);
   XrdDmStackStore dpm_ss;
}

using namespace DpmOss;

const char *XrdDpmOssErrorText[] =
      {XRDDPMOSS_T8001,
       XRDDPMOSS_T8002,
       XRDDPMOSS_T8003,
       XRDDPMOSS_T8004};

class fListItem {
public:
   fListItem(XrdDPMOssFile *fp, const char *fn) :
                XrdFileName(fn), fp(fp), canceled(0) { };

   XrdOucString XrdFileName;
   XrdDPMOssFile *fp;
   bool canceled;
};

static XrdSysMutex fLLock;
static std::list<fListItem> fList;

static XrdSysMutex pfnLLock;
static std::list<std::pair<XrdOucString, XrdOucString > > pfnTPCList;

static int checkAndClearItem(const XrdDPMOssFile *fp)
{
   bool canceled = 0;
   std::list<fListItem>::iterator itr;
   XrdSysMutexHelper hlpMtx(&fLLock);
   itr = fList.begin();
   while(itr != fList.end()) {
      if (itr->fp == fp) {
         if (itr->canceled) canceled = 1;
         itr = fList.erase(itr);
      } else {
         ++itr;
      }
   }
   return canceled;
}

static int markItemsCanceled(const char *fn)
{
   int n = 0;
   std::list<fListItem>::iterator itr;
   XrdSysMutexHelper hlpMtx(&fLLock);
   for(itr=fList.begin(); itr != fList.end(); ++itr) {
      if (itr->XrdFileName == fn) {
         itr->canceled = 1;
         ++n;
      }
   }
   return n;
}

static void addPfnToTPCList(XrdOucString lfn, XrdOucString pfn) {
   std::pair<XrdOucString, XrdOucString> p(lfn,pfn);
   XrdSysMutexHelper hlpMtx(&pfnLLock);
   pfnTPCList.push_front(p);
   while(pfnTPCList.size() > 1000) { pfnTPCList.pop_back(); }
}

static XrdOucString lookupPfnFromTPCList(XrdOucString lfn) {
   XrdOucString pfn;
   std::list<std::pair<XrdOucString, XrdOucString> >::iterator itr;
   XrdSysMutexHelper hlpMtx(&pfnLLock);
   for(itr=pfnTPCList.begin();itr!=pfnTPCList.end();++itr) {
      if (itr->first == lfn) {
         pfn = itr->second;
         break;
      }
   }
   return pfn;
}

XrdDPMOssDir::~XrdDPMOssDir() {
   if (dirp) {
      try {
         sw->getCatalog()->closeDir(dirp);
      } catch(...) {
         // do nothing
      }
   }
}

int XrdDPMOssDir::Close(long long *retsz) {
   EPNAME("Close");

   if (!dirp) {
      DEBUG_TID("Not open");
      return -XRDDPMOSS_E8002;
   }

   try {
      sw->getCatalog()->closeDir(dirp);
      dirp = 0;
   } catch(dmlite::DmException &e) {
      Say.Emsg("CloseDir", e.what());
      return -DmExErrno(e);
   } catch(const std::exception &) {
      Say.Emsg("CloseDir", "Unexpected exception");
      return -EINVAL;
   }
   sw.reset();

   DEBUG_TID("closed");
   return 0;
}

int XrdDPMOssDir::Opendir(const char *path, XrdOucEnv &Info) {
   EPNAME("Opendir");
   XrdOucString FullPath;

   if (!RedirConfig) {
      DEBUG_TID("RedirConfig not available");
      return -ENOTSUP;
   }

   if (dirp) {
      DEBUG_TID("Already open");
      return -XRDDPMOSS_E8001;
   }

   try {
      identP.reset(new DpmIdentity(&Info, RedirConfig->IdentConfig));
      sw.reset(*RedirConfig->ss, *identP);

      FullPath = TranslatePath(*RedirConfig, path, sw);
      dirp = sw->getCatalog()->openDir(SafeCStr(FullPath));
   } catch(dmlite::DmException &e) {
      Say.Emsg("opendir", e.what(), "; File", path);
      return -DmExErrno(e);
   } catch(const std::exception &) {
      Say.Emsg("opendir", "Unexpected exception");
      return -EINVAL;
   }

   TRACE_TID(opendir, "opened " << path << " (" << FullPath << ")");
   return 0;
}

int XrdDPMOssDir::Readdir(char *buff, int blen) {
   EPNAME("Readdir");

   if (!dirp) {
      DEBUG_TID("Not open");
      return -XRDDPMOSS_E8002;
   }

   struct dirent *de;
   *buff = '\0';

   try {
      de = sw->getCatalog()->readDir(dirp);
   } catch(dmlite::DmException &e) {
      Say.Emsg("Readdir", e.what());
      return -DmExErrno(e);
   } catch(const std::exception &) {
      Say.Emsg("Readdir", "Unexpected exception");
      return -EINVAL;
   }

   if (de) { strncat(buff, de->d_name, blen-1); }

   return 0;
}

XrdDPMOssFile::~XrdDPMOssFile() {
   checkAndClearItem(this);
   delete dfroute;
}

int XrdDPMOssFile::Fstat(struct stat *st) {
   EPNAME("Fstat");
   XrdOucString err;
   int ret;

   if (dfroute)
      return dfroute->Fstat(st);

   if (!ihP.get()) {
      DEBUG_TID("Not open");
      return -XRDDPMOSS_E8004;
   }

   memset(st, 0, sizeof (struct stat));
   do {
      ret = 0;
      try {
         st->st_size = ihP->fstat().st_size;
      } catch(dmlite::DmException &e) {
         err = e.what();
         ret = -DmExErrno(e);
      } catch(const std::exception &) {
         err = "Unexpected exception";
         ret = -EINVAL;
      }
   } while(ret == -EINTR);

   if (ret) {
      Say.Emsg("Fstat", SafeCStr(err), "; File", SafeCStr(pfn));
   }

   DEBUG_TID(pfn << " ; return " << ret);
   return ret;
}

int XrdDPMOssFile::Fsync() {
   EPNAME("Fsync");
   XrdOucString err;
   int ret;

   if (dfroute)
      return dfroute->Fsync();

   if (!ihP.get()) {
      DEBUG_TID("Not open");
      return -XRDDPMOSS_E8004;
   }

   do {
      ret = 0;
      try {
         ihP->flush();
      } catch(dmlite::DmException &e) {
         err = e.what();
         ret = -DmExErrno(e);
      } catch(const std::exception &) {
         err = "Unexpected exception";
         ret = -EINVAL;
      }
   } while(ret == -EINTR);

   if (ret) {
      Say.Emsg("Fsync", SafeCStr(err), "; File", SafeCStr(pfn));
   }

   DEBUG_TID("flush " << pfn << " ; return " << ret);
   return ret;
}

int XrdDPMOssFile::Fsync(XrdSfsAio *aiop) {
   if (dfroute) {return dfroute->Fsync(aiop);}
   aiop->Result = this->Fsync();
   aiop->doneWrite();
   return 0;
}

int XrdDPMOssFile::Ftruncate(unsigned long long len) {
   EPNAME("Ftruncate");
   if (dfroute) {return dfroute->Ftruncate(len);}
   DEBUG_TID("Ftrucate of " << pfn << " to " << len << " not supported");
   return -ENOTSUP;
}

int XrdDPMOssFile::getFD() {
   EPNAME("getFD");
   if (dfroute) {return dfroute->getFD();}

   if (!ihP.get()) {
      DEBUG_TID("Not open");
      return -1;
   }

   int thefd = -1;
   try {
      thefd = ihP->fileno();
   } catch(const std::exception &) {
      // nothing
   }

   DEBUG_TID("fd = " << thefd);
   return thefd;
}

off_t XrdDPMOssFile::getMmap(void **addr) {
   if (dfroute) {return dfroute->getMmap(addr);}
   if (addr) *addr = 0;
   return 0;
}

int XrdDPMOssFile::isCompressed(char *cxipd) {
   if (dfroute) {return dfroute->isCompressed(cxipd);}
   return 0;
}

ssize_t XrdDPMOssFile::Read(off_t off, size_t sz) {
   EPNAME("Read");

   if (dfroute)
      return dfroute->Read(off, sz);

   if (!ihP.get()) {
      DEBUG_TID("Not open");
      return (ssize_t)-XRDDPMOSS_E8004;
   }
   DEBUG_TID("return 0 (not implemented)");
   return 0;
}

ssize_t XrdDPMOssFile::Read(void *p, off_t off, size_t sz) {
   EPNAME("Read");
   ssize_t retval;
   XrdOucString err;

   if (dfroute)
      return dfroute->Read(p, off, sz);

   if (!ihP.get()) {
      DEBUG_TID("Not open");
      return (ssize_t)-XRDDPMOSS_E8004;
   }

   do {
      try {
         retval = (ssize_t) ihP->pread(p, sz, off);
         if (retval < 0) {
            char eb[128];
            int errnum = errno;
            dpm_strerror_r(errnum, eb, sizeof(eb));
            throw dmlite::DmException(errnum, "%s", eb);
         }
      } catch(dmlite::DmException &e) {
         retval = -DmExErrno(e);
         err = e.what();
      } catch(const std::exception &) {
         retval = -EINVAL;
         err = "Unexpected exception";
      }
   } while(retval == -EINTR);

   if (retval<0) {
      Say.Emsg("Read", SafeCStr(err), "; File", SafeCStr(pfn));
   }

   return retval;
}

int XrdDPMOssFile::Read(XrdSfsAio *aiop) {
   if (dfroute)
      return dfroute->Read(aiop);

   // execute in a synchronous fashion
   aiop->Result = this->Read((void*)aiop->sfsAio.aio_buf,
      (off_t)aiop->sfsAio.aio_offset,
      (size_t)aiop->sfsAio.aio_nbytes);
   aiop->doneRead();
   return 0;
}

ssize_t XrdDPMOssFile::ReadRaw(void *p, off_t off, size_t sz) {
   if (dfroute) {return dfroute->ReadRaw(p, off, sz);}
   return this->Read(p,off,sz);
}

ssize_t XrdDPMOssFile::ReadV(XrdOucIOVec *readV, int n) {
   if (dfroute) {return dfroute->ReadV(readV, n);}
   return XrdOssDF::ReadV(readV, n);
}

ssize_t XrdDPMOssFile::Write(const void *p, off_t off, size_t sz) {
   EPNAME("Write");
   ssize_t retval;
   XrdOucString err;

   if (dfroute)
      return dfroute->Write(p, off, sz);

   if (!ihP.get()) {
      DEBUG_TID("Not open");
      return (ssize_t)-XRDDPMOSS_E8004;
   }

   do {
      try {
         retval = (ssize_t) ihP->pwrite(p, sz, off);
         if (retval < 0) {
            char eb[128];
            int errnum = errno;
            dpm_strerror_r(errnum, eb, sizeof(eb));
            throw dmlite::DmException(errnum, "%s", eb);
         }
      } catch(dmlite::DmException &e) {
         retval = -DmExErrno(e);
         err = e.what();
      } catch(const std::exception &) {
         retval = -EINVAL;
         err = "Unexpected exception";
      }
   } while(retval == -EINTR);

   if (retval<0) {
      Say.Emsg("Write", SafeCStr(err), "; File", SafeCStr(pfn));
   }

   return retval;
}

int XrdDPMOssFile::Write(XrdSfsAio *aiop) {
   if (dfroute)
      return dfroute->Write(aiop);

   // execute in a synchronous fashion
   aiop->Result = this->Write((const void*)aiop->sfsAio.aio_buf,
      (off_t)aiop->sfsAio.aio_offset,
      (size_t)aiop->sfsAio.aio_nbytes);
   aiop->doneWrite();
   return 0;
}

ssize_t XrdDPMOssFile::WriteV(XrdOucIOVec *writeV, int n) {
   if (dfroute) {return dfroute->WriteV(writeV, n);}
   return XrdOssDF::WriteV(writeV, n);
}

int XrdDPMOssFile::Fchmod(mode_t mode) {
   EPNAME("Fchmod");

   if (dfroute) {
      int ret = dfroute->Fchmod(0660);
      DEBUG_TID("Issued XrdOssDF::Fchmod (forced 0660) ret=" << ret);
      return ret;
   }

   if (!ihP.get()) {
      DEBUG_TID("Not open");
      return -XRDDPMOSS_E8004;
   }

   DEBUG_TID("ignoring Fchmod request for " << pfn);
   return 0;
}

int XrdDPMOssFile::Open(const char *fn, int Oflag, mode_t mode, XrdOucEnv &env) {
   EPNAME("Open");
   XrdOucString sfn;
   int ret = 0;

   if (!dfroute && ihP.get()) {
      DEBUG_TID("Already open");
      return -XRDDPMOSS_E8003;
   }

   pfn.erase();
   NeedDoneW = 0;
   try {
      // get the user identity
      identP.reset(new DpmIdentity(&env));

      // get Location from the cgi info
      EnvToLocation(Loc, &env, fn);

      pfn = Loc[0].url.path.c_str();

      if (!pfn.length()) {
         throw dmlite::DmException(EINVAL, "No pfn");
      }

      int flags = Oflag & ~(O_CREAT | O_TRUNC | O_EXCL);
#ifdef O_LARGEFILE
      flags |= O_LARGEFILE;
#endif
      if (Oflag & (O_WRONLY | O_RDWR)) {
         NeedDoneW = 1;
         flags |= O_CREAT | O_EXCL;
      }   

      std::string s = Loc[0].url.query.getString("sfn");
      sfn = s.c_str();

      // get a stack to create the IOHandler
      DpmIdentity empty_ident; 
      XrdDmStackWrap sw(dpm_ss, empty_ident);
      
      int repeatOpen=2;
      do {
        ret = 0;
        try {
          if (dfroute) {
            ret = dfroute->Open(SafeCStr(pfn),flags, 0660, env);
          } else {
            ihP.reset(sw->getIODriver()->createIOHandler(
              SafeCStr(pfn),flags | dmlite::IODriver::kInsecure,
                                                         Loc[0].url.query));
          }
          
        } catch(dmlite::DmException &e) {
          if (DmExErrno(e) == ENOENT) {
            ret = -ENOENT;
          } else if (DmExErrno(e) != EINTR) {
            throw;
          }
        }
        
        if (!ret) break;
        
        // We are here if there was an error
        repeatOpen--;
        
        if ((flags & O_CREAT) && repeatOpen && (ret == -ENOENT)) {
          Say.Emsg("Open", "Going to create missing physical basedir ");
          char *dname = strdup(pfn.c_str());
          if (dname) {
            
            // Trim at the end and leave the path name
            //char *p = dname+pfn.length()-1;
            //while ( (p > dname) && (*p != '/') ) *p-- = '\0';
            //if (*p == '/') *p = '\0';
            
            Say.Emsg("Open", "Creating missing physical basedir '", dname, "'");
            
            // Beware, this func can throw damn exceptions
            DomeUtils::mkdirp(dname);
            Say.Emsg("Open", "mkdir() returned ");
            
            free(dname);
          }
          
        }
        
        
      } while(repeatOpen);

   } catch(dmlite::DmException &e) {
      if (pfn.length()) {
         Say.Emsg("Open", e.what(), "; File", SafeCStr(pfn));
      } else {
         Say.Emsg("Open", e.what());
      }
      ret = -DmExErrno(e);
   } catch(const std::exception &) {
      ret = -EINVAL;
      Say.Emsg("Open","Unexpected exception");
   }

   if (ret) {
      ihP.reset();
      if (NeedDoneW) {
         try {
            XrdDmStackWrap sw(dpm_ss, *identP);
            sw->getPoolManager()->cancelWrite(Loc);
         } catch(const std::exception &) {
            // ignore
         }
      }
      return ret;
   }

   if (NeedDoneW) {
      fListItem item(this,fn);
      XrdSysMutexHelper hlpMtx(&fLLock);
      fList.push_back(item);
   }

   if (sfn.length() && sfn != fn) {
      TRACE_TID(open, "opened " << fn << " (" << sfn << ")" << " --> " << pfn);
   } else {
      TRACE_TID(open, "opened " << fn << " --> " << pfn);
   }

   return 0;
}

int XrdDPMOssFile::Close(long long *retsz) {
   EPNAME("Close");
   XrdOucString err;
   int ret;

   if (!dfroute && !ihP.get()) {
      DEBUG_TID("Not open");
      return -XRDDPMOSS_E8004;
   }

   do {
      ret = 0;
      try {
         if (dfroute) {
            ret = dfroute->Close(retsz);
            break;
         } else {
            if (retsz) { *retsz = ihP->fstat().st_size; }
            ihP->close();
         }
      } catch(dmlite::DmException &e) {
         err = e.what();
         ret = -DmExErrno(e);
      } catch(const std::exception &) {
         err = "Unexpected exception";
         ret = -EINVAL;
      }
   } while(ret == -EINTR);

   if (ret) {
      Say.Emsg("Close", SafeCStr(err), "; File", SafeCStr(pfn));
      DEBUG_TID("close returned " << ret);
   }
   ihP.reset();

   try {
      bool canceled = checkAndClearItem(this);
      if (NeedDoneW) {
         XrdDmStackWrap sw(dpm_ss, *identP);
         if (!canceled && ret == 0) {
            try {
              DEBUG_TID("doneWriting");
              sw->getIODriver()->doneWriting(Loc);
            } catch (...) {
              DEBUG_TID("canceling file after failed putdone");
              try {
                sw->getPoolManager()->cancelWrite(Loc);
              }
              catch (...) {};
              
              throw;
            }
            
            
         } else {
            DEBUG_TID("canceling file");
            sw->getPoolManager()->cancelWrite(Loc);
         }
      }
   } catch(dmlite::DmException &e) {
      Say.Emsg("Close", e.what(), "; File", SafeCStr(pfn));
      ret = -DmExErrno(e);
   } catch(const std::exception &) {
      Say.Emsg("Close", "Unexpected exception");
      ret = -EINVAL;
   }

   DEBUG_TID("return " << ret);
   return ret;
}

int XrdDPMOss::Chmod(const char *path, mode_t mode, XrdOucEnv *eP) {
   EPNAME("Chmod");
   DEBUG("EINVAL");
   return -EINVAL;
}

int XrdDPMOss::Create(const char *tident, const char *path, mode_t access_mode,
      XrdOucEnv &env, int Opts) {
   EPNAME("Create");

   if (env.Get("tpc.key")) {
      // get Location from the cgi info
      dmlite::Location loc;
      EnvToLocation(loc, &env, path);

      XrdOucString lfn(path);
      XrdOucString pfn(loc[0].url.path.c_str());

      addPfnToTPCList(lfn,pfn);
      DEBUG_TID("Added lfn2pfn map " << lfn << ":" << pfn <<
                ", returning ENOTSUP");
   }
   return -ENOTSUP;
}

int XrdDPMOss::ConfigProc(XrdSysError &Eroute, const char *configfn) {
   char *var;
   int cfgFD, retc, NoGo = 0;
   XrdOucEnv myEnv;
   XrdOucStream Config(&Eroute, getenv("XRDINSTANCE"), &myEnv, "=====> ");

   if (!configfn || !*configfn) {
      Eroute.Say("Config warning: config file not specified; "
            "defaults assumed.");
      return 0;
   }

   if ((cfgFD = open(configfn, O_RDONLY, 0)) < 0) {
      Eroute.Emsg("Config", errno, "open config file", configfn);
      return 1;
   }
   Config.Attach(cfgFD);

   while((var = Config.GetMyFirstWord())) {
      if (!strncmp(var, "dpm.", 4)) {
         if (!strcmp(var+4, "dmio")) {
            UseDefaultOss = 0;
         }
      }
   }
   if ((retc = Config.LastError())) {
      NoGo = Eroute.Emsg("Config", retc, "read config file",
         configfn);
   }
   Config.Close();

   return NoGo;
}

int XrdDPMOss::Init(XrdSysLogger *lp, const char *configfn) {

   Say.logger(lp);

   XrdSysError_Table *ETab = new XrdSysError_Table(XRDDPMOSS_EBASE,
               XRDDPMOSS_ELAST, XrdDpmOssErrorText);
   Say.addTable(ETab);

   ETab = XrdDmliteError_Table();
        Say.addTable(ETab);

   XrdDmCommonInit(lp);

   Say.Say("This is XrdDPMOss "
           XrdDPMVERSIONSTR
           " compiled with xroot "
           XrdCompileVersion );

   int NoGo = DpmCommonConfigProc(Say, configfn, CommonConfig);
   if (NoGo) return NoGo;

   Trace.What = CommonConfig.OssTraceLevel;
   try {
      dpm_ss.SetDmConfFile(CommonConfig.DmliteConfig);
      dpm_ss.SetDmStackPoolSize(CommonConfig.DmliteStackPoolSize);
      // get a dmlite stack to trigger the loading of the dmlite
      // configuration. This may do things like setting variables
      // in the enviornment, which is not thread safe and might cause
      // problems later.
      DpmIdentity empty_ident;
      XrdDmStackWrap sw(dpm_ss, empty_ident);
   } catch(const std::exception &) {
      Say.Emsg("Init","problem setting up the dmlite stack");
      return 1;
   }

   NoGo |= ConfigProc(Say, configfn);
   if (NoGo) return NoGo;

   if (UseDefaultOss)
      return OssRoute->Init(lp, configfn);
   return 0;
}

int XrdDPMOss::Mkdir(const char *path, mode_t mode, int mkpath,
      XrdOucEnv *envP) {
   EPNAME("Mkdir");
   DEBUG("EINVAL");
   return -EINVAL;
}

int XrdDPMOss::Remdir(const char *, int Opts, XrdOucEnv *eP) {
   EPNAME("Remdir");
   DEBUG("EINVAL");
   return -EINVAL;
}

int XrdDPMOss::Rename(const char *, const char *, XrdOucEnv *eP1,
      XrdOucEnv *eP2) {
   EPNAME("Rename");
   DEBUG("EINVAL");
   return -EINVAL;
}

int XrdDPMOss::Stat(const char *fn, struct stat *st, int opts, XrdOucEnv *eP) {
   EPNAME("Stat");
   DpmRedirConfigOptions *RedirConfig(
      GetDpmRedirConfig(CommonConfig.cmslib));
   dmlite::ExtendedStat xstat;
   XrdOucString sfn;

   if (!RedirConfig) {
      DEBUG("RedirConfig not available");
      return -ENOTSUP;
   }

   if (!eP) {
      DEBUG("No environment parameters passed.");
      return -EINVAL;
   }

   memset(st, 0, sizeof (struct stat));
   
   // If this was requested for a Locate, then just say yes
   char *skip = eP->Get("dpm.skiplocate");
   if (skip && (*skip == '1')) {
      eP->Put("dpm.skiplocate", "0");
      return 0;
   }

   try {
      DpmIdentity ident(eP);
      XrdDmStackWrap sw(*RedirConfig->ss, ident);

      dmlite::Location loc;
      EnvToLocation(loc, eP, fn);

      std::string s = loc[0].url.query.getString("sfn");
      sfn = s.c_str();
      
      dmlite::DmStatus st = sw->getCatalog()->extendedStat(xstat, SafeCStr(sfn));
      if ( !st.ok() ) {
        DEBUG("Stat " << st.what() << " file: " << fn);
        return -DmExInt2Errno(st.code());
      }
      
   } catch(dmlite::DmException &e) {
      DEBUG("Stat " << e.what() << " file: " << fn);
      return -DmExErrno(e);
   } catch(const std::exception &) {
      Say.Emsg("Stat","Unexpected exception");
      return -EINVAL;
   }
   
   st->st_atim.tv_sec = xstat.stat.st_atime;
   st->st_ctim.tv_sec = xstat.stat.st_ctime;
   st->st_mode = xstat.stat.st_mode;
   st->st_nlink = xstat.stat.st_nlink;
   st->st_mtim.tv_sec = xstat.stat.st_mtime;
   st->st_size = xstat.stat.st_size;
   st->st_ino = xstat.stat.st_ino;

   DEBUG("stat " << sfn);
   return 0;
}

int XrdDPMOss::StatFS(const char *path, char *buff, int &blen, XrdOucEnv *eP) {
   EPNAME("StatFS");
   DpmRedirConfigOptions *RedirConfig(GetDpmRedirConfig(CommonConfig.cmslib));
   int sVal, wVal, Util;
   long long fSpace, fSize;

   if (!RedirConfig) {
      DEBUG("RedirConfig not available");
      return -ENOTSUP;
   }

   try {
      DpmIdentity ident(eP);
      XrdDmStackWrap sw(*RedirConfig->ss, ident);

      dmlite::Location loc;
      EnvToLocation(loc, eP, path);

      std::string s = loc[0].url.query.getString("sfn");

      std::vector<dmlite::Replica> reps = sw->getCatalog()->getReplicas(s);

      std::vector<dmlite::Pool> pools = sw->getPoolManager()->getPools();

      std::vector<dmlite::Replica>::iterator ritr;
      std::vector<dmlite::Pool>::iterator pitr;
      std::unique_ptr<dmlite::PoolHandler> phP;

      for(ritr=reps.begin();ritr != reps.end();++ritr) {
         std::unique_ptr<dmlite::PoolHandler> phtmpP;
         bool fndw=0;
         for(pitr=pools.begin();pitr!=pools.end();++pitr) {
            phtmpP.reset(sw->getPoolDriver(pitr->type)->
                         createPoolHandler(pitr->name));
            if (phtmpP->replicaIsAvailable(*ritr)) {
               if (phtmpP->poolIsAvailable()) {fndw=1;}
               phP=std::move(phtmpP);
               break;
            }
         }
         if (fndw) { break; }
      }

      if (!phP.get()) {
         throw dmlite::DmException(DMLITE_NO_SUCH_POOL,
            "No available pool associated to the file");
      }

      fSize = phP->getTotalSpace();
      fSpace = phP->getFreeSpace();
      sVal = 0;
      wVal = (phP->poolIsAvailable()) ? 1 : 0;

      if (fSpace <= 0) {fSize = fSpace = 0; Util = 0;}
      else {
         Util = (fSize ? (fSize - fSpace)*100LL/fSize : 0);
         fSpace = fSpace >> 20LL;
         if ((fSpace >> 31LL)) fSpace = 0x7fffffff;
      }

   } catch(dmlite::DmException &e) {
      DEBUG("StatFS " << e.what());
      return -DmExErrno(e);
   } catch(const std::exception &) {
      Say.Emsg("StatFS","Unexpected exception");
      return -EINVAL;
   }

   blen = snprintf(buff, blen, "%d %lld %d %d %lld %d",
      wVal, (wVal ? fSpace : 0LL), (wVal ? Util : 0),
      sVal, (sVal ? fSpace : 0LL), (sVal ? Util : 0));

   return 0;
}

int XrdDPMOss::StatXA(const char *path, char *buff, int &blen, XrdOucEnv *eP) {
   EPNAME("StatXA");
   DpmRedirConfigOptions *RedirConfig(
      GetDpmRedirConfig(CommonConfig.cmslib));
   dmlite::ExtendedStat xstat;

   if (!RedirConfig) {
      DEBUG("RedirConfig not available");
      return -ENOTSUP;
   }

   if (!eP) {
      DEBUG("No environment parameters passed.");
      return -EINVAL;
   }

   try {
      DpmIdentity ident(eP);
      XrdDmStackWrap sw(*RedirConfig->ss, ident);
      dmlite::DmStatus st;
      dmlite::Location loc;
      EnvToLocation(loc, eP, path);

      std::string s = loc[0].url.query.getString("sfn");

      st = sw->getCatalog()->extendedStat(xstat, s.c_str());
      if ( !st.ok() ) {
        DEBUG("StatXA " << st.what() << " file: " << path);
        return -DmExInt2Errno(st.code());
      }
      
   } catch(dmlite::DmException &e) {
      DEBUG("StatXA " << e.what() << " file: " << path);
      return -DmExErrno(e);
   } catch(const std::exception &) {
      Say.Emsg("StatXA","Unexpected exception");
      return -EINVAL;
   }

   char fType;
   if (S_ISREG(xstat.stat.st_mode))      fType = 'f';
   else if (S_ISDIR(xstat.stat.st_mode)) fType = 'd';
   else                                  fType = 'o';

   long long Size, Mtime, Ctime, Atime;
   Size = xstat.stat.st_size;
   Mtime = xstat.stat.st_mtime;
   Ctime = xstat.stat.st_ctime;
   Atime = xstat.stat.st_atime;

   blen = snprintf(buff, blen,
      "oss.cgroup=%s&oss.type=%c&oss.used=%lld&oss.mt=%lld"
      "&oss.ct=%lld&oss.at=%lld&oss.u=*&oss.g=*&oss.fs=%c",
      "public", fType, Size, Mtime, Ctime, Atime,
      (xstat.stat.st_mode & S_IWUSR ? 'w':'r'));

   return 0;
}

int XrdDPMOss::StatLS(XrdOucEnv &env, const char *cgrp, char *buff, int &blen) {
   EPNAME("StatLS");
   DpmRedirConfigOptions *RedirConfig(GetDpmRedirConfig(CommonConfig.cmslib));
   static const char *Resp="oss.cgroup=%s&oss.space=%lld&oss.free=%lld"
         "&oss.maxf=%lld&oss.used=%lld&oss.quota=%lld";

   if (!RedirConfig) {
      DEBUG("RedirConfig not available");
      return -ENOTSUP;
   }

   try {
      DpmIdentity ident(&env);
      XrdDmStackWrap sw(*RedirConfig->ss, ident);

      std::vector<dmlite::Pool> p =
         sw->getPoolManager()->getPools(dmlite::PoolManager::kForBoth);

      long long fSize=0, fSpace=0, mfSpace=0;

      std::vector<dmlite::Pool>::iterator itr;
      for(itr=p.begin();itr!=p.end();++itr) {
         std::unique_ptr<dmlite::PoolHandler> phP(sw->getPoolDriver(itr->type)->
            createPoolHandler(itr->name));
         fSize += phP->getTotalSpace();
         long long tmp = phP->getFreeSpace();
         fSpace += tmp;
         if (tmp > mfSpace) { mfSpace = tmp; }
      }

      blen = snprintf(buff, blen, Resp, "public", fSize, fSpace,
            mfSpace, fSize-fSpace, -1LL);

   } catch(dmlite::DmException &e) {
      DEBUG("Stat " << e.what());
      return -DmExErrno(e);
   } catch(const std::exception &) {
      Say.Emsg("Stat","Unexpected exception");
      return -EINVAL;
   }

   return 0;
}

int XrdDPMOss::Unlink(const char *fn, int Opts, XrdOucEnv *eP) {
   EPNAME("Unlink");

   int n = markItemsCanceled(fn);

   DEBUG("marked " << n << " items for cancel: " << fn);
   return n ? 0 : -ENOENT;
}

int XrdDPMOss::Truncate(const char *path, unsigned long long size,
      XrdOucEnv *eP) {
   EPNAME("Truncate");
   DEBUG("Truncate " << path << " to " << size << " (not sup)");
   return -ENOTSUP;
}

int XrdDPMOss::StatXP(const char *path, unsigned long long &attr,
      XrdOucEnv *eP) {
   EPNAME("StatXP");
   DEBUG("StatXP");
   return 0;
}

int XrdDPMOss::Stats(char *buff, int blen) {
   //EPNAME("Stats");
   static const char statfmt1[] =
      "<stats id=\"dpmoss\" v=\"" XrdDPMVERSIONSTR 
      "/" XrdCompileVersion "/" __TIME__ "/" __DATE__ "\">";
   static const char statfmt2[] = "</stats>";
   static const int  statflen = sizeof(statfmt1) + sizeof(statfmt2) - 1;
   char *bp = buff;

// If only size wanted, return what size we need
//
   if (!buff) {
      int ex = 0;
      if (UseDefaultOss)
         ex = OssRoute->Stats(buff, blen);
      return statflen + ex;
   }

   // Make sure we have enough space
   //
   if (blen < statflen) return 0;
   strcpy(bp, statfmt1);
   bp += sizeof(statfmt1)-1; blen -= sizeof(statfmt1)-1;

   // Add trailer
   //
   if (blen >= (int)sizeof(statfmt2)) {
      strcpy(bp, statfmt2);
      bp += sizeof(statfmt2)-1;
      blen -= sizeof(statfmt2)-1;
   }

   // Add stats from default oss if we're using it
   //
   if (UseDefaultOss)
      bp += OssRoute->Stats(bp, blen);

   return bp - buff;
}

int XrdDPMOss::Lfn2Pfn(const char *Path, char *buff, int blen) {
   XrdOucString pfn = lookupPfnFromTPCList(Path);
   if (!pfn.length()) { return -ENOENT; }
   if (pfn.length() >= blen) { return -ENAMETOOLONG; }
   strcpy(buff, SafeCStr(pfn));
   return 0;
}

const char *XrdDPMOss::Lfn2Pfn(const char *Path, char *buff, int blen,
      int &rc) {
   rc = Lfn2Pfn(Path, buff, blen);
   if (rc) { return 0; }
   return buff;
}


// -------------------------

XrdOss *XrdOssGetStorageSystem(XrdOss *native_oss, XrdSysLogger *Logger,
   const char *config_fn, const char *parms) {
   //EPNAME("XrdOssGetStorageSystem");

   XrdDPMOss *myoss = new XrdDPMOss(native_oss, (char *) parms);
   if (myoss) {
      if (myoss->Init(Logger, config_fn)) {
         delete myoss;
         myoss = 0;
      }
   }

   return myoss;
}
