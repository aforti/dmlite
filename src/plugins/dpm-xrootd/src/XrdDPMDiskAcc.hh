/* 
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * File:   XrdDPMDiskAcc.hh
 * Author: Fabrizio Furano, David Smith
 * 
 * An Acc plugin for a DPM-xrootd disk server
 *
 * Created on Jan, 13, 2012, 10:25 AM
 */

#ifndef XRDDPMDISKACC_HH
#define XRDDPMDISKACC_HH

#include <XrdAcc/XrdAccAuthorize.hh>
#include <XrdSys/XrdSysLogger.hh>
#include <XrdDPMCommon.hh>

class XrdDPMDiskAcc : public XrdAccAuthorize {
public:
   /* Access() indicates whether or not the user/host is permitted access to the
      path for the specified operation. The default implementation that is
      statically linked determines privileges by combining user, host, user group, 
      and user/host netgroup privileges. If the operation is AOP_Any, then the 
      actual privileges are returned and the caller may make subsequent tests using 
      Test(). Otherwise, a non-zero value is returned if access is permitted or a 
      zero value is returned is access is to be denied. Other iplementations may
      use other decision making schemes but the return values must mean the same.

      Parameters: Entity    -> Authentication information
             path      -> The logical path which is the target of oper
             oper      -> The operation being attempted (see above)
             Env       -> Environmental information at the time of the
                operation as supplied by the path CGI string.
                This is optional and the pointer may be zero.
    */

   virtual XrdAccPrivs Access(const XrdSecEntity *Entity,
      const char *path,
      const Access_Operation oper,
      XrdOucEnv *Env = 0);

   int Audit(const int accok,
      const XrdSecEntity *Entity,
      const char *path,
      const Access_Operation oper,
      XrdOucEnv *Env) {

      return accok;

   }

   
   // Test() check whether the specified operation is permitted. If permitted it
   // returns a non-zero. Otherwise, zero is returned.
   //
   int Test(const XrdAccPrivs priv, const Access_Operation oper);

   XrdDPMDiskAcc(const char *cfn, const char *parm);

   virtual ~XrdDPMDiskAcc();

private:
   int maxgracetime;
   std::vector<unsigned char> key;
   std::vector<XrdOucString> LocalHostNames;
   DpmCommonConfigOptions CommonConfig;
};



#endif   /* XRDDPMOSS_HH */

/******************************************************************************/
/*                 X r d A c c A u t h o r i z e O b j e c t                  */
/******************************************************************************/

/* XrdAccAuthorizeObject() is called to obtain an instance of the auth object
   that will be used for all subsequent authorization decisions. If it returns
   a null pointer; initialization fails and the program exits. It must be
   declared as follows:

   extern "C" XrdAccAuthorize *XrdAccAuthorizeObject(XrdSysLogger *lp,
                       const char   *cfn,
                       const char   *parm);

   where:
   lp    -> XrdSysLogger to be tied to an XrdSysError object for messages
   cfn   -> The name of the configuration file
   parm  -> Parameters specified on the authlib directive. If none it is zero.

   For the default statically linked authorization framework, the non-extern C
   XrdAccDefaultAuthorizeObject() is called instead so as to not conflict with
   that symbol in a shared library plug-in, otherwise (depending on the loader)
   we might not be able to get its address in the shared segment. Normally,
   the difference between extern C and not should be all that matters. This
   mechanism here just makes sure we don't rely on that assumption.
 */
extern "C" XrdAccAuthorize *XrdAccAuthorizeObject(XrdSysLogger *lp,
   const char *cfn,
   const char *parm);
