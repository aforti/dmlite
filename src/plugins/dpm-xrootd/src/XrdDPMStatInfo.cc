/*
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * Author: David Smith <David.Smith@cern.ch>
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <exception>

#include <XrdOss/XrdOss.hh>
#include <XrdOss/XrdOssStatInfo.hh>
#include <XrdOuc/XrdOucEnv.hh>
#include <XrdOuc/XrdOucName2Name.hh>
#include <XrdSys/XrdSysError.hh>

#include <XrdVersion.hh>
#include <XrdDPMTrace.hh>
#include <XrdDPMCommon.hh>
#include <XrdDPMStatInfo.hh>
#include <XrdCompileVersion.hh>

#include "dmlite/cpp/status.h"
#include "dmlite/cpp/dmlite.h"
#include "dmlite/cpp/catalog.h"

namespace DpmStatInfo {
   XrdSysError Say(0,"dpmstatinfo_");
   XrdOucTrace Trace(&Say);
   DpmCommonConfigOptions CommonConfig;
   DpmRedirConfigOptions RedirConfig;
   XrdDmStackStore dpm_ss;
}

using namespace DpmStatInfo;

  
/******************************************************************************/
/*                        X r d O s s S t a t I n f o                         */
/******************************************************************************/
  
// Note this is version 2 of XrdOssStatInfo() you specify the following
// directives in your config file:

// if exec cmsd
// oss.statlib -2 <path to your statinfo plugin shared library> [<any parms>]
// fi

// Details in XrdOssStatInfo.hh

int DpmXrdOssStatInfo(const char *path, struct stat *buff,
   int opts, XrdOucEnv   *envP, const char *lfn)
{
   // lfn may or may not be given. Use it if it's available to be sure
   // we have a non-translated name. Otherwise use path.
   std::vector<XrdOucString> names;
   try {
      names = TranslatePathVec(RedirConfig, lfn ? lfn : path);
   } catch(const std::exception &) {
      errno = ENOENT;
      return -1;
   }

   // There is no need to return information about the file (though you can
   // if it is low overhead). However you should clear it in any case.
   //
   memset(buff, 0, sizeof(struct stat));

   DpmIdentity ident;
   XrdDmStackWrap sw;
   try {
      sw.reset(dpm_ss, ident);
   } catch(dmlite::DmException &e) {
      Say.Say("Error setting up access to the dmlite stack", e.what());
      errno = EINVAL;
      return -1;
   } catch(const std::exception &) {
      Say.Say("Error setting up access to the dmlite stack");
      errno = EINVAL;
      return -1;
   }

   // At this point run through the vector looking up the physical file name in
   // the DPM database. If you  find one, you should return the success (0).
   // If none exist, then you should return ENOENT. We do this in a nice way.
   //
   bool found = false;
   for (size_t i = 0; i < names.size(); ++i) {
      try {
         
         dmlite::ExtendedStat xstat;
         dmlite::DmStatus st = sw->getCatalog()->extendedStat(xstat, SafeCStr(names[i]));
         if ( !st.ok() ) continue;
         
         found = true;
         buff->st_atim.tv_sec = xstat.stat.st_atime;
         buff->st_ctim.tv_sec = xstat.stat.st_ctime;
         buff->st_mode = xstat.stat.st_mode;
         buff->st_nlink = xstat.stat.st_nlink;
         buff->st_mtim.tv_sec = xstat.stat.st_mtime;
         buff->st_size = xstat.stat.st_size;
         buff->st_ino = xstat.stat.st_ino;
         break;
      } catch(const std::exception &) {
         // nothing
      }
   }

   // Now if we found it return 0; otherwise, ENOENT.
   //
   EPNAME("DpmXrdOssStatInfo")
   if (found) {
     DEBUG("Found: " << path);
     return 0;
   }
   errno = ENOENT;
   DEBUG("Not found: " << path);
   return -1;
}

int DpmXrdOssStatInfo0(const char *path, struct stat *buff,
   int opts, XrdOucEnv   *envP)
{
   //
   // incase this library has been initialised via the older
   // init function this is the function which gets called. There
   // is no 'lfn' available, so path must be suitable for translation
   // (i.e. it should not have already been translated via a namelib
   // by the oss)
   //
   return DpmXrdOssStatInfo(path, buff, opts, envP, (const char*)0);
}

/******************************************************************************/
/*                    X r d O s s S t a t I n f o I n i t                     */
/******************************************************************************/

// The following gets called to initialize your processing. Argument 'parms'
// contains whatever parameters you put after the shared library path. This
// allows you to skip rescanning the config file if you can encode all of
// your required parms on the statlib directive. The envP will be used to
// get the address of the Name2NameVec object which is stored for use by
// your XrdOssStatInfo() function.

static bool doinit(         XrdOss        *native_oss,
               XrdSysLogger  *Logger,
               const char    *config_fn,
               const char    *parms,
               XrdOucEnv     *envP)
{
   Say.logger(Logger);

   // Save the pointer to the XrdOucName2NameVec object.
   //
   XrdOucName2NameVec *n2nVec = 0;
   if (envP) n2nVec = (XrdOucName2NameVec *)envP->GetPtr("XrdOucName2NameVec*");

   // Perform other initialization for DPM.
   //
   XrdSysError_Table *ETab = XrdDmliteError_Table();
   Say.addTable(ETab);

   XrdDmCommonInit(Logger);

   Say.Say("This is XrdDPMStatInfo "
           XrdDPMVERSIONSTR
           " compiled with xroot "
           XrdCompileVersion );
   
   // If the oss has already loaded the n2n (given in an oss.namelib opt)
   // the n2nVec may be available. If so store it in the RedirConfig.
   // If available it will prevent DpmCommon from trying to load the n2n
   // library itself (from either the dpm.namelib or oss.namelib options)
   //
   RedirConfig.theN2NVec = n2nVec;

   if (DpmCommonConfigProc(Say, config_fn, CommonConfig, &RedirConfig)) {
      Say.Emsg("Init","problem setting up the common config");
      return true;
   }
   Trace.What = CommonConfig.OssTraceLevel;

   try {
      dpm_ss.SetDmConfFile(CommonConfig.DmliteConfig);
      dpm_ss.SetDmStackPoolSize(CommonConfig.DmliteStackPoolSize);
      // get a dmlite stack to trigger the loading of the dmlite
      // configuration. This may do things like setting variables
      // in the enviornment, which is not thread safe and might cause
      // problems later.
      DpmIdentity empty_ident;
      XrdDmStackWrap sw(dpm_ss, empty_ident);
   } catch(const std::exception &) {
      Say.Emsg("Init","problem setting up the dmlite stack");
      return true;
   }

   return false;
}

extern "C"
{
XrdOssStatInfo2_t XrdOssStatInfoInit2(XrdOss        *native_oss,
               XrdSysLogger  *Logger,
               const char    *config_fn,
               const char    *parms,
               XrdOucEnv     *envP)
{
   if (doinit(native_oss,Logger,config_fn,parms,envP)) {
      return 0;
   }

   // Now return the address of your XrdOssStatInfo() function
   // which expects an lfn
   return (XrdOssStatInfo2_t)DpmXrdOssStatInfo;
}

XrdOssStatInfo_t XrdOssStatInfoInit(XrdOss        *native_oss,
               XrdSysLogger  *Logger,
               const char    *config_fn,
               const char    *parms)
{
   if (doinit(native_oss,Logger,config_fn,parms,(XrdOucEnv*)0)) {
      return 0;
   }

   // Now return the address of your XrdOssStatInfo() function
   // which does not expect an lfn
   return (XrdOssStatInfo_t)DpmXrdOssStatInfo0;
}
} /* extern "C" */

/******************************************************************************/
/*                   V e r s i o n   I n f o r m a t i o n                    */
/******************************************************************************/

XrdVERSIONINFO(XrdOssStatInfoInit2,statlib)
XrdVERSIONINFO(XrdOssStatInfoInit,statlib)
