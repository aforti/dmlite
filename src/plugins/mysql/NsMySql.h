/// @file   NsMySql.h
/// @brief  MySQL NS Implementation.
/// @author Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#ifndef NSMYSQL_H
#define	NSMYSQL_H

#include <dirent.h>
#include <dmlite/cpp/utils/poolcontainer.h>
#include <dmlite/cpp/inode.h>
#include <dmlite/cpp/utils/mysqlpools.h>
#include <vector>

#include "utils/MySqlWrapper.h"

namespace dmlite {

  /// Struct used internally to bind when reading
  struct CStat {
    ino_t       parent;
    struct stat stat;
    char        status;
    short       type;
    char        name[256];
    char        guid[37];
    char        csumtype[4];
    char        csumvalue[34];
    char        acl[300 * 13]; // Maximum 300 entries of 13 bytes each
    char        xattr[1024];
  };

  /// Struct used internally to read drectories.
  struct NsMySqlDir: public IDirectory {
    virtual ~NsMySqlDir() {};

    ExtendedStat  dir;           ///< Directory being read.
    CStat         cstat;         ///< Used for the binding
    ExtendedStat  current;       ///< Current entry metadata.
    struct dirent ds;            ///< The structure used to hold the returned data.
    Statement    *stmt;          ///< The statement.
    bool          eod;           ///< True when end of dir is reached.
    MysqlWrap    *conn_;         ///< The connection the statement is relying on.
  };

  // Forward declaration
  class NsMySqlFactory;

  /// Implementation of INode MySQL backend.
  class INodeMySql: public INode {
   public:

    /// Constructor
    INodeMySql(NsMySqlFactory* factory,
              const std::string& db)  ;

    /// Destructor
    ~INodeMySql();

    // Overloading
    std::string getImplId(void) const throw ();

    void setStackInstance(StackInstance* si)  ;
    void setSecurityContext(const SecurityContext* ctx)  ;

    void begin(void)  ;
    void commit(void)  ;
    void rollback(void)  ;

    ExtendedStat create(const ExtendedStat&)  ;

    void symlink(ino_t inode, const std::string &link)  ;

    void unlink(ino_t inode)  ;

    void move  (ino_t inode, ino_t dest)  ;
    void rename(ino_t inode, const std::string& name)  ;

    ExtendedStat extendedStat(ino_t inode)  ;
    ExtendedStat extendedStat(ino_t parent, const std::string& name)  ;
    ExtendedStat extendedStat(const std::string& guid)  ;

    DmStatus extendedStat(ExtendedStat &xstat, ino_t inode)  ;
    DmStatus extendedStat(ExtendedStat &xstat, ino_t parent, const std::string& name)  ;

    SymLink readLink(ino_t inode)  ;

    void addReplica   (const Replica&)  ;
    void deleteReplica(const Replica&)  ;

    std::vector<Replica> getReplicas(ino_t inode)  ;

    Replica  getReplica   (int64_t rid)  ;

    DmStatus getReplica   (Replica &r, const std::string& sfn)  ;
    Replica getReplica    (const std::string& sfn)  ;

    void    updateReplica(const Replica& replica)  ;

    void utime(ino_t inode, const struct utimbuf* buf)  ;

    void setMode(ino_t inode, uid_t uid, gid_t gid,
                mode_t mode, const Acl& acl)  ;

    void setSize    (ino_t inode, size_t size)  ;
    void setChecksum(ino_t inode, const std::string& csumtype,
                                 const std::string& csumvalue)  ;

    std::string getComment   (ino_t inode)  ;
    void        setComment   (ino_t inode, const std::string& comment)  ;
    void        deleteComment(ino_t inode)  ;

    void setGuid(ino_t inode, const std::string& guid)  ;

    void updateExtendedAttributes(ino_t inode,
                                  const Extensible& attr)  ;

    IDirectory*    openDir (ino_t inode)  ;
    void           closeDir(IDirectory* dir)  ;
    ExtendedStat*  readDirx(IDirectory* dir)  ;
    struct dirent* readDir (IDirectory* dir)  ;

   protected:
    /// The corresponding factory.
    NsMySqlFactory* factory_;

    /// Transaction level, so begins and commits can be nested.
    unsigned transactionLevel_;

   private:
    /// NS DB.
    std::string nsDb_;

    // Connection
    dmlite::MysqlWrap *conn_;


  };

  /// Convenience class that releases a resource on destruction
  class InodeMySqlTrans {
  public:
    InodeMySqlTrans(INodeMySql *o)
    {
      obj = o;
      obj->begin();
    }

    ~InodeMySqlTrans() {
      if (obj != 0) obj->rollback();
      obj = 0;
    }

    void Commit() {
      if (obj != 0) obj->commit();
      obj = 0;
    }

  private:
    INodeMySql *obj;

  };


};

#endif	// NSMYSQL_H
