/// @file   RFIO.h
/// @brief  RFIO IO.
/// @author CERN DPM <dpm-devel@cern.ch>
#ifndef RFIO_H
#define	RFIO_H

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/io.h>
#include <fstream>
#include <pthread.h>

namespace dmlite {

  class StdRFIOFactory: public IODriverFactory {
   public:
    StdRFIOFactory();
    virtual ~StdRFIOFactory();

    void configure(const std::string& key, const std::string& value)  ;
    IODriver* createIODriver(PluginManager* pm)  ;

   private:
    std::string passwd_;
    bool        useIp_;
    bool        useDn_;
  };

  class StdRFIODriver: public IODriver {
   public:
    StdRFIODriver(std::string passwd, bool useIp, bool useDn);
    virtual ~StdRFIODriver();

    std::string getImplId() const throw();

    virtual void setStackInstance(StackInstance* si)  ;
    virtual void setSecurityContext(const SecurityContext* ctx)  ;

    IOHandler* createIOHandler(const std::string& pfn, int flags,
                               const Extensible& extras, mode_t mode)  ;

    void doneWriting(const Location& loc)  ;

   private:
    StackInstance*         si_;
    const SecurityContext* secCtx_;

    std::string passwd_;
    bool        useIp_;
    bool        useDn_;
  };

  class StdRFIOHandler: public IOHandler {
  public:
    StdRFIOHandler(const std::string& path,
                   int flags, mode_t mode)  ;
    virtual ~StdRFIOHandler();

    void   close(void)  ;
    int    fileno(void)  ;
    size_t read (char* buffer, size_t count)  ;
    size_t pread(void* buffer, size_t count, off_t offset)  ;
    size_t write(const char* buffer, size_t count)  ;
    size_t pwrite(const void* buffer, size_t count, off_t offset)  ;
    void   seek (off_t offset, Whence whence)  ;
    off_t  tell (void)  ;
    void   flush(void)  ;
    bool   eof  (void)  ;

  protected:
    int  fd_;
    bool eof_;
    pthread_mutex_t mtx_;
    bool islocal_;

    class lk {
      public:
        lk(pthread_mutex_t *mp): mp(mp)
          { int err; if (mp && (err=pthread_mutex_lock(mp)))
             throw DmException(err, "Could not lock a mutex"); }
        ~lk()
          { int err; if (mp && (err=pthread_mutex_unlock(mp)))
            throw DmException(err, "Could not unlock a mutex"); }
      private:
        pthread_mutex_t *mp;
    };
    class pp {
      public:
        pp(int fd, bool *peof, off_t np);
        ~pp();
      private:
        int fd;
        off_t pos;
        bool eof,*peof;
    };
  };

};

#endif	// RFIO_H
