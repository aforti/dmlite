/// @file   ProfilerCatalog.h
/// @brief  Profiler plugin.
/// @author Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#ifndef PROFILERCATALOG_H
#define	PROFILERCATALOG_H

#include "ProfilerXrdMon.h"

#include <dmlite/cpp/catalog.h>

namespace dmlite {

  // Profiler catalog.
  class ProfilerCatalog: public Catalog, private ProfilerXrdMon
  {
   public:

    /// Constructor.
    /// @param decorates The underlying decorated catalog.
    ProfilerCatalog(Catalog* decorates)  ;

    /// Destructor.
    ~ProfilerCatalog();

    // Overloading
    std::string getImplId(void) const throw ();

    void setStackInstance(StackInstance* si)  ;

    void setSecurityContext(const SecurityContext*)  ;

    void        changeDir     (const std::string&)  ;
    std::string getWorkingDir (void)                ;

    DmStatus extendedStat(ExtendedStat &xstat, const std::string& path,
                          bool followSym = true)  ;
                          
    ExtendedStat extendedStat(const std::string&, bool)  ;
    ExtendedStat extendedStatByRFN(const std::string& rfn)  ;

    bool access(const std::string& path, int mode)  ;
    bool accessReplica(const std::string& replica, int mode)  ;

    void addReplica(const Replica&)  ;
    void deleteReplica(const Replica&)  ;
    std::vector<Replica> getReplicas(const std::string&)  ;

    void        symlink (const std::string&, const std::string&)  ;
    std::string readLink(const std::string& path)  ;

    void unlink(const std::string&)                      ;

    void create(const std::string&, mode_t)  ;

    mode_t umask          (mode_t)                           throw ();
    void   setMode     (const std::string&, mode_t)        ;
    void   setOwner    (const std::string&, uid_t, gid_t, bool)  ;

    void setSize    (const std::string&, size_t)  ;
    void setChecksum(const std::string&, const std::string&, const std::string&)  ;

    void getChecksum(const std::string& path, const std::string& csumtype,
                     std::string& csumvalue, const std::string& pfn,
                     const bool forcerecalc = false, const int waitsecs = 0)  ;

    void setAcl(const std::string&, const Acl&)  ;

    void utime(const std::string&, const struct utimbuf*)  ;

    std::string getComment(const std::string&)                      ;
    void        setComment(const std::string&, const std::string&)  ;

    void setGuid(const std::string&, const std::string&)  ;

    void updateExtendedAttributes(const std::string&,
                                  const Extensible&)  ;

    Directory* openDir (const std::string&)  ;
    void       closeDir(Directory*)          ;

    struct dirent* readDir (Directory*)  ;
    ExtendedStat*  readDirx(Directory*)  ;

    void makeDir(const std::string&, mode_t)  ;

    void rename     (const std::string&, const std::string&)  ;
    void removeDir  (const std::string&)                      ;

    Replica getReplicaByRFN(const std::string&)  ;
    void    updateReplica(const Replica&)  ;

   protected:
    /// The decorated Catalog.
    Catalog* decorated_;
    char*    decoratedId_;
  };

};

#endif	// PROFILERCATALOG_H
