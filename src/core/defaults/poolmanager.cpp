#include <dmlite/cpp/poolmanager.h>
#include "NotImplemented.h"


using namespace dmlite;



PoolManagerFactory::~PoolManagerFactory()
{
  // Nothing
}



FACTORY_NOT_IMPLEMENTED(PoolManager* PoolManagerFactory::createPoolManager(PluginManager* pm)  );



PoolManager* PoolManagerFactory::createPoolManager(PoolManagerFactory* factory, PluginManager* pm)  
{
  if (factory)
    return factory->createPoolManager(pm);
  return 0;
}



PoolManager::~PoolManager()
{
  // Nothing
}



NOT_IMPLEMENTED(std::vector<Pool> PoolManager::getPools(PoolAvailability availability)  );
NOT_IMPLEMENTED(Pool PoolManager::getPool(const std::string& poolname)  );
NOT_IMPLEMENTED(void PoolManager::newPool(const Pool& pool)  );
NOT_IMPLEMENTED(void PoolManager::updatePool(const Pool& pool)  );
NOT_IMPLEMENTED(void PoolManager::deletePool(const Pool& pool)  );
NOT_IMPLEMENTED(Location PoolManager::whereToRead(const std::string& path)  );
NOT_IMPLEMENTED(Location PoolManager::whereToRead(ino_t inode)  );
NOT_IMPLEMENTED(Location PoolManager::whereToWrite(const std::string& path)  );
NOT_IMPLEMENTED(Location PoolManager::chooseServer(const std::string& path)  );

NOT_IMPLEMENTED(void PoolManager::cancelWrite(const Location& loc)  );

NOT_IMPLEMENTED(void PoolManager::getDirSpaces(const std::string& path, int64_t &totalfree, int64_t &used)  );
NOT_IMPLEMENTED(DmStatus PoolManager::fileCopyPush(const std::string& localsrcpath, const std::string &remotedesturl, int cksumcheck, char *cksumtype, dmlite_xferinfo *progressdata)  );

NOT_IMPLEMENTED(DmStatus PoolManager::fileCopyPull(const std::string& localdestpath, const std::string &remotesrcurl, int cksumcheck, char *cksumtype, dmlite_xferinfo *progressdata)  );
